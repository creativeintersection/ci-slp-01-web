import express from 'express';
require('dotenv').config({ silent: true });

let app = express();

app.set('view options', { layout: false });
app.use(express.static(__dirname + '/'));

let port = process.env.PORT || 8080;

app.get('/', (req, res) => {
    res.render('/');
});

app.listen(port, err => {
    if (err) {
        return console.error(err);
    }

    console.log('The server is listening on port %s', port);
});