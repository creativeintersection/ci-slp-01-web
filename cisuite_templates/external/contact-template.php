<!--xCICMS_START TYPE="newInclude" NAME="external/slp-header.php" x---><!--xCICMS_ENDx-->

<div class="container-fluid <!--xCICMS_START TYPE="dropdown" ID="675289" GROUP="675289" DESC="Show Feature Image?" DEFASSET="9100489" x--->
    <!--xDROPDOWN TYPE="item" ID="6752892" VALUE="hidden" TEXT="No, Dont Show Feature Image" x-->
    <!--xDROPDOWN TYPE="item" ID="6752891" VALUE="" TEXT="Yes, Show Feature Image" x-->
<!--xCICMS_ENDx-->">
    <div class="row">
        <div id="slp-carousel" class="carousel" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active" style="background:url('<!--xCICMS_START TYPE="image" ID="45" DESC="Feature Image (JPG 1200 x 800px)" RESOURCE="imageResources" x---><!--xCICMS_ENDx-->') no-repeat fixed 50% -30.8182px / cover; -webkit-transform: none; transform: none;" title="Feature Image">
                    <div class="carousel-caption">
                        <h1><!--xCICMS_START TYPE="text" ID="551" DESC="Feature Image Caption" x---><!--xCICMS_ENDx--></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--xCICMS_START TYPE="comp" GROUP="2" DESC="Main Page Content" DEFASSET="250" x--->
<div class="outerwrapper slp-padding-top-btm slp-bkg-<!--xCOMP TYPE="dropdown" ID="675285" GROUP="675285" DESC="Section Background Colour?" DEFASSET="9100485" x-->
    <!--xDROPDOWN TYPE="item" ID="6752851" VALUE="blue" TEXT="Blue" x-->
    <!--xDROPDOWN TYPE="item" ID="6752852" VALUE="green" TEXT="Green" x-->
    <!--xDROPDOWN TYPE="item" ID="6752853" VALUE="white" TEXT="White" x-->
<!--xCOMP_ENDx-->">
    <div class="<!--xCOMP TYPE="dropdown" ID="675284" GROUP="675284" DESC="Does the section have Left Right Margins or Full Width?" DEFASSET="9100484" x-->
        <!--xDROPDOWN TYPE="item" ID="6752841" VALUE="container" TEXT="Left Right Margins" x-->
        <!--xDROPDOWN TYPE="item" ID="6752842" VALUE="container-fluid" TEXT="Full Width" x-->
    <!--xCOMP_ENDx-->">
        <div class="row">
            <div class="hidden-md col-sm-1"></div>
            <div class="col-md-12 col-sm-10 text-left">
                <div class="slp-heading <!--xCOMP TYPE="dropdown" ID="675279" GROUP="675279" DESC="Show Heading?" DEFASSET="9100477" x-->
                    <!--xDROPDOWN TYPE="item" ID="9100577" VALUE="hidden" TEXT="No, Dont Show Heading" x-->
                    <!--xDROPDOWN TYPE="item" ID="9100477" VALUE="" TEXT="Yes, Show Heading" x-->
                <!--xCOMP_ENDx--> slp-text-<!--xCOMP TYPE="dropdown" ID="675286" GROUP="675286" DESC="Heading Text Colour?" DEFASSET="6752861" x-->
                <!--xDROPDOWN TYPE="item" ID="6752861" VALUE="blue" TEXT="Blue" x-->
                <!--xDROPDOWN TYPE="item" ID="6752862" VALUE="green" TEXT="Green" x-->
                <!--xDROPDOWN TYPE="item" ID="6752863" VALUE="black" TEXT="Black" x-->
                <!--xDROPDOWN TYPE="item" ID="6752864" VALUE="white" TEXT="White" x-->
                <!--xCOMP_ENDx-->">
                    <h3><!--xCOMP TYPE="text" ID="250" DESC="Heading" x--></h3>
                </div>
                <div class="slp-lead">
                    <div class="displayimage <!--xCOMP TYPE="dropdown" ID="675277" GROUP="639877" DESC="Image style" DEFASSET="6100577" x-->
                        <!--xDROPDOWN TYPE="item" ID="6100477" VALUE="imageStyle0" TEXT="Do not display an image" x-->
                        <!--xDROPDOWN TYPE="item" ID="6100577" VALUE="imageStyle2" TEXT="Image Left Aligned" x-->
                        <!--xDROPDOWN TYPE="item" ID="6100677" VALUE="imageStyle1" TEXT="Image Right Aligned" x-->
                        <!--xDROPDOWN TYPE="item" ID="6100777" VALUE="imageStyle3" TEXT="Image Centred, sides clear" x-->
                    <!--xCOMP_ENDx-->">
                        <div class="popup-container-trigger">
                            <img src="resize_image.php?image=<!--xCOMP TYPE="image" ID="6666" DESC="Image (Max width 1200px)" RESOURCE="imageResources" x-->&max_width=<!--xCOMP TYPE="text" ID="32" DESC="Image width (Numeric characters only; height locked to max 1000px)" x-->&max_height=1000" alt="<!--xCOMP TYPE="text" ID="3905" DESC="Image Alt Text" x-->" />
                        </div>
                        <div class="image_caption slp-text-<!--xCOMP TYPE="dropdown" ID="675288" GROUP="675288" DESC="Image Caption Colour?" DEFASSET="6752881" x-->
                        <!--xDROPDOWN TYPE="item" ID="6752881" VALUE="blue" TEXT="Blue" x-->
                        <!--xDROPDOWN TYPE="item" ID="6752882" VALUE="green" TEXT="Green" x-->
                        <!--xDROPDOWN TYPE="item" ID="6752883" VALUE="black" TEXT="Black" x-->
                        <!--xDROPDOWN TYPE="item" ID="6752884" VALUE="white" TEXT="White" x-->
                        <!--xCOMP_ENDx-->">
                            <h4 style="text-align:center;"><!--xCOMP TYPE="text" id="252778" DESC="Image caption" x--></h4>
                        </div>
                    </div>
                    <div class="text-container">
                        <!--xCOMP TYPE="HTML" ID="25277" DESC="Body Copy" x-->
                    </div>
                    <div class="hidden popup-container-image"><!--xCOMP TYPE="REPEAT" ID="6666" x--></div>
                    <div class="popup-container hidden" data-popuptype="<!--xCOMP TYPE="dropdown" ID="675278" GROUP="639878" DESC="Popup Type" DEFASSET="8100471" x-->
                        <!--xDROPDOWN TYPE="item" ID="8100471" VALUE="" TEXT="None" x-->
                        <!--xDROPDOWN TYPE="item" ID="8100477" VALUE="video" TEXT="Video" x-->
                        <!--xDROPDOWN TYPE="item" ID="8100577" VALUE="image" TEXT="Image" x-->
                        <!--xCOMP_ENDx-->" data-value="<!--xCOMP TYPE="TEXT" ID="25278" DESC="Youtube Video URL (e.g. https://www.youtube.com/embed/RPwOGf1Xh5M)" x-->">
                        <div class="popup-container-content"></div>
                    </div>
                    <div class="slp-btn-wrapper" style="float: left;">
                        <a class="slp-btn-inline-center <!--xCOMP TYPE="dropdown" ID="675280" GROUP="639880" DESC="Show Button?" DEFASSET="6100580" x-->
                            <!--xDROPDOWN TYPE="item" ID="6398802" VALUE="hidden" TEXT="No, Dont Show Button" x-->
                            <!--xDROPDOWN TYPE="item" ID="6398801" VALUE="" TEXT="Yes, Show Button" x-->
                        <!--xCOMP_ENDx--> slp-btn-<!--xCOMP TYPE="dropdown" ID="675281" GROUP="639881" DESC="Button Colour" DEFASSET="6100581" x-->
                            <!--xDROPDOWN TYPE="item" ID="6398811" VALUE="blue" TEXT="Blue Button" x-->
                            <!--xDROPDOWN TYPE="item" ID="6398812" VALUE="green" TEXT="Green Button" x-->
                        <!--xCOMP_ENDx-->" href="<!--xCOMP TYPE="text" ID="675282" DESC="Button Link" x-->">
                            <!--xCOMP TYPE="text" ID="675283" DESC="Button Text" x-->
                        </a>
                    </div>
                </div>
            </div>
            <div class="hidden-md col-sm-1"></div>
        </div>
    </div>
</div>
<!--xCICMS_ENDx-->

<div class="outerwrapper  slp-padding-top-btm slp-bkg-<!--xCICMS_START TYPE="dropdown" ID="975285" GROUP="975285" DESC="Map - Background Colour?" DEFASSET="9752851" x--->
    <!--xDROPDOWN TYPE="item" ID="9752851" VALUE="blue" TEXT="Blue" x-->
    <!--xDROPDOWN TYPE="item" ID="9752852" VALUE="green" TEXT="Green" x-->
    <!--xDROPDOWN TYPE="item" ID="9752853" VALUE="white" TEXT="White" x-->
<!--xCICMS_ENDx--> slp-text-<!--xCICMS_START TYPE="dropdown" ID="975286" GROUP="975286" DESC="Map - Text Colour?" DEFASSET="9752861" x--->
    <!--xDROPDOWN TYPE="item" ID="9752861" VALUE="blue" TEXT="Blue" x-->
    <!--xDROPDOWN TYPE="item" ID="9752862" VALUE="green" TEXT="Green" x-->
    <!--xDROPDOWN TYPE="item" ID="9752863" VALUE="black" TEXT="Black" x-->
    <!--xDROPDOWN TYPE="item" ID="9752864" VALUE="white" TEXT="White" x-->
<!--xCICMS_ENDx-->">
    <div class="<!--xCICMS_START TYPE="dropdown" ID="975284" GROUP="975284" DESC="Map - Does this have Left Right Margins or Full Width?" DEFASSET="9752841" x--->
        <!--xDROPDOWN TYPE="item" ID="9752841" VALUE="container" TEXT="Left Right Margins" x-->
        <!--xDROPDOWN TYPE="item" ID="9752842" VALUE="container-fluid" TEXT="Full Width" x-->
    <!--xCICMS_ENDx-->">

        <div class="row">
            <div class="col-md-6">
                <h3 class="slp-text-green">Address</h3>
                <div class="slp-left-margin slp-btn-borderless"><!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10001" x--><!--xCICMS_ENDx--></div>
                <div class="slp-gap-lg"></div>
                <h3 class="slp-text-green">Phone</h3>
                <div class="slp-left-margin slp-btn-borderless"><!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10002" x--><!--xCICMS_ENDx--></div>
                <div class="slp-gap-lg"></div>
                <h3 class="slp-text-green">Email</h3>
                <div class="slp-left-margin slp-btn-borderless"><!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10003" x--><!--xCICMS_ENDx--></div>
            </div>
            <div class="col-md-6">
                <div class="embed-responsive embed-responsive-4by3">
                    <iframe class="embed-responsive-item" src="<!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="20001" x--><!--xCICMS_ENDx-->" ID="303" x-->" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="outerwrapper slp-padding-top-btm slp-bkg-<!--xCICMS_START TYPE="dropdown" ID="1275285" GROUP="1275285" DESC="Contact Form - Background Colour?" DEFASSET="12752851" x--->
    <!--xDROPDOWN TYPE="item" ID="12752851" VALUE="blue" TEXT="Blue" x-->
    <!--xDROPDOWN TYPE="item" ID="12752852" VALUE="green" TEXT="Green" x-->
    <!--xDROPDOWN TYPE="item" ID="12752853" VALUE="white" TEXT="White" x-->
<!--xCICMS_ENDx--> slp-text-<!--xCICMS_START TYPE="dropdown" ID="1275286" GROUP="1275286" DESC="Contact Form - Text Colour?" DEFASSET="12752861" x--->
    <!--xDROPDOWN TYPE="item" ID="12752861" VALUE="blue" TEXT="Blue" x-->
    <!--xDROPDOWN TYPE="item" ID="12752862" VALUE="green" TEXT="Green" x-->
    <!--xDROPDOWN TYPE="item" ID="12752863" VALUE="black" TEXT="Black" x-->
    <!--xDROPDOWN TYPE="item" ID="1275286" VALUE="white" TEXT="White" x-->
<!--xCICMS_ENDx-->">
    <div class="<!--xCICMS_START TYPE="dropdown" ID="1275284" GROUP="1275284" DESC="Contact Form - Does this have Left Right Margins or Full Width?" DEFASSET="12752841" x--->
        <!--xDROPDOWN TYPE="item" ID="12752841" VALUE="container" TEXT="Left Right Margins" x-->
        <!--xDROPDOWN TYPE="item" ID="12752842" VALUE="container-fluid" TEXT="Full Width" x-->
    <!--xCICMS_ENDx-->">
        <div class="row">
            <div class="col-xs-12">
                <!--xCICMS_START TYPE="HTML" ID="125277" DESC="Contact Form - Body Copy" x---><!--xCICMS_ENDx-->
            </div>
            <div class="col-xs-12">
                <form id="frmContact" class="form-horizontal">
                    <div class="form-group">
                        <label for="txtName" class="col-sm-offset-2 col-sm-8">Name</label>
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="text" class="form-control" id="txtName" name="epbf-name" placeholder="Name" maxlength="100">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="txtEmail" class="col-sm-offset-2 col-sm-8">Email</label>
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="text" class="form-control" id="txtEmail" name="epbf-email" placeholder="Email" maxlength="200">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="txtMessage" class="col-sm-offset-2 col-sm-8">Message</label>
                        <div class="col-sm-offset-2 col-sm-8">
                            <textarea class="form-control" rows="3" id="txtMessage" name="epbf-message" placeholder="Message" maxlength="2000" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <div class="g-recaptcha" data-sitekey="6LdL1FoUAAAAAAnGF5II8N8Fa0jqvlfiP3Tmt-Zv"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8 errMessage hidden"></div>
                        <div class="col-sm-offset-2 col-sm-8 text-center">
                            <button type="submit" class="slp-btn-inline-center slp-btn-<!--xCICMS_START TYPE="dropdown" ID="1275281" GROUP="1275281" DESC="Contact Form - Button Colour" DEFASSET="12752811" x--->
                            <!--xDROPDOWN TYPE="item" ID="12752811" VALUE="blue" TEXT="Blue Button" x-->
                            <!--xDROPDOWN TYPE="item" ID="12752812" VALUE="green" TEXT="Green Button" x-->
                            <!--xCICMS_ENDx-->">
                            <!--xCICMS_START TYPE="text" ID="1275283" DESC="Contact Form - Button Text" x---><!--xCICMS_ENDx-->
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!--xCICMS_START TYPE="newInclude" NAME="external/slp-footer.php" x---><!--xCICMS_ENDx-->