<!--xCICMS_START TYPE="newInclude" NAME="external/slp-header.php" x---><!--xCICMS_ENDx-->

<div class="outerwrapper slp-padding-top-btm slp-bkg-<!--xCICMS_START TYPE="dropdown" ID="1275285" GROUP="1275285" DESC="Forgot Password Form Background Colour?" DEFASSET="12752851" x--->
<!--xDROPDOWN TYPE="item" ID="12752851" VALUE="blue" TEXT="Blue" x-->
<!--xDROPDOWN TYPE="item" ID="12752852" VALUE="green" TEXT="Green" x-->
<!--xDROPDOWN TYPE="item" ID="12752853" VALUE="white" TEXT="White" x-->
<!--xCICMS_ENDx--> slp-text-<!--xCICMS_START TYPE="dropdown" ID="1275286" GROUP="1275286" DESC="Forgot Password Form Text Colour?" DEFASSET="12752861" x--->
<!--xDROPDOWN TYPE="item" ID="12752861" VALUE="blue" TEXT="Blue" x-->
<!--xDROPDOWN TYPE="item" ID="12752862" VALUE="green" TEXT="Green" x-->
<!--xDROPDOWN TYPE="item" ID="12752863" VALUE="black" TEXT="Black" x-->
<!--xDROPDOWN TYPE="item" ID="1275286" VALUE="white" TEXT="White" x-->
<!--xCICMS_ENDx-->">
    <div class="<!--xCICMS_START TYPE="dropdown" ID="1275284" GROUP="1275284" DESC="Does the forgot password form have Left Right Margins or Full Width?" DEFASSET="12752841" x--->
    <!--xDROPDOWN TYPE="item" ID="12752841" VALUE="container" TEXT="Left Right Margins" x-->
    <!--xDROPDOWN TYPE="item" ID="12752842" VALUE="container-fluid" TEXT="Full Width" x-->
    <!--xCICMS_ENDx-->">
        <div class="row">
            <div class="col-xs-12">
                <div class="slp-heading">
                    <h3>Forgot Password</h3>
                </div>
            </div>
            <div class="col-xs-12">
                <!--xCICMS_START TYPE="HTML" ID="125277" DESC="Body Copy" x---><!--xCICMS_ENDx-->
            </div>

            <div class="col-sm-8 col-sm-offset-2">
                <form id="frm_forgot_password" method="POST" class="form-horizontal">
                    <div class="slp-gap-lg"></div>
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label" style="text-align: left;">Email Address</label>
                        <div class="col-md-9">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" data-required="yes">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="slp-gap-lg"></div>
                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <div class="alert alert-danger fade in err-message" role="alert" style="display: none;">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Error(s):</strong><br><br>
                                <ul></ul>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <button type="submit" class="slp-btn-inline-center slp-btn-<!--xCICMS_START TYPE="dropdown" ID="1275281" GROUP="1275281" DESC="Submit Button - Colour" DEFASSET="12752811" x--->
                            <!--xDROPDOWN TYPE="item" ID="12752811" VALUE="blue" TEXT="Blue Button" x-->
                            <!--xDROPDOWN TYPE="item" ID="12752812" VALUE="green" TEXT="Green Button" x-->
                            <!--xCICMS_ENDx-->">
                                Submit
                            </button>
                        </div>
                        <div class="slp-gap-lg"></div>
                        <div class="col-xs-12">
                            <p class="p-form text-center"><a class="slp-btn-borderless-<!--xCICMS_START TYPE="dropdown" ID="1275290" GROUP="1275290" DESC="Register - Link Colour" DEFASSET="12752901" x--->
                                <!--xDROPDOWN TYPE="item" ID="12752901" VALUE="blue" TEXT="Blue Button" x-->
                                <!--xDROPDOWN TYPE="item" ID="12752902" VALUE="green" TEXT="Green Button" x-->
                                <!--xCICMS_ENDx-->" href="/register">Not Registered Yet?</a>
                            </p>
                        </div>
                    </div>
                </form>
                <p class="information" style="display:none;">
                    If there was an account with this email, it should have received an email with a link to reset your password. If you have not received the email, check your spam or <a id="resend" href="#">click here</a> to send it again.
                </p>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        var classCol = 'col-md-9 col-md-offset-3';

        newFormHandler.formInit('frm_forgot_password', classCol);

        $('#frm_forgot_password').on('submit', function(){
            var self = $(this);

            var boolSuccess = newFormHandler.formCheckAllFieldNameIsEmpty('frm_forgot_password', classCol);

            if (boolSuccess && isDoneSubmitting) {
                self.hide();
                self.parent().append(htmlLargeSpinner);

                $.post(urlForgotPasswordSubmit, {
                    apikey: apikey,
                    email: $('#email').val()
                }, function(r){
                    self.show();
                    $('.bki-spinner').remove();

                    if (r.error) {
                        $('.err-message ul').empty();
                        $('.err-message ul').append('<li>'+r.error+'</li>');
                        $('.err-message').css('display', 'block');
                    }
                    else {
                        $('.bki-spinner').fadeOut();
                        $('.information').fadeIn();
                    }
                });
            }
            return false;
        });

        $('#resend').on('click', function(e){
            e.preventDefault();
            $('#frm_forgot_password').submit();
        });
    });
</script>

<!--xCICMS_START TYPE="newInclude" NAME="external/slp-footer.php" x---><!--xCICMS_ENDx-->