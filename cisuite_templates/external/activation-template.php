<!--xCICMS_START TYPE="newInclude" NAME="external/slp-header.php" x---><!--xCICMS_ENDx-->

<div class="outerwrapper slp-padding-top-btm slp-bkg-<!--xCICMS_START TYPE="dropdown" ID="1275285" GROUP="1275285" DESC="Activation Form Background Colour?" DEFASSET="12752851" x--->
<!--xDROPDOWN TYPE="item" ID="12752851" VALUE="blue" TEXT="Blue" x-->
<!--xDROPDOWN TYPE="item" ID="12752852" VALUE="green" TEXT="Green" x-->
<!--xDROPDOWN TYPE="item" ID="12752853" VALUE="white" TEXT="White" x-->
<!--xCICMS_ENDx--> slp-text-<!--xCICMS_START TYPE="dropdown" ID="1275286" GROUP="1275286" DESC="Activation Form Text Colour?" DEFASSET="12752861" x--->
<!--xDROPDOWN TYPE="item" ID="12752861" VALUE="blue" TEXT="Blue" x-->
<!--xDROPDOWN TYPE="item" ID="12752862" VALUE="green" TEXT="Green" x-->
<!--xDROPDOWN TYPE="item" ID="12752863" VALUE="black" TEXT="Black" x-->
<!--xDROPDOWN TYPE="item" ID="1275286" VALUE="white" TEXT="White" x-->
<!--xCICMS_ENDx-->">
    <div class="slp-gallery-img-overlay slp-padding-top-btm">
        <div class="<!--xCICMS_START TYPE="dropdown" ID="1275284" GROUP="1275284" DESC="Does the activation form have Left Right Margins or Full Width?" DEFASSET="12752841" x--->
        <!--xDROPDOWN TYPE="item" ID="12752841" VALUE="container" TEXT="Left Right Margins" x-->
        <!--xDROPDOWN TYPE="item" ID="12752842" VALUE="container-fluid" TEXT="Full Width" x-->
        <!--xCICMS_ENDx-->">
            <div class="row">
                <div class="slp-gap-lg"></div>
                <div class="slp-gap-lg"></div>
                <div class="col-xs-12">
                    <!--xCICMS_START TYPE="HTML" ID="125277" DESC="Body Copy" x---><!--xCICMS_ENDx-->
                </div>

                <div class="col-sm-8 col-sm-offset-2">
                    <form id="frm_activation" method="POST">
                        <div class="form-group">
                            <div class="alert alert-danger fade in err-message" role="alert" style="display: none;">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Error(s):</strong><br><br>
                                <ul></ul>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="slp-btn-inline-center slp-btn-<!--xCICMS_START TYPE="dropdown" ID="1275281" GROUP="1275281" DESC="Activate Button - Colour" DEFASSET="12752811" x--->
                                <!--xDROPDOWN TYPE="item" ID="12752811" VALUE="blue" TEXT="Blue Button" x-->
                                <!--xDROPDOWN TYPE="item" ID="12752812" VALUE="green" TEXT="Green Button" x-->
                                <!--xCICMS_ENDx-->">
                                    Activate
                                </button>
                            </div>
                            <div class="slp-gap-lg"></div>
                            <div class="slp-gap-lg"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        var classCol = '';

        newFormHandler.formInit('frm_activation', classCol);

        $('#frm_activation').on('submit', function(){
            var self = $(this);
            var boolSuccess = newFormHandler.formCheckAllFieldNameIsEmpty('frm_activation', classCol);

            if(boolSuccess && isDoneSubmitting){
                self.hide();
                self.parent().append(htmlLargeSpinner);

                var data = self.serializeArray();
                data.push({name: 'apikey', value: apikey});
                data.push({name: 'userId', value: activationUserId});
                data.push({name: 'hash', value: activationHash});

                isDoneSubmitting = false;

                newFormHandler.postForm(urlActivation, data, function(r){
                    isDoneSubmitting = true;

                    if(r.error){
                        self.show();
                        $('.slp-spinner').remove();

                        $('.err-message ul').empty();
                        $('.err-message ul').append('<li>'+r.error+'</li>');
                        $('.err-message').css('display', 'block');
                    }
                    else {
                        window.location.href = urlLoginView+'?activated=true';
                    }
                });
            }

            return false;
        });
    });
</script>

<!--xCICMS_START TYPE="newInclude" NAME="external/slp-footer.php" x---><!--xCICMS_ENDx-->