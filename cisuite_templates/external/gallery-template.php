<!--xCICMS_START TYPE="newInclude" NAME="external/slp-header.php" x---><!--xCICMS_ENDx-->

<div class="container-fluid <!--xCICMS_START TYPE="dropdown" ID="675289" GROUP="675289" DESC="Show Feature Image?" DEFASSET="9100489" x--->
    <!--xDROPDOWN TYPE="item" ID="6752892" VALUE="hidden" TEXT="No, Dont Show Feature Image" x-->
    <!--xDROPDOWN TYPE="item" ID="6752891" VALUE="" TEXT="Yes, Show Feature Image" x-->
<!--xCICMS_ENDx-->">
    <div class="row">
        <div id="slp-carousel" class="carousel" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active" style="background:url('<!--xCICMS_START TYPE="image" ID="45" DESC="Feature Image (JPG 1200 x 800px)" RESOURCE="imageResources" x---><!--xCICMS_ENDx-->') no-repeat fixed 50% -30.8182px / cover; -webkit-transform: none; transform: none;" title="Feature Image">
                    <div class="carousel-caption">
                        <h1><!--xCICMS_START TYPE="text" ID="551" DESC="Feature Image Caption" x---><!--xCICMS_ENDx--></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="outerwrapper <!--xCICMS_START TYPE="dropdown" ID="975289" GROUP="975289" DESC="Main Page Content Section?" DEFASSET="9752892" x--->
<!--xDROPDOWN TYPE="item" ID="9752892" VALUE="hidden" TEXT="No, Dont Show Main Page Content Section" x-->
<!--xDROPDOWN TYPE="item" ID="9752891" VALUE="" TEXT="Yes, Show Main Page Content Section" x-->
<!--xCICMS_ENDx-->">
    <!--xCICMS_START TYPE="comp" GROUP="2" DESC="Main Page Content" DEFASSET="250" x--->
    <div class="outerwrapper slp-padding-top-btm slp-bkg-<!--xCOMP TYPE="dropdown" ID="675285" GROUP="675285" DESC="Main Page Content - Background Colour?" DEFASSET="9100485" x-->
    <!--xDROPDOWN TYPE="item" ID="6752851" VALUE="blue" TEXT="Blue" x-->
    <!--xDROPDOWN TYPE="item" ID="6752852" VALUE="green" TEXT="Green" x-->
    <!--xDROPDOWN TYPE="item" ID="6752853" VALUE="white" TEXT="White" x-->
    <!--xCOMP_ENDx-->">
        <div class="<!--xCOMP TYPE="dropdown" ID="675284" GROUP="675284" DESC="Does the section have Left Right Margins or Full Width?" DEFASSET="9100484" x-->
        <!--xDROPDOWN TYPE="item" ID="6752841" VALUE="container" TEXT="Left Right Margins" x-->
        <!--xDROPDOWN TYPE="item" ID="6752842" VALUE="container-fluid" TEXT="Full Width" x-->
        <!--xCOMP_ENDx-->">
            <div class="row">
                <div class="hidden-md col-sm-1"></div>
                <div class="col-md-12 col-sm-10 text-left">
                    <div class="slp-heading <!--xCOMP TYPE="dropdown" ID="675279" GROUP="675279" DESC="Show Heading?" DEFASSET="9100477" x-->
                    <!--xDROPDOWN TYPE="item" ID="9100577" VALUE="hidden" TEXT="No, Dont Show Heading" x-->
                    <!--xDROPDOWN TYPE="item" ID="9100477" VALUE="" TEXT="Yes, Show Heading" x-->
                    <!--xCOMP_ENDx--> slp-text-<!--xCOMP TYPE="dropdown" ID="675286" GROUP="675286" DESC="Heading Text Colour?" DEFASSET="6752861" x-->
                    <!--xDROPDOWN TYPE="item" ID="6752861" VALUE="blue" TEXT="Blue" x-->
                    <!--xDROPDOWN TYPE="item" ID="6752862" VALUE="green" TEXT="Green" x-->
                    <!--xDROPDOWN TYPE="item" ID="6752863" VALUE="black" TEXT="Black" x-->
                    <!--xDROPDOWN TYPE="item" ID="6752864" VALUE="white" TEXT="White" x-->
                    <!--xCOMP_ENDx-->">
                        <h3><!--xCOMP TYPE="text" ID="250" DESC="Heading" x--></h3>
                    </div>
                    <div class="slp-lead">
                        <div class="displayimage <!--xCOMP TYPE="dropdown" ID="675277" GROUP="639877" DESC="Image style" DEFASSET="6100577" x-->
                        <!--xDROPDOWN TYPE="item" ID="6100477" VALUE="imageStyle0" TEXT="Do not display an image" x-->
                        <!--xDROPDOWN TYPE="item" ID="6100577" VALUE="imageStyle2" TEXT="Image Left Aligned" x-->
                        <!--xDROPDOWN TYPE="item" ID="6100677" VALUE="imageStyle1" TEXT="Image Right Aligned" x-->
                        <!--xDROPDOWN TYPE="item" ID="6100777" VALUE="imageStyle3" TEXT="Image Centred, sides clear" x-->
                        <!--xCOMP_ENDx-->">
                            <div class="popup-container-trigger">
                                <img src="resize_image.php?image=<!--xCOMP TYPE="image" ID="6666" DESC="Image (Max width 1200px)" RESOURCE="imageResources" x-->&max_width=<!--xCOMP TYPE="text" ID="32" DESC="Image width (Numeric characters only; height locked to max 1000px)" x-->&max_height=1000" alt="<!--xCOMP TYPE="text" ID="3905" DESC="Image Alt Text" x-->" />
                            </div>
                            <div class="image_caption slp-text-<!--xCOMP TYPE="dropdown" ID="675288" GROUP="675288" DESC="Image Caption Colour?" DEFASSET="6752881" x-->
                                <!--xDROPDOWN TYPE="item" ID="6752881" VALUE="blue" TEXT="Blue" x-->
                                <!--xDROPDOWN TYPE="item" ID="6752882" VALUE="green" TEXT="Green" x-->
                                <!--xDROPDOWN TYPE="item" ID="6752883" VALUE="black" TEXT="Black" x-->
                                <!--xDROPDOWN TYPE="item" ID="6752884" VALUE="white" TEXT="White" x-->
                            <!--xCOMP_ENDx-->">
                                <h4 style="text-align:center;"><!--xCOMP TYPE="text" id="252778" DESC="Image caption" x--></h4>
                            </div>
                        </div>
                        <div class="text-container">
                            <!--xCOMP TYPE="HTML" ID="25277" DESC="Body Copy" x-->
                        </div>
                        <div class="hidden popup-container-image"><!--xCOMP TYPE="REPEAT" ID="6666" x--></div>
                        <div class="popup-container hidden" data-popuptype="<!--xCOMP TYPE="dropdown" ID="675278" GROUP="639878" DESC="Popup Type" DEFASSET="8100471" x-->
                        <!--xDROPDOWN TYPE="item" ID="8100471" VALUE="" TEXT="None" x-->
                        <!--xDROPDOWN TYPE="item" ID="8100477" VALUE="video" TEXT="Video" x-->
                        <!--xDROPDOWN TYPE="item" ID="8100577" VALUE="image" TEXT="Image" x-->
                        <!--xCOMP_ENDx-->" data-value="<!--xCOMP TYPE="TEXT" ID="25278" DESC="Youtube Video URL (e.g. https://www.youtube.com/embed/RPwOGf1Xh5M)" x-->">
                            <div class="popup-container-content"></div>
                        </div>
                        <div class="slp-btn-wrapper" style="float: left;">
                            <a class="slp-btn-inline-center <!--xCOMP TYPE="dropdown" ID="675280" GROUP="639880" DESC="Show Button?" DEFASSET="6100580" x-->
                            <!--xDROPDOWN TYPE="item" ID="6398802" VALUE="hidden" TEXT="No, Dont Show Button" x-->
                            <!--xDROPDOWN TYPE="item" ID="6398801" VALUE="" TEXT="Yes, Show Button" x-->
                            <!--xCOMP_ENDx--> slp-btn-<!--xCOMP TYPE="dropdown" ID="675281" GROUP="639881" DESC="Button Colour" DEFASSET="6100581" x-->
                            <!--xDROPDOWN TYPE="item" ID="6398811" VALUE="blue" TEXT="Blue Button" x-->
                            <!--xDROPDOWN TYPE="item" ID="6398812" VALUE="green" TEXT="Green Button" x-->
                            <!--xCOMP_ENDx-->" href="<!--xCOMP TYPE="text" ID="675282" DESC="Button Link" x-->">
                            <!--xCOMP TYPE="text" ID="675283" DESC="Button Text" x-->
                            </a>
                        </div>
                    </div>
                </div>
                <div class="hidden-md col-sm-1"></div>
            </div>
        </div>
    </div>
    <!--xCICMS_ENDx-->
</div>

<div class="outerwrapper slp-padding-top-btm slp-bkg-<!--xCICMS_START TYPE="dropdown" ID="1075285" GROUP="1075285" DESC="Gallery Slider - Background Colour?" DEFASSET="10752851" x--->
    <!--xDROPDOWN TYPE="item" ID="10752851" VALUE="blue" TEXT="Blue" x-->
    <!--xDROPDOWN TYPE="item" ID="10752852" VALUE="green" TEXT="Green" x-->
    <!--xDROPDOWN TYPE="item" ID="10752853" VALUE="white" TEXT="White" x-->
<!--xCICMS_ENDx-->">
    <div class="<!--xCICMS_START TYPE="dropdown" ID="1075284" GROUP="1075284" DESC="Gallery Slider - Does this have Left Right Margins or Full Width?" DEFASSET="10752841" x--->
            <!--xDROPDOWN TYPE="item" ID="10752841" VALUE="container" TEXT="Left Right Margins" x-->
            <!--xDROPDOWN TYPE="item" ID="10752842" VALUE="container-fluid" TEXT="Full Width" x-->
        <!--xCICMS_ENDx-->">
        <div class="row">
            <div class="col-xs-12">
                <div class="slp-carousel-performance-wrapper">
                    <div id="slp-carousel-performance" class="carousel slide carousel-fade">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox"></div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#slp-carousel-performance" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#slp-carousel-performance" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="outerwrapper slp-bkg-white slp-bkg-<!--xCICMS_START TYPE="dropdown" ID="775285" GROUP="775285" DESC="Image Gallery - Show Background Image?" DEFASSET="7752852" x--->
    <!--xDROPDOWN TYPE="item" ID="7752852" VALUE="none" TEXT="No, Dont Show Background Image x-->
    <!--xDROPDOWN TYPE="item" ID="7752851" VALUE="img" TEXT="Yes, Show Background Image" x-->
<!--xCICMS_ENDx-->" style="background:url('<!--xCICMS_START TYPE="image" ID="775290" DESC="Image Gallery - Background Image (JPG 1200 x 800px)" RESOURCE="imageResources" x---><!--xCICMS_ENDx-->') no-repeat fixed 50% -30.8182px / cover;">
    <div class="slp-gallery-img-overlay slp-padding-top-btm">
        <div class="<!--xCICMS_START TYPE="dropdown" ID="775284" GROUP="775284" DESC="Image Gallery - Does this have Left Right Margins or Full Width?" DEFASSET="7752841" x--->
            <!--xDROPDOWN TYPE="item" ID="7752841" VALUE="container" TEXT="Left Right Margins" x-->
            <!--xDROPDOWN TYPE="item" ID="7752842" VALUE="container-fluid" TEXT="Full Width" x-->
        <!--xCICMS_ENDx-->">
            <div class="row">
                <div class="hidden-md col-sm-1"></div>
                <div class="col-md-12 col-sm-10 text-left">
                <!--xCICMS_START TYPE="comp" GROUP="3" DESC="Image Gallery Loop" DEFASSET="750" x--->
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="slp-gallery-image-wrapper slp-margin-top-btm">
                        <div class="popup-container-trigger" style="position:static;">
                            <img class="slp-gallery-image img-rounded" src="<!--xCOMP TYPE="image" ID="450" DESC="Gallery Image (JPG 1600 x 900px)" RESOURCE="imageResources" x-->" alt="<!--xCOMP TYPE="text" ID="750" DESC="Gallery Text" x-->">
                        </div>
                        <div class="hidden popup-container-image"><!--xCOMP TYPE="REPEAT" ID="450" x--></div>
                        <div class="popup-container hidden" data-popuptype="image">
                            <div class="popup-container-content"></div>
                        </div>
                    </div>
                </div>
                <!--xCICMS_ENDx-->
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        var i = 0;
        var html = '';
        $('.slp-gallery-image').each(function () {
           var curSrc = $(this).attr('src');
            var active = i == 0 ? ' active' : '';

            html += '<div class="item '+active+'" style="background:url('+curSrc+')"></div>';
            i++;
        });

        $('#slp-carousel-performance .carousel-inner').html(html);
//        for(var i=0 ; i< m.length ; i++) {
//            $('<div class="item"><img src="'+m[i]+'"><div class="carousel-caption"></div>   </div>').appendTo('.carousel-inner');
//            $('<li data-target="#carousel-example-generic" data-slide-to="'+i+'"></li>').appendTo('.carousel-indicators')
//
//        }
//        $('.item').first().addClass('active');
//        $('.carousel-indicators > li').first().addClass('active');
        $('#slp-carousel-performance').carousel();
    });
</script>

<!--xCICMS_START TYPE="newInclude" NAME="external/slp-footer.php" x---><!--xCICMS_ENDx-->