<!--xCICMS_START TYPE="newInclude" NAME="external/slp-header.php" x---><!--xCICMS_ENDx-->

<div class="outerwrapper slp-padding-top-btm slp-bkg-<!--xCICMS_START TYPE="dropdown" ID="1275285" GROUP="1275285" DESC="Login Form Background Colour?" DEFASSET="12752851" x--->
<!--xDROPDOWN TYPE="item" ID="12752851" VALUE="blue" TEXT="Blue" x-->
<!--xDROPDOWN TYPE="item" ID="12752852" VALUE="green" TEXT="Green" x-->
<!--xDROPDOWN TYPE="item" ID="12752853" VALUE="white" TEXT="White" x-->
<!--xCICMS_ENDx--> slp-text-<!--xCICMS_START TYPE="dropdown" ID="1275286" GROUP="1275286" DESC="Login Form Text Colour?" DEFASSET="12752861" x--->
<!--xDROPDOWN TYPE="item" ID="12752861" VALUE="blue" TEXT="Blue" x-->
<!--xDROPDOWN TYPE="item" ID="12752862" VALUE="green" TEXT="Green" x-->
<!--xDROPDOWN TYPE="item" ID="12752863" VALUE="black" TEXT="Black" x-->
<!--xDROPDOWN TYPE="item" ID="1275286" VALUE="white" TEXT="White" x-->
<!--xCICMS_ENDx-->">
    <div class="<!--xCICMS_START TYPE="dropdown" ID="1275284" GROUP="1275284" DESC="Does the login form have Left Right Margins or Full Width?" DEFASSET="12752841" x--->
    <!--xDROPDOWN TYPE="item" ID="12752841" VALUE="container" TEXT="Left Right Margins" x-->
    <!--xDROPDOWN TYPE="item" ID="12752842" VALUE="container-fluid" TEXT="Full Width" x-->
    <!--xCICMS_ENDx-->">
        <div class="row">
            <div class="col-xs-12">
                <div class="slp-heading">
                    <h3>Log in</h3>
                </div>
            </div>
            <div class="col-xs-12">
                <!--xCICMS_START TYPE="HTML" ID="125277" DESC="Body Copy" x---><!--xCICMS_ENDx-->
            </div>

            <div class="col-sm-8 col-sm-offset-2">
                <div class="alert alert-info fade in hidden divRegistered" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <p class="text-center" style="text-transform: uppercase;font-weight: bold;">Thank you for your registration.</p>
                    <p class="text-center">Please check the confirmation email for steps to activate your account.</p>
                    <ul></ul>
                </div>
                <div class="alert alert-info fade in hidden divActivated" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <p class="text-center" style="text-transform: uppercase;font-weight: bold;">Your account has been activated.</p>
                    <p class="text-center">Please log in using your username and password.</p>
                    <ul></ul>
                </div>
                <div class="alert alert-info fade in hidden divReset" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <p class="text-center" style="text-transform: uppercase;font-weight: bold;">Your account password has been successfully reset.</p>
                    <ul></ul>
                </div>
                <form id="frm_login" method="POST">
                    <div class="slp-gap-lg"></div>

                    <div class="form-group">
                        <label class="control-label" for="email">Email</label>
                        <div style="position: relative;">
                            <input type="text" class="form-control" name="email" id="email" placeholder="Email" data-required="yes">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="password">Password</label>
                        <div style="position: relative;">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password" data-required="yes">
                        </div>
                    </div>

                    <div class="slp-gap-sm"></div>

                    <div class="form-group">
                        <div class="alert alert-danger fade in err-message" role="alert" style="display: none;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Error(s):</strong><br><br>
                            <ul></ul>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="slp-btn-inline-center slp-btn-<!--xCICMS_START TYPE="dropdown" ID="1275281" GROUP="1275281" DESC="Submit Button - Colour" DEFASSET="12752811" x--->
                            <!--xDROPDOWN TYPE="item" ID="12752811" VALUE="blue" TEXT="Blue Button" x-->
                            <!--xDROPDOWN TYPE="item" ID="12752812" VALUE="green" TEXT="Green Button" x-->
                            <!--xCICMS_ENDx-->">
                                Submit
                            </button>
                        </div>
                        <div class="slp-gap-lg"></div>
                        <p class="p-form text-center"><a class="slp-btn-borderless-<!--xCICMS_START TYPE="dropdown" ID="1275287" GROUP="1275287" DESC="Forgot Password - Link Colour" DEFASSET="12752871" x--->
                            <!--xDROPDOWN TYPE="item" ID="12752871" VALUE="blue" TEXT="Blue Button" x-->
                            <!--xDROPDOWN TYPE="item" ID="12752872" VALUE="green" TEXT="Green Button" x-->
                            <!--xCICMS_ENDx-->" href="/forgot-password">Forgot Password?</a></p>
                        <div class="slp-gap-lg"></div>
                        <p class="p-form text-center"><a class="slp-btn-borderless-<!--xCICMS_START TYPE="dropdown" ID="1275290" GROUP="1275290" DESC="Register - Link Colour" DEFASSET="12752901" x--->
                            <!--xDROPDOWN TYPE="item" ID="12752901" VALUE="blue" TEXT="Blue Button" x-->
                            <!--xDROPDOWN TYPE="item" ID="12752902" VALUE="green" TEXT="Green Button" x-->
                            <!--xCICMS_ENDx-->" href="/register">Not registered?</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        var classCol = '';

        var blRegistered = helpers.getParameterByName('registered', window.location);
        if (blRegistered) {
            if ($('.divRegistered').hasClass('hidden')) {
                $('.divRegistered').removeClass('hidden');
            }
        }
        var blActivated = helpers.getParameterByName('activated', window.location);
        if (blActivated) {
            if ($('.divActivated').hasClass('hidden')) {
                $('.divActivated').removeClass('hidden');
            }
        }
        var blReset = helpers.getParameterByName('reset', window.location);
        if (blReset) {
            if ($('.divReset').hasClass('hidden')) {
                $('.divReset').removeClass('hidden');
            }
        }

        newFormHandler.formInit('frm_login', classCol);

        $('#frm_login').on('submit', function(){
            var self = $(this);
            var boolSuccess = newFormHandler.formCheckAllFieldNameIsEmpty('frm_login', classCol);

            if(boolSuccess && isDoneSubmitting){
                self.hide();
                self.parent().append(htmlLargeSpinner);

                var data = self.serializeArray();
                data.push({name: 'apikey', value: apikey});

                isDoneSubmitting = false;

                newFormHandler.postForm(urlLoginSubmit, data, function(r){
                    isDoneSubmitting = true;

                    if(r.error){
                        self.show();
                        $('.slp-spinner').remove();

                        $('.err-message ul').empty();
                        $('.err-message ul').append('<li>'+r.error+'</li>');
                        $('.err-message').css('display', 'block');
                    }
                    else {
                        clearUserSession();
                        storeJwt(r.jwt);
                        storeUser(r.user);

                        newFormHandler.resetForm('frm_login');
                        var link = helpers.getParameterByName('link', window.location);
                        if (link) {
                            window.location.href = link;
                        }
                        else {
                            window.location.href = urlDashboard;
                        }
                    }
                });
            }

            return false;
        });
    });
</script>

<!--xCICMS_START TYPE="newInclude" NAME="external/slp-footer.php" x---><!--xCICMS_ENDx-->