<!--xCICMS_START TYPE="newInclude" NAME="external/slp-header.php" x---><!--xCICMS_ENDx-->

<style>
    .carousel-inner .item {
        background-repeat: no-repeat;
        background-position: center center;
        -webkit-background-size: cover !important;
        -moz-background-size: cover !important;
        -o-background-size: cover !important;
        background-size: cover !important;
        background-attachment: inherit;
    }

    #slp-carousel .carousel-inner .carousel-caption:before {
        content: '';
        display: none;
    }

    #slp-carousel .carousel-inner .carousel-caption {
        background: transparent !important;
        z-index: 10 !important;
    }

    #slp-carousel .carousel-inner .new-overlay {
        width: 101%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        background: url("/cicms/includes/assets/img/slp/transparent-bkg.png") repeat 0 0;
        z-index: 1;
    }

    @media (min-width: 768px) {
        .carousel-inner {
            height: 600px !important;
        }
    }
    @media (max-width: 767px) {
        .carousel-caption  h1 {
            /*font-size: 15px;*/
            /*line-height: 15px;*/
            /*margin-top: 5px;*/
            /*margin-bottom: 5px;*/
            /*display: none !important;*/
        }
    }
</style>
<div class="container-fluid <!--xCICMS_START TYPE="dropdown" ID="675289" GROUP="675289" DESC="Show Feature Image?" DEFASSET="9100489" x--->
    <!--xDROPDOWN TYPE="item" ID="6752892" VALUE="hidden" TEXT="No, Dont Show Feature Image" x-->
    <!--xDROPDOWN TYPE="item" ID="6752891" VALUE="" TEXT="Yes, Show Feature Image" x-->
<!--xCICMS_ENDx-->">
    <div class="row">
        <div id="slp-carousel" class="carousel" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active" style="background-image:url('<!--xCICMS_START TYPE="image" ID="45" DESC="Feature Image (JPG 1200 x 800px)" RESOURCE="imageResources" x---><!--xCICMS_ENDx-->');" title="Feature Image">
                    <div class="new-overlay"></div>
                    <div class="container" style="height: 100%;position: relative;display: table;">
                        <div class="carousel-caption" style="position: relative;display: table-cell;vertical-align: middle; text-align: center;margin: 0;max-height: none;left: auto;right: auto;bottom: auto;padding: 15px;">
                            <h1><!--xCICMS_START TYPE="text" ID="551" DESC="Feature Image Caption" x---><!--xCICMS_ENDx--></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--xCICMS_START TYPE="comp" GROUP="222" DESC="Top Content" DEFASSET="850" x--->
<div class="outerwrapper slp-padding-top-btm slp-bkg-<!--xCOMP TYPE="dropdown" ID="875285" GROUP="875285" DESC="Top Content Background Colour?" DEFASSET="8752851" x-->
<!--xDROPDOWN TYPE="item" ID="8752851" VALUE="blue" TEXT="Blue" x-->
<!--xDROPDOWN TYPE="item" ID="8752852" VALUE="green" TEXT="Green" x-->
<!--xDROPDOWN TYPE="item" ID="8752853" VALUE="white" TEXT="White" x-->
<!--xCOMP_ENDx-->">
    <div class="<!--xCOMP TYPE="dropdown" ID="875284" GROUP="875284" DESC="Does the section have Left Right Margins or Full Width?" DEFASSET="8752841" x-->
    <!--xDROPDOWN TYPE="item" ID="8752841" VALUE="container" TEXT="Left Right Margins" x-->
    <!--xDROPDOWN TYPE="item" ID="8752842" VALUE="container-fluid" TEXT="Full Width" x-->
    <!--xCOMP_ENDx-->">
        <div class="row">
            <div class="hidden-md col-sm-1"></div>
            <div class="col-md-12 text-left">
                <div class="slp-heading <!--xCOMP TYPE="dropdown" ID="875279" GROUP="875279" DESC="Show Heading?" DEFASSET="8752791" x-->
                <!--xDROPDOWN TYPE="item" ID="8752791" VALUE="hidden" TEXT="No, Dont Show Heading" x-->
                <!--xDROPDOWN TYPE="item" ID="8752792" VALUE="" TEXT="Yes, Show Heading" x-->
                <!--xCOMP_ENDx--> slp-text-<!--xCOMP TYPE="dropdown" ID="875280" GROUP="875280" DESC="Heading Text Colour?" DEFASSET="8752801" x-->
                <!--xDROPDOWN TYPE="item" ID="8752801" VALUE="blue" TEXT="Blue" x-->
                <!--xDROPDOWN TYPE="item" ID="8752802" VALUE="green" TEXT="Green" x-->
                <!--xDROPDOWN TYPE="item" ID="8752803" VALUE="black" TEXT="Black" x-->
                <!--xDROPDOWN TYPE="item" ID="8752804" VALUE="white" TEXT="White" x-->
                <!--xCOMP_ENDx-->">
                    <h3><!--xCOMP TYPE="text" ID="850" DESC="Heading" x--></h3>
                </div>
                <div class="slp-lead">
                    <div class="displayimage <!--xCOMP TYPE="dropdown" ID="875277" GROUP="875277" DESC="Image style" DEFASSET="8752771" x-->
                    <!--xDROPDOWN TYPE="item" ID="8752771" VALUE="imageStyle0" TEXT="Do not display an image" x-->
                    <!--xDROPDOWN TYPE="item" ID="8752772" VALUE="imageStyle2" TEXT="Image Left Aligned" x-->
                    <!--xDROPDOWN TYPE="item" ID="8752773" VALUE="imageStyle1" TEXT="Image Right Aligned" x-->
                    <!--xDROPDOWN TYPE="item" ID="8752774" VALUE="imageStyle3" TEXT="Image Centred, sides clear" x-->
                    <!--xCOMP_ENDx-->">
                        <div class="popup-container-trigger">
                            <img src="resize_image.php?image=<!--xCOMP TYPE="image" ID="8666" DESC="Image (Max width 1200px)" RESOURCE="imageResources" x-->&max_width=<!--xCOMP TYPE="text" ID="8667" DESC="Image width (Numeric characters only; height locked to max 1000px)" x-->&max_height=1000" alt="<!--xCOMP TYPE="text" ID="8668" DESC="Image Alt Text" x-->" />
                        </div>
                        <div class="image_caption slp-text-<!--xCOMP TYPE="dropdown" ID="875288" GROUP="875288" DESC="Image Caption Colour?" DEFASSET="8752881" x-->
                            <!--xDROPDOWN TYPE="item" ID="8752881" VALUE="blue" TEXT="Blue" x-->
                            <!--xDROPDOWN TYPE="item" ID="8752882" VALUE="green" TEXT="Green" x-->
                            <!--xDROPDOWN TYPE="item" ID="8752883" VALUE="black" TEXT="Black" x-->
                            <!--xDROPDOWN TYPE="item" ID="8752884" VALUE="white" TEXT="White" x-->
                        <!--xCOMP_ENDx-->">
                            <h4 style="text-align:center"><!--xCOMP TYPE="text" id="852778" DESC="Image caption" x--></h4>
                        </div>
                    </div>
                    <div class="text-container">
                        <!--xCOMP TYPE="HTML" ID="852777" DESC="Body Copy" x-->
                    </div>
                    <div class="hidden popup-container-image"><!--xCOMP TYPE="REPEAT" ID="8666" x--></div>
                    <div class="popup-container hidden" data-popuptype="<!--xCOMP TYPE="dropdown" ID="875278" GROUP="875278" DESC="Popup Type" DEFASSET="8752781" x-->
                    <!--xDROPDOWN TYPE="item" ID="8752781" VALUE="" TEXT="None" x-->
                    <!--xDROPDOWN TYPE="item" ID="8752782" VALUE="video" TEXT="Video" x-->
                    <!--xDROPDOWN TYPE="item" ID="8752783" VALUE="image" TEXT="Image" x-->
                    <!--xCOMP_ENDx-->" data-value="<!--xCOMP TYPE="TEXT" ID="85278" DESC="Youtube Video URL (e.g. https://www.youtube.com/embed/RPwOGf1Xh5M)" x-->">
                        <div class="popup-container-content"></div>
                    </div>
                </div>
            </div>
            <div class="hidden-md col-sm-1"></div>
        </div>
    </div>
</div>
<!--xCICMS_ENDx-->

<!--xCICMS_START TYPE="comp" GROUP="2" DESC="Main Content" DEFASSET="250" x--->
<div class="outerwrapper slp-padding-top-btm slp-bkg-<!--xCOMP TYPE="dropdown" ID="675285" GROUP="675285" DESC="Main Content Background Colour?" DEFASSET="9100485" x-->
    <!--xDROPDOWN TYPE="item" ID="6752851" VALUE="blue" TEXT="Blue" x-->
    <!--xDROPDOWN TYPE="item" ID="6752852" VALUE="green" TEXT="Green" x-->
    <!--xDROPDOWN TYPE="item" ID="6752853" VALUE="white" TEXT="White" x-->
<!--xCOMP_ENDx-->">
    <div class="<!--xCOMP TYPE="dropdown" ID="675284" GROUP="675284" DESC="Does the section have Left Right Margins or Full Width?" DEFASSET="9100484" x-->
        <!--xDROPDOWN TYPE="item" ID="6752841" VALUE="container" TEXT="Left Right Margins" x-->
        <!--xDROPDOWN TYPE="item" ID="6752842" VALUE="container-fluid" TEXT="Full Width" x-->
    <!--xCOMP_ENDx-->">
        <div class="row">
            <div class="hidden-md col-sm-1"></div>
            <div class="col-md-12 col-sm-10 text-left">
                <div class="slp-heading <!--xCOMP TYPE="dropdown" ID="675279" GROUP="675279" DESC="Show Heading?" DEFASSET="9100477" x-->
                    <!--xDROPDOWN TYPE="item" ID="9100577" VALUE="hidden" TEXT="No, Dont Show Heading" x-->
                    <!--xDROPDOWN TYPE="item" ID="9100477" VALUE="" TEXT="Yes, Show Heading" x-->
                <!--xCOMP_ENDx--> slp-text-<!--xCOMP TYPE="dropdown" ID="675280" GROUP="675280" DESC="Heading Text Colour?" DEFASSET="6752801" x-->
                <!--xDROPDOWN TYPE="item" ID="6752801" VALUE="blue" TEXT="Blue" x-->
                <!--xDROPDOWN TYPE="item" ID="6752802" VALUE="green" TEXT="Green" x-->
                <!--xDROPDOWN TYPE="item" ID="6752803" VALUE="black" TEXT="Black" x-->
                <!--xDROPDOWN TYPE="item" ID="6752804" VALUE="white" TEXT="White" x-->
                <!--xCOMP_ENDx-->">
                    <h3><!--xCOMP TYPE="text" ID="250" DESC="Heading" x--></h3>
                </div>
                <div class="slp-lead">
                    <div class="displayimage <!--xCOMP TYPE="dropdown" ID="675277" GROUP="639877" DESC="Image style" DEFASSET="6100577" x-->
                        <!--xDROPDOWN TYPE="item" ID="6100477" VALUE="imageStyle0" TEXT="Do not display an image" x-->
                        <!--xDROPDOWN TYPE="item" ID="6100577" VALUE="imageStyle2" TEXT="Image Left Aligned" x-->
                        <!--xDROPDOWN TYPE="item" ID="6100677" VALUE="imageStyle1" TEXT="Image Right Aligned" x-->
                        <!--xDROPDOWN TYPE="item" ID="6100777" VALUE="imageStyle3" TEXT="Image Centred, sides clear" x-->
                    <!--xCOMP_ENDx-->">
                        <div class="popup-container-trigger">
                            <img src="resize_image.php?image=<!--xCOMP TYPE="image" ID="6666" DESC="Image (Max width 1200px)" RESOURCE="imageResources" x-->&max_width=<!--xCOMP TYPE="text" ID="32" DESC="Image width (Numeric characters only; height locked to max 1000px)" x-->&max_height=1000" alt="<!--xCOMP TYPE="text" ID="3905" DESC="Image Alt Text" x-->" />
                        </div>
                        <div class="image_caption slp-text-<!--xCOMP TYPE="dropdown" ID="675288" GROUP="675288" DESC="Image Caption Colour?" DEFASSET="9100488" x-->
                    <!--xDROPDOWN TYPE="item" ID="6752881" VALUE="blue" TEXT="Blue" x-->
                    <!--xDROPDOWN TYPE="item" ID="6752882" VALUE="green" TEXT="Green" x-->
                    <!--xDROPDOWN TYPE="item" ID="6752883" VALUE="black" TEXT="Black" x-->
                    <!--xDROPDOWN TYPE="item" ID="6752884" VALUE="white" TEXT="White" x-->
                    <!--xCOMP_ENDx-->">
                            <h4 style="text-align:center;"><!--xCOMP TYPE="text" id="252778" DESC="Image caption" x--></h4>
                        </div>
                    </div>
                    <div class="text-container">
                        <!--xCOMP TYPE="HTML" ID="25277" DESC="Body Copy" x-->
                    </div>
                    <div class="hidden popup-container-image"><!--xCOMP TYPE="REPEAT" ID="6666" x--></div>
                    <div class="popup-container hidden" data-popuptype="<!--xCOMP TYPE="dropdown" ID="675278" GROUP="639878" DESC="Popup Type" DEFASSET="8100471" x-->
                        <!--xDROPDOWN TYPE="item" ID="8100471" VALUE="" TEXT="None" x-->
                        <!--xDROPDOWN TYPE="item" ID="8100477" VALUE="video" TEXT="Video" x-->
                        <!--xDROPDOWN TYPE="item" ID="8100577" VALUE="image" TEXT="Image" x-->
                        <!--xCOMP_ENDx-->" data-value="<!--xCOMP TYPE="TEXT" ID="25278" DESC="Youtube Video URL (e.g. https://www.youtube.com/embed/RPwOGf1Xh5M)" x-->">
                        <div class="popup-container-content"></div>
                    </div>
                    <div class="slp-btn-wrapper" style="float: left;">
                        <a class="slp-btn-inline-center <!--xCOMP TYPE="dropdown" ID="639880" GROUP="639880" DESC="Show Button?" DEFASSET="6398801" x-->
                            <!--xDROPDOWN TYPE="item" ID="6398801" VALUE="hidden" TEXT="No, Dont Show Button" x-->
                            <!--xDROPDOWN TYPE="item" ID="6398802" VALUE="" TEXT="Yes, Show Button" x-->
                        <!--xCOMP_ENDx--> slp-btn-<!--xCOMP TYPE="dropdown" ID="675281" GROUP="639881" DESC="Button Colour" DEFASSET="6100581" x-->
                            <!--xDROPDOWN TYPE="item" ID="6398811" VALUE="blue" TEXT="Blue Button" x-->
                            <!--xDROPDOWN TYPE="item" ID="6398812" VALUE="green" TEXT="Green Button" x-->
                        <!--xCOMP_ENDx-->" href="<!--xCOMP TYPE="text" ID="675282" DESC="Button Link" x-->">
                            <!--xCOMP TYPE="text" ID="675283" DESC="Button Text" x-->
                        </a>
                    </div>
                </div>
            </div>
            <div class="hidden-md col-sm-1"></div>
        </div>
    </div>
</div>
<!--xCICMS_ENDx-->

<div class="outerwrapper  <!--xCICMS_START TYPE="dropdown" ID="1175289" GROUP="1175289" DESC="Multiple-Column - Show the section?" DEFASSET="11752892" x--->
    <!--xDROPDOWN TYPE="item" ID="11752892" VALUE="hidden" TEXT="No, Dont Show Multiple-Column Section" x-->
    <!--xDROPDOWN TYPE="item" ID="11752891" VALUE="" TEXT="Yes, Show Multiple-Column Section" x-->
<!--xCICMS_ENDx--> slp-padding-top-btm slp-bkg-<!--xCICMS_START TYPE="dropdown" ID="1075285" GROUP="1075285" DESC="Multiple-Column - Background Colour?" DEFASSET="10752851" x--->
    <!--xDROPDOWN TYPE="item" ID="10752851" VALUE="blue" TEXT="Blue" x-->
    <!--xDROPDOWN TYPE="item" ID="10752852" VALUE="green" TEXT="Green" x-->
    <!--xDROPDOWN TYPE="item" ID="10752853" VALUE="white" TEXT="White" x-->
<!--xCICMS_ENDx-->">
    <div class="<!--xCICMS_START TYPE="dropdown" ID="1075284" GROUP="1075284" DESC="Multiple-Column - Does this have Left Right Margins or Full Width?" DEFASSET="10752841" x--->
            <!--xDROPDOWN TYPE="item" ID="10752841" VALUE="container" TEXT="Left Right Margins" x-->
            <!--xDROPDOWN TYPE="item" ID="10752842" VALUE="container-fluid" TEXT="Full Width" x-->
        <!--xCICMS_ENDx-->">
        <div class="row auto-clear">
            <!--xCICMS_START TYPE="comp" GROUP="5" DESC="Multiple-Column Loop" DEFASSET="650" x--->
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="<!--xCOMP TYPE="dropdown" ID="575279" GROUP="575279" DESC="Show Heading?" DEFASSET="5752791" x-->
                    <!--xDROPDOWN TYPE="item" ID="5752791" VALUE="hidden" TEXT="No, Dont Show Heading" x-->
                    <!--xDROPDOWN TYPE="item" ID="5752792" VALUE="" TEXT="Yes, Show Heading" x-->
                <!--xCOMP_ENDx--> slp-text-<!--xCOMP TYPE="dropdown" ID="575280" GROUP="575280" DESC="Heading Text Colour?" DEFASSET="5752801" x-->
                <!--xDROPDOWN TYPE="item" ID="5752801" VALUE="blue" TEXT="Blue" x-->
                <!--xDROPDOWN TYPE="item" ID="5752802" VALUE="green" TEXT="Green" x-->
                <!--xDROPDOWN TYPE="item" ID="5752803" VALUE="black" TEXT="Black" x-->
                <!--xDROPDOWN TYPE="item" ID="5752804" VALUE="white" TEXT="White" x-->
                <!--xCOMP_ENDx-->">
                    <h3><!--xCOMP TYPE="text" ID="650" DESC="Column Heading" x--></h3>
                </div>
                <div class="text-container">
                    <!--xCOMP TYPE="HTML" ID="552777" DESC="Column Content" x-->
                </div>
                <div class="slp-btn-wrapper <!--xCOMP TYPE="dropdown" ID="1039880" GROUP="1039880" DESC="Show Column Button?" DEFASSET="10398802" x-->
                    <!--xDROPDOWN TYPE="item" ID="10398802" VALUE="hidden" TEXT="No, Dont Show Button" x-->
                    <!--xDROPDOWN TYPE="item" ID="10398801" VALUE="" TEXT="Yes, Show Button" x-->
                <!--xCOMP_ENDx-->">
                    <a class="slp-btn-inline-center slp-btn-<!--xCOMP TYPE="dropdown" ID="1075281" GROUP="1075281" DESC="Column Button Colour" DEFASSET="10752811" x-->
                        <!--xDROPDOWN TYPE="item" ID="10752811" VALUE="blue" TEXT="Blue Button" x-->
                        <!--xDROPDOWN TYPE="item" ID="10752812" VALUE="green" TEXT="Green Button" x-->
                    <!--xCOMP_ENDx-->" href="<!--xCOMP TYPE="text" ID="1075282" DESC="Column Button Link" x-->">
                        <!--xCOMP TYPE="text" ID="1075283" DESC="Column Button Text" x-->
                    </a>
                </div>
            </div>
            <!--xCICMS_ENDx-->
        </div>
    </div>
</div>

<div class="outerwrapper  <!--xCICMS_START TYPE="dropdown" ID="775289" GROUP="775289" DESC="Home Page Image Gallery - Show the section?" DEFASSET="7752892" x--->
    <!--xDROPDOWN TYPE="item" ID="7752892" VALUE="hidden" TEXT="No, Dont Home Page Image Gallery" x-->
    <!--xDROPDOWN TYPE="item" ID="7752891" VALUE="" TEXT="Yes, Show Home Page Image Gallery" x-->
<!--xCICMS_ENDx--> slp-bkg-white slp-bkg-<!--xCICMS_START TYPE="dropdown" ID="775285" GROUP="775285" DESC="Home Page Image Gallery - Show Background Image?" DEFASSET="7752852" x--->
    <!--xDROPDOWN TYPE="item" ID="7752852" VALUE="none" TEXT="No, Dont Show Background Image x-->
    <!--xDROPDOWN TYPE="item" ID="7752851" VALUE="img" TEXT="Yes, Show Background Image" x-->
<!--xCICMS_ENDx-->" style="background:url('<!--xCICMS_START TYPE="image" ID="775290" DESC="Home Page Image Gallery - Background Image (JPG 1200 x 800px)" RESOURCE="imageResources" x---><!--xCICMS_ENDx-->') no-repeat fixed 50% -30.8182px / cover;">
    <div class="slp-gallery-img-overlay slp-padding-top-btm">
        <div class="<!--xCICMS_START TYPE="dropdown" ID="775284" GROUP="775284" DESC="Home Page Image Gallery - Does this have Left Right Margins or Full Width?" DEFASSET="7752841" x--->
            <!--xDROPDOWN TYPE="item" ID="7752841" VALUE="container" TEXT="Left Right Margins" x-->
            <!--xDROPDOWN TYPE="item" ID="7752842" VALUE="container-fluid" TEXT="Full Width" x-->
        <!--xCICMS_ENDx-->">
            <div class="row">
                <!--xCICMS_START TYPE="comp" GROUP="3" DESC="Home Page Image Gallery Loop" DEFASSET="750" x--->
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="slp-gallery-image-wrapper slp-margin-top-btm">
                        <img class="slp-gallery-image" src="<!--xCOMP TYPE="image" ID="450" DESC="Gallery Image (JPG 1600 x 900px)" RESOURCE="imageResources" x-->" alt="<!--xCOMP TYPE="text" ID="750" DESC="Gallery Text" x-->">
                    </div>
                </div>
                <!--xCICMS_ENDx-->
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="slp-btn-wrapper ">
                        <a class="slp-btn-inline-center slp-btn-green" href="/gallery">View Gallery</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--xCICMS_START TYPE="newInclude" NAME="external/slp-footer.php" x---><!--xCICMS_ENDx-->