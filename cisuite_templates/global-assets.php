<h2>Global File Upload - Do NOT Reorder Global Assets</h2>

<!--xCICMS_START TYPE="comp" GROUP="218" DESC="File Uploads - Do NOT Reorder Uploaded Files" DEFASSET="151865" x--->
<img src="<!--xCOMP TYPE="image" ID="2180" DESC="<span style='color:#C00;'>NOTE: Do NOT Reorder Global Assets</span><br><br> &nbsp;&nbsp; 1. "Upload an Image" x-->"
alt="<!--xCOMP TYPE="text" ID="2181" DESC="Image Alt / Description" x-->" /><br />
<strong>Image Source (copy this URL to admin HTML editor):</strong> &lt;img src="<!--xCOMP TYPE="repeat" ID="2180" x-->"
alt="<!--xCOMP TYPE="repeat" ID="2181" x-->" /&gt;<br /><br /><hr /><br />
<hr />
<strong>File Description:</strong><!--xCOMP TYPE="text" ID="151865" DESC="2. File Uploads - File Name / Description (For sorting purposes)" x-->:
<strong>File:</strong><!--xCOMP TYPE="file" ID="2182" DESC="Upload a File eg. PDF, .doc, .xls etc" x-->
<strong>File Source (copy this URL to admin HTML editor):</strong> &lt;a href="<!--xCOMP TYPE="repeat" ID="2182" x-->"
alt="<!--xCOMP TYPE="repeat" ID="151865" x-->" /&gt;<br /><br /><hr /><br />

<hr /><br /><br />
<!--xCICMS_ENDx-->

<!--xCICMS_START TYPE="text" ID="10000" DESC="Login Link" x---><!--xCICMS_ENDx-->

<!--xCICMS_START TYPE="html" ID="10001" DESC="Address" x---><!--xCICMS_ENDx-->
<!--xCICMS_START TYPE="text" ID="20001" DESC="Google Map Address" x---><!--xCICMS_ENDx-->
<!--xCICMS_START TYPE="html" ID="10002" DESC="Phone" x---><!--xCICMS_ENDx-->
<!--xCICMS_START TYPE="html" ID="10003" DESC="Email" x---><!--xCICMS_ENDx-->
<!--xCICMS_START TYPE="text" ID="10004" DESC="Facebook Link" x---><!--xCICMS_ENDx-->
<!--xCICMS_START TYPE="text" ID="10005" DESC="Twitter Link" x---><!--xCICMS_ENDx-->
<!--xCICMS_START TYPE="text" ID="10006" DESC="Pinterest Link" x---><!--xCICMS_ENDx-->
<!--xCICMS_START TYPE="text" ID="10007" DESC="Privacy Policy Link" x---><!--xCICMS_ENDx-->
<!--xCICMS_START TYPE="text" ID="10008" DESC="Terms and Conditions Link" x---><!--xCICMS_ENDx-->
<!--xCICMS_START TYPE="text" ID="10009" DESC="Apple App Link" x---><!--xCICMS_ENDx-->
<!--xCICMS_START TYPE="text" ID="10010" DESC="Android App Link" x---><!--xCICMS_ENDx-->
<!--xCICMS_START TYPE="text" ID="10011" DESC="Footer Copyright 1 (e.g. All design and content Copyright 2000 - 2018 Streamline)" x---><!--xCICMS_ENDx-->
<!--xCICMS_START TYPE="text" ID="10012" DESC="Footer Copyright 2 (e.g. Architectural Solutions Pty Ltd All rights reserved)" x---><!--xCICMS_ENDx-->