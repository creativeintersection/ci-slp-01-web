<!--xCICMS_START TYPE="newInclude" NAME="internal/slp-header.php" x---><!--xCICMS_ENDx-->

<div class="outerwrapper slp-padding-top-btm slp-bkg-<!--xCICMS_START TYPE="dropdown" ID="1275285" GROUP="1275285" DESC="Settings Form Background Colour?" DEFASSET="12752851" x--->
<!--xDROPDOWN TYPE="item" ID="12752851" VALUE="blue" TEXT="Blue" x-->
<!--xDROPDOWN TYPE="item" ID="12752852" VALUE="green" TEXT="Green" x-->
<!--xDROPDOWN TYPE="item" ID="12752853" VALUE="white" TEXT="White" x-->
<!--xCICMS_ENDx--> slp-text-<!--xCICMS_START TYPE="dropdown" ID="1275286" GROUP="1275286" DESC="Settings Form Text Colour?" DEFASSET="12752861" x--->
<!--xDROPDOWN TYPE="item" ID="12752861" VALUE="blue" TEXT="Blue" x-->
<!--xDROPDOWN TYPE="item" ID="12752862" VALUE="green" TEXT="Green" x-->
<!--xDROPDOWN TYPE="item" ID="12752863" VALUE="black" TEXT="Black" x-->
<!--xDROPDOWN TYPE="item" ID="1275286" VALUE="white" TEXT="White" x-->
<!--xCICMS_ENDx-->">
    <div class="<!--xCICMS_START TYPE="dropdown" ID="1275284" GROUP="1275284" DESC="Does the settings form have Left Right Margins or Full Width?" DEFASSET="12752841" x--->
    <!--xDROPDOWN TYPE="item" ID="12752841" VALUE="container" TEXT="Left Right Margins" x-->
    <!--xDROPDOWN TYPE="item" ID="12752842" VALUE="container-fluid" TEXT="Full Width" x-->
    <!--xCICMS_ENDx-->">
        <div class="row">
            <div class="col-xs-12">
                <div class="slp-heading slp-text-<!--xCICMS_START TYPE="dropdown" ID="675286" GROUP="675286" DESC="Heading Text Colour?" DEFASSET="6752861" x--->
                    <!--xDROPDOWN TYPE="item" ID="6752861" VALUE="blue" TEXT="Blue" x-->
                    <!--xDROPDOWN TYPE="item" ID="6752862" VALUE="green" TEXT="Green" x-->
                    <!--xDROPDOWN TYPE="item" ID="6752863" VALUE="black" TEXT="Black" x-->
                    <!--xDROPDOWN TYPE="item" ID="6752864" VALUE="white" TEXT="White" x-->
                <!--xCICMS_ENDx-->">
                    <h3><!--xCICMS_START TYPE="text" ID="250" DESC="Heading" x---></h3>
                </div>
            </div>
            <div class="col-xs-12">
                <!--xCICMS_START TYPE="HTML" ID="125277" DESC="Body Copy" x---><!--xCICMS_ENDx-->
            </div>

            <div class="col-sm-8 col-sm-offset-2">
                <form id="frm_settings" method="POST">
                    <div class="slp-gap-lg"></div>

                    <div class="form-group">
                        <label for="username" class="control-label">Name</label>
                        <div style="position: relative;">
                            <input type="text" class="form-control" id="username" name="username" placeholder="Name" data-required="yes">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="phone" class="control-label">Contact Number</label>
                        <div style="position: relative;">
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Contact Number" data-required="no">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="control-label">Email</label>
                        <div style="position: relative;">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" data-required="yes">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="control-label">Password</label>
                        <div style="position: relative;">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password" data-required="no" value="">
                        </div>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input id="is_pushed" type="checkbox" name="is_pushed"> Push Notifications
                        </label>
                    </div>

                    <div class="slp-gap-lg"></div>

                    <div class="form-group">
                        <div class="alert alert-danger fade in err-message" role="alert" style="display: none;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Error(s):</strong><br><br>
                            <ul></ul>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="slp-btn-inline-center slp-btn-<!--xCICMS_START TYPE="dropdown" ID="1275281" GROUP="1275281" DESC="Submit Button Colour" DEFASSET="12752811" x--->
                            <!--xDROPDOWN TYPE="item" ID="12752811" VALUE="blue" TEXT="Blue Button" x-->
                            <!--xDROPDOWN TYPE="item" ID="12752812" VALUE="green" TEXT="Green Button" x-->
                            <!--xCICMS_ENDx-->">
                            <!--xCICMS_START TYPE="text" ID="1275283" DESC="Submit Button Text" x---><!--xCICMS_ENDx-->
                            </button>
                            <button type="button" class="slp-btn-inline-center slp-btn-<!--xCICMS_START TYPE="dropdown" ID="1275282" GROUP="1275282" DESC="Cancel Button Colour" DEFASSET="12752821" x--->
                            <!--xDROPDOWN TYPE="item" ID="12752821" VALUE="blue" TEXT="Blue Button" x-->
                            <!--xDROPDOWN TYPE="item" ID="12752822" VALUE="green" TEXT="Green Button" x-->
                            <!--xCICMS_ENDx--> btnCancel">
                            <!--xCICMS_START TYPE="text" ID="1275287" DESC="Cancel Button Text" x---><!--xCICMS_ENDx-->
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="slp-gap-lg"></div>
            <div class="slp-gap-lg"></div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        var jwt = getJwt();
        var classCol = '';

        newFormHandler.getForm(urlSettingsGetSubmit, {
            apikey : apikey,
            jwt : jwt
        }, function(r) {

            var settingsUsername = r['username'];
            var settingsPhone = r['phone'];
            var settingsEmail = r['email'];
            var settingsIsPushed = parseInt(r['isPushed']) == 1 ? true : false;

            $('#username').val(settingsUsername);
            $('#phone').val(settingsPhone);
            $('#email').val(settingsEmail);
            $('#is_pushed').prop('checked', settingsIsPushed);
        });

        newFormHandler.formInit('frm_settings', classCol);

        $('#frm_settings').on('submit', function(){
            var self = $(this);
            var boolSuccess = newFormHandler.formCheckAllFieldNameIsEmpty('frm_settings', classCol);

            if(boolSuccess && isDoneSubmitting){
                self.hide();
                self.parent().append(htmlLargeSpinner);

                var data = self.serializeArray();
                data.push({name: 'apikey', value: apikey});
                data.push({name: 'jwt', value: apikey});

                isDoneSubmitting = false;

                newFormHandler.postForm(urlSettingsGetSubmit, data, function(r){
                    isDoneSubmitting = true;
                    self.show();
                    $('.slp-spinner').remove();

                    if(r.error){
                        $('.err-message ul').empty();
                        $('.err-message ul').append('<li>'+r.error+'</li>');
                        $('.err-message').css('display', 'block');
                    }
                    else {
                        alert('Settings is successfully updated.');
                    }
                });
            }

            return false;
        });

        $('.btnCancel').on('click', function () {
            window.location.reload();
        });
    });
</script>

<!--xCICMS_START TYPE="newInclude" NAME="internal/slp-footer.php" x---><!--xCICMS_ENDx-->