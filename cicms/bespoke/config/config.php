<?php

return array(
    'env' 			=> 'development',
    'baseURL'       => 'https://www.streamlineplus.com.au',
    'baseDomain'    => 'streamlineplus.com.au',

    // ciAuthServer Config
    'jwt' => array(
        'getUrl'              => 'http://192.168.2.110/jwt/',
        'verifyUrl'           => 'http://192.168.2.110/jwt/verify',
        'key'                 => '9IYBpTj/O+fsgVUDIbm2h3m76B+sKb1d5QqNmrGMgsLtbZJ1ah7vY94MZKmkyMUzL6xpnLqZSjIkFPzgLyQEnQ==', // key to verify token
        'headerAuthorization' => 'Basic Y2lzbHAwMTpYWm5YZUxxdHhn', // in header for getting and verifying jwt
        'headerApiKey'        => 'B24D2EF7B0584934AB75DA4AFEF4AAB2', // in header for getting and verifying jwt
        'algorithm'           => 'RS256' // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
    ),

    // Streamline Plus Database Config
    'apiDatabase' => array(
        'host'     => 'localhost', // Database host
        'port'     => '3306', // Default Port
        'user'     => 'streamli_admin', // Database username
        'password' => 'F@4p!QlBJ&#n', // Database password
        'name'     => 'streamli_api' // Database schema name 
    ),

    // ciSuite Database Config
    'cisuiteDatabase' => array(
        'host'     => 'localhost', // Database host
        'port'     => '3306', // Default Port
        'user'     => 'streamli_admin', // Database username
        'password' => 'F@4p!QlBJ&#n', // Database password
        'name'     => 'streamli_cisuite' // Database schema name
    ),

    'applePushNotifications' => array(
        'issuer'           => '72RE3F7QQF',
        'audience'         => 'streamlineplus.com.au',
        'uniqueidentifier' => '9IYBpTj/O+fsgVUDIbm2',
        'key'              => '553T24J339',
        'bundle_id'        => 'au.com.streamlineplus.StreamlinePLUS',

        'apikey'=>'AAAAKP0P2y8:APA91bHoXomddEHH5WCbYMU3UQ-lwbO2v02RheRIIlAdmC0gt0RVtN6MIGZv_u6rDZRnanRf_sniydGKYsqsbpCZI7_rcNiO60p5ZHCWx-2w6NWu2L62TItFu3vhEsbriLDn3dHSwmYz'
    ),

    'androidPushNotifications' => array(
        'issuer'           => '72RE3F7QQF',
        'audience'         => 'streamlineplus.com.au',
        'uniqueidentifier' => '9IYBpTj/O+fsgVUDIbm2',
        'key'              => '553T24J339',
        'bundle_id'        => 'au.com.streamlineplus.StreamlinePLUS',

        'apikey'=>'AAAAJQZhgHg:APA91bEztuJ2GMThwBFYISOrHXy-gPggc5MoXc4UaTTv37i9hnp500hl5lDhx61jRckeJdfeKVQJ2ArdjZZlnSUVFaFBhQcLemgQnIoKG2CrFQiCwhl0cFMtdeQM0ePDLE9m3bZoemD9'
    ),

//    'iOSUrl'     => 'http://192.168.2.110/pns/dev/apns/send',
    'iOSUrl'     => 'http://192.168.2.110/pns/apns/send',
    'androidUrl' => 'http://192.168.2.110/pns/firebase/send'
);