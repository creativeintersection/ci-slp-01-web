<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?= isset($pageTitle) && !empty($pageTitle) ? $pageTitle : 'Streamline Plus' ?></title>

    <!-- Bootstrap -->
    <link href="/cicms/includes/assets/css/slp-library/jquery-ui-1.12.1.min.css" rel="stylesheet">
    <link href="/cicms/includes/assets/css/slp-library/bootstrap-3.3.7.min.css" rel="stylesheet">
    <link href="/cicms/includes/assets/css/slp-library/font-awesome-4.6.3.min.css" rel="stylesheet">
    <link href="/cicms/includes/assets/css/slp-custom/app.css" rel="stylesheet">

    <!-- favicons - start -->
    <!--<link rel="apple-touch-icon" sizes="180x180" href="/bookit/assets/img/favicons/apple-touch-icon.png">-->
    <!--<link rel="icon" type="image/png" sizes="32x32" href="/bookit/assets/img/favicons/favicon-32x32.png">-->
    <!--<link rel="icon" type="image/png" sizes="16x16" href="/bookit/assets/img/favicons/favicon-16x16.png">-->
    <!--<link rel="manifest" href="/bookit/assets/img/favicons/manifest.json">-->
    <!--<link rel="mask-icon" href="/bookit/assets/img/favicons/safari-pinned-tab.svg" color="#74001e">-->
    <!--<link rel="shortcut icon" href="/bookit/assets/img/favicons/favicon.ico">-->
    <!--<meta name="msapplication-config" content="/bookit/assets/img/favicons/browserconfig.xml">-->
    <!--<meta name="theme-color" content="#74001e">-->
    <!-- favicons - end -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Common Scripts -->
    <script src="/cicms/includes/assets/js/slp-library/jquery-1.12.4.min.js"></script>
    <script src="/cicms/includes/assets/js/slp-library/jquery-ui-1.12.1.min.js"></script>
    <script src="/cicms/includes/assets/js/slp-library/bootstrap-3.3.7.min.js"></script>
    <script src="/cicms/includes/assets/js/slp-custom/app.js"></script>
    
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <script>
        $(function () {
            $('body').load('/cisuite-index.php?preview=34&pid=<?=$pageid?>', function(){
                $(this).fadeIn('slow');
            });
        });
    </script>
</head>
<body></body>
</html>