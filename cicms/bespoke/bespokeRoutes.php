<?php

//session_cache_limiter(false);
//session_start();
//session_regenerate_id();
//error_reporting(E_ALL);

$configVar = include(dirname(__FILE__) . '/config/config.php');

global $env;
$env = $configVar['env'];

global $debugEnabled;
$debugEnabled = ($env !== 'production');

global $slpBaseUrl;
$slpBaseUrl = $configVar['baseURL'];

global $slpBaseDomain;
$slpBaseDomain = $configVar['baseDomain'];

global $tzLocal;
$tzLocal = 'Australia/Brisbane';

define('BASE_DIR', dirname(__FILE__) . '/');

define('EXTERNAL_GROUP_ID', '1');
define('INTERNAL_GROUP_ID', '2');

define('NEWS_SECTION_ID', '100');

define('APP_MENU_PAGE_ID', '214');
define('APP_MENU_GROUP_ID', '1');
define('APP_MENU_NEWS_ID', '8');

define('MENU_ITEM_TITLE_ID', '1');
define('MENU_ITEM_LINK_ID', '2');
define('MENU_ITEM_UNIQUE_ID', '5');

define('RECORD_TEMPLATE_ID', '1');
define('RECORD_CATEGORY_ID', '49');

define('FIELD_USERNAME_ID', '100');
define('FIELD_PHONE_ID', '700');
define('FIELD_EMAIL_ID', '1900');
define('FIELD_PASSWORD_ID', '600');
define('FIELD_ANDROID_TOKEN_ID', '2000');
define('FIELD_IOS_TOKEN_ID', '2100');
define('FIELD_HASH_ID', '2200');
define('FIELD_PUSH_NOTIFIED', '2300');

require_once 'BespokeController.php';
global $bespokeController;
$bespokeController = new BespokeController();

function getCallerDeviceType() {
    $app = \Slim\Slim::getInstance();

    // Fetch any new or cached value
    $deviceType = isset($_REQUEST['device_type'])
        ? trim($_REQUEST['device_type'])
        : $app->getCookie('deviceType')
    ;

    return $deviceType;
}


function isAppRequest($deviceTypeOrPlatform = null) {
//    if(empty($deviceTypeOrPlatform)) {
    if($deviceTypeOrPlatform === null) {
        $deviceTypeOrPlatform = getCallerDeviceType();
    }

    return (filter_var($deviceTypeOrPlatform, FILTER_VALIDATE_INT) === false)
        ? in_array($deviceTypeOrPlatform, array('android', 'ios'))
        : in_array(strval($deviceTypeOrPlatform), array('0', '1'))
        ;
}

// NOTE: this method modifies the source $viewData parameter
function includeCommonViewData(&$viewData, $user = null, $jwt = null) {
    global $bespokeController;
    $usingApp = isAppRequest();

    // Always include this
    if (!isset($appData['usingApp'])) {
        $viewData['usingApp'] = $usingApp;
    }

    // NOTE: App Edition does not have these in localStorage
    if ($usingApp) {
        if (!isset($viewData['appJWT'])) {
            if (empty($jwt) && isset($_SESSION['slp']['jwt'])) {
                $jwt = $_SESSION['slp']['jwt'];
            }

            if (!empty($jwt)) {
                $viewData['appJWT'] = $jwt;
            }
        }

        if (!isset($viewData['appUser'])) {
            if (empty($user) && isset($_SESSION['slp']['userId'])) {
                $user = $bespokeController->getSingleUserByUserId($_SESSION['slp']['userId']);
            }

            if (!empty($user)) {
                // TODO: determine why transformUser is not found... the function is there !!! - dalesmckay
                //$viewData['appUser'] = $bespokeController->transformUser($user);
                $viewData['appUser'] = array(
                    'userId'       => $user->cprID,
                    'username'     => $bespokeController->getUserFieldValue($user->cprID, FIELD_USERNAME_ID),
                    'phone'        => $bespokeController->getUserFieldValue($user->cprID, FIELD_PHONE_ID),
                    'email'        => $bespokeController->getUserFieldValue($user->cprID, FIELD_EMAIL_ID)
                );
            }
        }
    }
}


function isAjaxRequest() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest');
}


// IMPORTANT: this method should be used rather than $app->render as it sets up data to support AppEdition Views
function renderView($template, $data = array(), $status = null) {
    $app = \Slim\Slim::getInstance();

    if(empty($data)) {
        $data = array();
    }

    includeCommonViewData($data);

    $app->render($template, $data, $status);
}


function sendJsonResponse($payload) {
    header('Content-Type: application/json');
    echo json_encode($payload ?: array());

    if (isAjaxRequest()) {
        exit;
    }
}


// IMPORTANT: this method should be used to send NOT_FOUND responses as it differentiates between ajax & browser requests
function sendNotFoundResponse() {
    if (isAjaxRequest()) {
        // NOTE: Ajax requests should NOT return rendered content !!!
        sendJsonResponse(array('error' => 'not found'));
    }
    else {
        $app = \Slim\Slim::getInstance();

        $app->redirect('/not-found');
    }

    exit;
}


class CIAppDetectorMiddleware extends \Slim\Middleware
{
    public function call() {
        //global $debugEnabled;

        $deviceType   = getCallerDeviceType();
        $isAppRequest = isAppRequest($deviceType);

        // Ensure the cookie is up to date
        if($isAppRequest) {
            $this->app->setCookie('deviceType', $deviceType, '10 years'); // Long life cookie
            $_SESSION['slp']['deviceType'] = $deviceType;
        }
        else {
            $this->app->deleteCookie('deviceType');
        }

        $this->next->call();
    }
}

$app->config('settings.determineRouteBeforeAppMiddleware', true);
// Mount the middleware (this middleware is executed before every request)
$app->add(new CIAppDetectorMiddleware(array()));


function setClearSessionVars($jwt) {
    global $bespokeController;

    $authenticated = $bespokeController->validateJwt($jwt);
    if(empty($authenticated) || isset($authenticated['error'])) {
        session_destroy();

        return false;
    }

    $userId = $authenticated['payload']['userId'];

    $_SESSION['slp']['jwt']          = $authenticated['jwt'];
    $_SESSION['slp']['userId']       = $userId;
    $_SESSION['slp']['username']     = $authenticated['payload']['username'];

    return $authenticated;
}

function validateAPIKey() {
    global $bespokeController;

    $apiKey = isset($_REQUEST['apikey']) ? trim($_REQUEST['apikey']) : '';

    if(empty($apiKey)) {
        $bespokeController->throwError(8);
        exit;
    }

    return $bespokeController->validateAPIKey($apiKey);
}

function validateJwt() {
    global $bespokeController;

    $app = \Slim\Slim::getInstance();

    if (isset($_SESSION['slp']['jwt'])) {
        $jwt = $_SESSION['slp']['jwt'];
        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
            session_regenerate_id();
        }
        return setClearSessionVars($jwt);
    }

    if (isset($_REQUEST['jwt']) && !empty($_REQUEST['jwt'])) {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
            session_regenerate_id();
        }
        return setClearSessionVars($_REQUEST['jwt']);
    }

    $actualLink = $_SERVER['REQUEST_URI'];

    if (isAjaxRequest()) {
//        $bespokeController->throwError(12);
//        exit;
        sendJsonResponse(array('error' => 'Invalid Token'));
    }
    else {
        $app->redirect('/login?link=' . urlencode($actualLink));
    }

    return false;
}

function checkBespokeLogin() {
    $app = \Slim\Slim::getInstance();

    if (isset($_SESSION['slp']['jwt'])) {
        if (setClearSessionVars($_SESSION['slp']['jwt'])) {
            if (!empty($_REQUEST['link'])) {
                $app->redirect($_REQUEST['link']);
            }
            else {
                $app->redirect('/dashboard');
            }
        }
    }

    if (isset($_REQUEST['jwt']) && !empty($_REQUEST['jwt'])) {
        if (setClearSessionVars($_REQUEST['jwt'])) {
            if (!empty($_REQUEST['link'])) {
                $app->redirect($_REQUEST['link']);
            }
            else {
                $app->redirect('/dashboard');
            }
        }
    }
}

// Start routing

//$app->get('/dashboard', 'validateJwt', function () use ($app, $bespokeController) {
//    if(isAppRequest()) {
//        $app->redirect('/account-support/menu');
//
//        return;
//    }
//
//    renderView('dashboard.php',array(
//        'pageTitle'=>'Dashboard'
//    ));
//});

$app->group('/slp', function() use($app, $engine, $bespokeController) {

    $app->get('/verify', function () use ($app, $bespokeController) {
        $userId = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
        if (empty($userId)) {
            $bespokeController->customErrorMessage('Missing user ID');
            exit;
        }

        $hash = isset($_REQUEST['hash']) ? $_REQUEST['hash'] : '';
        if (empty($hash)) {
            $bespokeController->customErrorMessage('Missing hash');
            exit;
        }

        $app->redirect('/activation'
            .'?user_id='.$userId
            .'&hash='.$hash
        );
    });
    
    $app->group('/api', 'validateAPIKey', function () use ($app, $bespokeController) {

        $app->group('/auth', function () use ($app, $bespokeController) {

            // submit login form
            $app->post('/login', function () use ($app, $bespokeController) {
                $loginCredentials = $bespokeController->loginAsUser($_POST);
                $userId           = $loginCredentials['user']['userId'];
                $username         = $loginCredentials['user']['username'];
                $appMenuModules   = $bespokeController->getAppMenuModules();

                $loginCredentials['modules'] = json_decode($appMenuModules);

                if (empty($_SESSION['slp'])) {
                    $_SESSION['slp'] = array();
                }

                $_SESSION['slp']['jwt']          = $loginCredentials['jwt'];
                $_SESSION['slp']['userId']       = $userId;
                $_SESSION['slp']['username']     = $username;
                
                sendJsonResponse($loginCredentials);
            });

            // submit register form
            $app->post('/register', function () use ($app, $bespokeController) {

                $_POST['record_template'] = RECORD_TEMPLATE_ID; // using default Records Template of Users
                $_POST['record_category'] = RECORD_CATEGORY_ID; // using default Records Category of Registered Users

                $userId = $bespokeController->createUser($_POST);
                // $userid = $engine->newCIPartnerUser($_POST); // POST /cicms/admin/cipartner/user/new

                // GET /cicms/admin/cipartner/user/:userId/edit/fields
//            $ProductData = $engine->templatesHandler->GetCIPartnerUserData($userid)[0];
//            $ProductFields = $engine->templatesHandler->getCIPartnerUserFields($ProductData['cpdFields']);
//
//            $groupoptions = $engine->getCIPARTNERGroupOptionsData($userid);

                // POST /cicms/admin/cipartner/user/:userId/edit/fields
//            $engine->editCIPartnerUserValues($productid,$_POST);
//            $app->redirect('/cicms/admin/cipartner/user/'.$productid.'/edit#productfields');

                sendJsonResponse(array('success' => $userId));
            });

            // authenticated route
            $app->post('/authenticated', function () use ($app, $bespokeController) {
//            $jwt = $bespokeController->getBearerToken($bespokeController->getAuthorizationHeader($_SERVER));
//            sendJsonResponse($jwt);
                $response = setClearSessionVars($_POST['jwt']);
                $authenticated = $response ? true : false;
                sendJsonResponse(array('authenticated'=>$authenticated,'user'=>$response['payload'],'jwt'=>$response['jwt']));
            });

            $app->post('/forgot-password', function() use($app,$bespokeController){
                $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
                if (empty($email)) {
                    $bespokeController->customErrorMessage('Missing email address');
                    exit;
                }

                $email = str_replace(' ', '+', $email);
                $user = $bespokeController->generatePasswordResetEmail($email);

                sendJsonResponse(array('success'=>$user));
            });

            $app->post('/reset-password', function() use($app,$bespokeController){
                $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
                if (empty($email)) {
                    $bespokeController->customErrorMessage('Missing email address');
                    exit;
                }
                $email = str_replace(' ', '+', $email);

                $hash = isset($_REQUEST['hash']) ? $_REQUEST['hash'] : '';
                if (empty($hash)) {
                    $bespokeController->customErrorMessage('Missing hash');
                    exit;
                }

                $bespokeController->confirmHashForResetPassword($email, $hash);

                $password = isset($_REQUEST['password']) ? $_REQUEST['password'] : '';
                $vrfPassword = isset($_REQUEST['vrf_password']) ? $_REQUEST['vrf_password'] : '';

                if (empty($password)) {
                    $bespokeController->customErrorMessage('Missing password');
                    exit;
                }

                if ($password !== $vrfPassword) {
                    $this->throwError(19);exit;
                }

                $bespokeController->updateNewPassword($email, $password);

                sendJsonResponse(array('success'=>'success'));
            });
        });

        /**
         * logout for the app
         */
        $app->post('/logout', function () use ($app, $bespokeController) {
            if (isset($_POST['deviceType']) && $_POST['deviceType'] != '' && isset($_POST['userid']) && !empty($_POST['userid'])) {
                $bespokeController->clearCiPartnerToken($_POST);

                // Initialize the session.
                // If you are using session_name("something"), don't forget it now!
                session_start();

                // Unset all of the session variables.
                $_SESSION = array();

                // If it's desired to kill the session, also delete the session cookie.
                // Note: This will destroy the session, and not just the session data!
                if (ini_get("session.use_cookies")) {
                    $params = session_get_cookie_params();
                    setcookie(session_name(), '', time() - 42000,
                        $params["path"], $params["domain"],
                        $params["secure"], $params["httponly"]
                    );
                }

                // Finally, destroy the session.
                session_destroy();

                sendJsonResponse(array('success'=>'APP logged out.'));
            }
            else {
                sendJsonResponse(array('error'=>'Device Type or User ID is missing.'));
            }
        });

        /**
         * logout for the web
         */
        $app->get('/logout', function () use ($app, $bespokeController) {
            unset($_SESSION['slp']);
            $_SESSION['slp'] = '';
            sendJsonResponse(array('success'=>'log out successfully'));
        });
        
        $app->post('/activation', function () use ($app, $bespokeController) {
            $userId = isset($_POST['userId']) ? $_POST['userId'] : '';
            if (empty($userId)) {
                $bespokeController->customErrorMessage('Missing user ID');
                exit;
            }

            $hash = isset($_POST['hash']) ? $_POST['hash'] : '';
            if (empty($hash)) {
                $bespokeController->customErrorMessage('Missing hash');
                exit;
            }

            $success = $bespokeController->confirmUserHash($userId, $hash);

            sendJsonResponse(array('success'=>$success));
        });

        $app->group('/jwt', 'validateJwt', function () use ($app, $bespokeController) {

            $app->get('/settings', function () use ($app, $bespokeController) {
                global $slpBaseUrl;
                $userId = $_SESSION['slp']['userId'];

                $phone = $bespokeController->getUserFieldValue($userId, FIELD_PHONE_ID);
                $email = $bespokeController->getUserFieldValue($userId, FIELD_EMAIL_ID);
                $isPushed = $bespokeController->getUserFieldValue($userId, FIELD_PUSH_NOTIFIED);

                sendJsonResponse(array(
                    'username'  => $_SESSION['slp']['username'],
                    'phone'     => $phone,
                    'email'     => $email,
                    'isPushed'  => $isPushed,
                ));
            });
            
            $app->post('/settings', function () use ($app, $bespokeController) {

                $_POST['record_template'] = RECORD_TEMPLATE_ID; // using default Records Template of Users
                $_POST['record_category'] = RECORD_CATEGORY_ID; // using default Records Category of Registered Users

                $success = $bespokeController->updateUser($_POST, $_SESSION['slp']['userId']);

                sendJsonResponse(array('success'=>$success));
            });

            $app->post('/notifications/unread', function () use ($app, $bespokeController) {
                $toUserId = $_SESSION['slp']['userId'];
                $toUsers = $bespokeController->getUsersByUserId($toUserId);
                $toUser = count($toUsers) > 0 ? $toUsers[0] : array();

                $link = isset($_POST['link']) ? $_POST['link'] : '';

                if (!empty($link)) {
                    $bespokeController->markPushNotificationAsReadForLink($toUserId, $link);
                }
                $notifications = $bespokeController->getUnreadPushNotifications($toUserId);
                $badgeNumber = count($notifications);

                $bespokeController->sendSilentPushNotification($toUser, $badgeNumber);

                sendJsonResponse(array('number'=>$badgeNumber));
            });

            $app->post('/notifications', function () use ($app, $bespokeController) {
                $toUserId = $_SESSION['slp']['userId'];
                $toUser = $bespokeController->getUsersByUserId($toUserId);

                $bespokeController->markPushNotificationAsRead($toUserId);
                $notifications = $bespokeController->getUnreadPushNotifications($toUserId);
                $badgeNumber = count($notifications);

                $bespokeController->sendSilentPushNotification($toUser, $badgeNumber);

                sendJsonResponse(array('success'=>$toUserId));
            });

            $app->post('/order-form', function() use($app,$bespokeController){

                $strSubject = 'Order Form';

                $companyDetails = isset($_POST['company_details']) ? $_POST['company_details'] : '';
                $dateRequired = isset($_POST['date_required']) ? $_POST['date_required'] : '';
                $panelSize = isset($_POST['panel_size']) ? $_POST['panel_size'] : '';
                $numberRequired = isset($_POST['number_required']) ? $_POST['number_required'] : '';
                $location = isset($_POST['location']) ? $_POST['location'] : '';

                $strContent = '
            Company Details: '.$companyDetails.'<br>
            Date required: '.$dateRequired.'<br>
            Panel size: '.$panelSize.'<br>
            Number required: '.$numberRequired.'<br>
            Approximate location: '.$location.'<br>
            ';

                $bespokeController->emailFormRequest($_SESSION['slp']['userId'], $strSubject, $strContent);

                sendJsonResponse(array('success'=>'success'));
            });

            $app->post('/order-warranties', function() use($app,$bespokeController){

                $strSubject = 'Order Warranties';

                $companyDetails = isset($_POST['company_details']) ? $_POST['company_details'] : '';
                $material = isset($_POST['material']) ? $_POST['material'] : '';
                $address = isset($_POST['address']) ? $_POST['address'] : '';
                $numberRequired = isset($_POST['order_number']) ? $_POST['order_number'] : '';

                $strContent = '
            Company Details: '.$companyDetails.'<br>
            Material: '.$material.'<br>
            Address: '.$address.'<br>
            Original Order Number: '.$numberRequired.'<br>
            ';

                $bespokeController->emailFormRequest($_SESSION['slp']['userId'], $strSubject, $strContent);

                sendJsonResponse(array('success'=>'success'));
            });

            $app->post('/cad-drawings', function() use($app,$bespokeController){

                $strSubject = 'Order Warranties';

                $companyDetails = isset($_POST['company_details']) ? $_POST['company_details'] : '';
                $material = isset($_POST['material']) ? $_POST['material'] : '';
                $product = isset($_POST['product']) ? $_POST['product'] : '';
                $loremIpsum = isset($_POST['lorem_ipsum']) ? $_POST['lorem_ipsum'] : '';

                $strContent = '
            Company Details: '.$companyDetails.'<br>
            Material: '.$material.'<br>
            Product: '.$product.'<br>
            Lorem Ipsum: '.$loremIpsum.'<br>
            ';

                $bespokeController->emailFormRequest($_SESSION['slp']['userId'], $strSubject, $strContent);

                sendJsonResponse(array('success'=>'success'));
            });

            $app->post('/maintenance-manual', function() use($app,$bespokeController){

                $strSubject = 'Maintenance Manual';

                $companyDetails = isset($_POST['company_details']) ? $_POST['company_details'] : '';
                $jobNumber = isset($_POST['job_number']) ? $_POST['job_number'] : '';
                $material = isset($_POST['material']) ? $_POST['material'] : '';
                $loremIpsum = isset($_POST['lorem_ipsum']) ? $_POST['lorem_ipsum'] : '';

                $strContent = '
            Company Details: '.$companyDetails.'<br>
            Job Number: '.$jobNumber.'<br>
            Material: '.$material.'<br>
            Lorem Ipsum: '.$loremIpsum.'<br>
            ';

                $bespokeController->emailFormRequest($_SESSION['slp']['userId'], $strSubject, $strContent);

                sendJsonResponse(array('success'=>'success'));
            });

            $app->post('/internal-contact', function() use($app,$bespokeController){

                $subject = isset($_POST['subject']) ? $_POST['subject'] : '';
                $message = isset($_POST['message']) ? $_POST['message'] : '';

                if (empty($subject)) {
                    $bespokeController->customErrorMessage('Missing subject');
                    exit;
                }

                if (empty($message)) {
                    $bespokeController->customErrorMessage('Missing message content');
                    exit;
                }

//                if(empty($_POST['g-recaptcha-response'])){
//                    $bespokeController->customErrorMessage('Please answer the captcha');
//                    exit;
//                }
//
//                $curl = curl_init();
//                curl_setopt($curl,CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
//                curl_setopt($curl,CURLOPT_RETURNTRANSFER , true);
//                curl_setopt($curl,CURLOPT_POST , 1);
//                curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, false);
//                curl_setopt($curl,CURLOPT_POSTFIELDS , array("secret"=>'6LdL1FoUAAAAAE90sCoVPyF7VMkbgk5-IrfC1csK',"response"=>$_POST['g-recaptcha-response']));
//                $response = curl_exec($curl);
//                $err = curl_error($curl);
//                curl_close($curl);
//
//                $res = json_decode($response,true);
//
//                if ($err) {
//                    $bespokeController->customErrorMessage('Error when verifying the captcha');
//                    exit;
//                }
//                else {
//                    if($res['success']==true) {
//                        unset($_POST['g-recaptcha-response']);

                        $strSubject = 'Internal Contact | '.$subject;

                        $strContent = '
                        Subject: '.$subject.'<br>
                        Message: '.$message.'<br>
                        ';

                        $bespokeController->emailFormRequest($_SESSION['slp']['userId'], $strSubject, $strContent);

//                    } else{
//                        $bespokeController->customErrorMessage('Error when verifying the captcha');
//                        exit;
//                    }
//                }

                sendJsonResponse(array('success'=>'success'));
            });
        });
    });
});

$app->group('/admin', function() use($app,$engine,$bespokeController){
    $app->group('/notifications','checkLogin',function() use($app,$bespokeController,$engine){
        $app->get('/', function() use($app,$bespokeController,$engine){
            $appModuleAssets = $bespokeController->getCisuiteAssetsByPageId(APP_MENU_PAGE_ID, APP_MENU_GROUP_ID);
            $moduleTitles = $bespokeController->getAssetsByTypeId($appModuleAssets, MENU_ITEM_TITLE_ID);
            $moduleLinks = $bespokeController->getAssetsByTypeId($appModuleAssets, MENU_ITEM_LINK_ID);
            $moduleUniqueIDs = $bespokeController->getAssetsByTypeId($appModuleAssets, MENU_ITEM_UNIQUE_ID);
            
            $app->render('/bespoke/notification/index.php',array(
                'curpage'               => "dashboard",
                'cisuite_build_number'  => $engine->getModuleBuildNumber("ciUSER"),
                'modules'               => $engine->getModules(),
                'records'               => array(),
                'moduleTitles'          => $moduleTitles,
                'moduleLinks'           => $moduleLinks,
                'moduleUniqueIDs'       => $moduleUniqueIDs
            ));
        });

        $app->post('/', function() use($app,$bespokeController,$engine){
            $users = $bespokeController->getAllUsers();
            $message = isset($_POST['message']) ? $_POST['message'] : '';
            $link = isset($_POST['module']) ? $_POST['module'] : '';
            $link = isset($_POST['record']) && !empty($_POST['record']) && (string)$_POST['record'] != 'All' ? $_POST['record'] : $link;
            
            foreach ($users as $user) {
                $bespokeController->sendPushNotification($user, $message, $link);
            }
            
            echo json_encode(array('success'=>'okay'));
        });

        $app->post('/news','checkLogin',function() use($app,$bespokeController,$engine){
            $articles = $bespokeController->getCisuitePagesBySectionId(NEWS_SECTION_ID);
            echo json_encode(array(
                'articles'  => $articles
            ));
        });
    });
});