<?php

require 'controllers/DatabaseController.php';
require 'controllers/ConfigController.php';

require 'controllers/StatusCodes.php';

require 'controllers/UserController.php';
require 'controllers/PushNotifications.php';

require 'controllers/PagesController.php';

require_once 'controllers/ErrorHanlder.php';

class BespokeController {

    private $apiDatabaseController;
    private $cisuiteDatabaseController;
    private $configController;

    private $userController;
    private $pushNotifications;

    private $pagesController;

    public $apiDb;
    public $cisuiteDb;
    
    public function __construct(){
        $this->configController = new ConfigController();

        // API Database
        $this->apiDatabaseController = new DatabaseController(
            $this->configController->getApiDatabaseHost(),
            $this->configController->getApiDatabasePort(),
            $this->configController->getApiDatabaseName(),
            $this->configController->getApiDatabaseUser(),
            $this->configController->getApiDatabasePass()
        );
        $this->apiDb = $this->apiDatabaseController->getConnection();

        // cisuite Database
        $this->cisuiteDatabaseController = new DatabaseController(
            $this->configController->getCisuiteDatabaseHost(),
            $this->configController->getCisuiteDatabasePort(),
            $this->configController->getCisuiteDatabaseName(),
            $this->configController->getCisuiteDatabaseUser(),
            $this->configController->getCisuiteDatabasePass()
        );
        $this->cisuiteDb = $this->cisuiteDatabaseController->getConnection();

        $this->userController = new UserController(
            $this->apiDb,
            $this->cisuiteDb,
            $this->configController->getJwtGetUrl(),
            $this->configController->getJwtVerifyUrl(),
            $this->configController->getJwtKey(),
            $this->configController->getJwtHeaderAuthorization(),
            $this->configController->getJwtHeaderApiKey()
        );

        $this->pushNotifications = new PushNotifications(
            $this->apiDb,
            $this->configController->getApplePushNotificationsConfig(),
            $this->configController->getAndroidPushNotificationsConfig(),
            $this->configController->getIosUrl(),
            $this->configController->getAndroidUrl(),
            $this->configController->getJwtHeaderAuthorization(),
            $this->configController->getJwtHeaderApiKey()
        );


        $this->pagesController = new PagesController(
            $this->apiDb,
            $this->cisuiteDb
        );
    }

    use ErrorHandler;
    
    public function getCisuiteInternalPages() {        
        return $this->getCisuitePagesByGroupId(INTERNAL_GROUP_ID);
    }

    public function getCisuiteExternalPages() {
        return $this->getCisuitePagesByGroupId(EXTERNAL_GROUP_ID);
    }

    public function getCisuitePagesByGroupId($groupId) {return $this->pagesController->getCisuitePagesByGroupId($groupId);}

    public function getCisuitePagesBySectionId($sectionId) {return $this->pagesController->getCisuitePagesBySectionId($sectionId);}

    public function getCisuiteAssetsByPageId($pageId, $groupId) {return $this->pagesController->getCisuiteAssetsByPageId($pageId, $groupId);}

    public function getAssetsByTypeId($assets, $typeId) {return $this->pagesController->getAssetsByTypeId($assets, $typeId);}

    public function getPageTitle($slug) {return $this->pagesController->getPageTitle($slug);}
    
    /**
     * Auth Functions
     */
    public function validateAPIKey($apikey){return $this->userController->validateAPIKey($apikey); }

    public function validateJwt($jwt){return $this->userController->validateJwt($jwt);}

    /**
     * @param $fields
     * @return null|string
     */
    public function getAuthorizationHeader($fields){
        $headers = null;
        if (isset($fields['Authorization'])) {
            $headers = trim($fields["Authorization"]);
        }
        else if (isset($fields['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($fields["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }

    /**
     * @param $headers
     * @return null
     */
    public function getBearerToken($headers) {
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }

    public function getAppMenuModules(){return $this->userController->getAppMenuModules();}

    /**
     * User
     */
    public function transformUser($user){return $this->userController->transformUser($user);}

    public function loginAsUser($fields){return $this->userController->loginAsUser($fields, $this->userController->getUsersByEmail($fields['email']));}

    public function createUser($fields){return $this->userController->createUser($fields);}

    public function updateUser($fields, $curUserId){return $this->userController->updateUser($fields, $curUserId);}

    public function confirmUserHash($userId, $hash){return $this->userController->confirmUserHash($userId, $hash);}

    public function generatePasswordResetEmail($email){return $this->userController->generatePasswordResetEmail($email);}

    public function confirmHashForResetPassword($email, $hash){return $this->userController->confirmHashForResetPassword($email, $hash);}

    public function updateNewPassword($email, $password){$this->userController->updateNewPassword($email, $password);}

    public function sendSLPEmail($email, $subject, $message){$this->userController->sendSLPEmail($email, $subject, $message);}

    public function getAllUsers(){return $this->userController->getAllUsers();}

    public function getUserFieldValue($userId, $fieldId){return $this->userController->getUserFieldValue($userId, $fieldId);}

    public function getUsersByEmail($email){return $this->userController->getUsersByEmail($email);}

    public function getUsersByUserId($userId){return $this->userController->getUsersByUserId($userId);}

    public function getUsersByUsername($username){return $this->userController->getUsersByUsername($username);}

    public function getSingleUserByUserId($userId){return $this->userController->getSingleUserByUserId($userId);}

    public function emailFormRequest($userId, $strSubject, $strContent) {$this->userController->emailFormRequest($userId, $strSubject, $strContent);}

    public function clearCiPartnerToken($fields){$this->userController->clearCiPartnerToken($fields);}

    /**
     * Push Notification
     */

    public function createNotification($fields, $toUserId){return $this->pushNotifications->createNotification($fields, $toUserId);}

    public function markPushNotificationAsRead($userId){return $this->pushNotifications->markPushNotificationAsRead($userId);}

    public function markPushNotificationAsReadForLink($userId, $link){return $this->pushNotifications->markPushNotificationAsReadForLink($userId, $link);}

    public function getUnreadPushNotifications($toUserId){return $this->pushNotifications->getUnreadPushNotifications($toUserId);}

    public function sendAndroidPushNotification($fields, $unreadMessageNumber, $isSilent){return $this->pushNotifications->sendAndroidPushNotification($fields, $unreadMessageNumber, $isSilent);}

    public function sendApplePushNotification($fields, $unreadMessageNumber, $isSilent){return $this->pushNotifications->sendApplePushNotification($fields, $unreadMessageNumber, $isSilent);}
    
    public function sendPushNotification($toUser, $notificationText, $link){
        global $slpBaseUrl;
        $toUserId = $toUser->cprID;
        $isPush = $this->getUserFieldValue($toUserId, FIELD_PUSH_NOTIFIED);
        $link = substr($link, 0, 1) == '/' ? substr($link, 1, strlen($link) - 1) : $link;
        $link = $slpBaseUrl . '/' .$link;

        if ((int)$isPush == 1) {
            $androidToken = $this->getUserFieldValue($toUserId, FIELD_ANDROID_TOKEN_ID);
            if (!empty($androidToken)) {
                $fields = array();
                $fields['body'] = $notificationText;
                $fields['link'] = $link;
                $fields['device_token'] = $androidToken;
                $fields['device_platform'] = 'android';

                $this->createNotification($fields, $toUserId);
                $notifications = $this->getUnreadPushNotifications($toUserId);

                $this->sendAndroidPushNotification($fields, count($notifications), false);
            }

            $iosToken = $this->getUserFieldValue($toUserId, FIELD_IOS_TOKEN_ID);
            if (!empty($iosToken)) {
                $fields = array();
                $fields['body'] = $notificationText;
                $fields['link'] = $link;
                $fields['device_token'] = $iosToken;
                $fields['device_platform'] = 'ios';

                $this->createNotification($fields, $toUserId);
                $notifications = $this->getUnreadPushNotifications($toUserId);

                $this->sendApplePushNotification($fields, count($notifications), false);
            }
        }
    }

    public function sendSilentPushNotification($toUser, $badgeNumber) {
        $toUserId = $toUser->cprID;
        $isPush = $this->getUserFieldValue($toUserId, FIELD_PUSH_NOTIFIED);

        if ((int)$isPush == 1) {
            $androidToken = $this->getUserFieldValue($toUserId, FIELD_ANDROID_TOKEN_ID);
            if (!empty($androidToken)) {
                $fields = array();
                $fields['body'] = '';
                $fields['link'] = '';
                $fields['device_token'] = $androidToken;
                $fields['device_platform'] = 'android';

                $this->sendAndroidPushNotification($fields, $badgeNumber, true);
            }

            $iosToken = $this->getUserFieldValue($toUserId, FIELD_IOS_TOKEN_ID);
            if (!empty($iosToken)) {
                $fields = array();
                $fields['body'] = '';
                $fields['link'] = '';
                $fields['device_token'] = $iosToken;
                $fields['device_platform'] = 'ios';

                $this->sendApplePushNotification($fields, $badgeNumber, true);
            }
        }
    }
}