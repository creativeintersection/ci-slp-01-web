<?php


class ConfigController {
    private $api_database_host;
    private $api_database_port;
    private $api_database_name;
    private $api_database_user;
    private $api_database_pass;

    private $cisuite_database_host;
    private $cisuite_database_port;
    private $cisuite_database_name;
    private $cisuite_database_user;
    private $cisuite_database_pass;

    private $jwt_get_url;
    private $jwt_verify_url;
    private $jwt_key;
    private $jwt_header_authorization;
    private $jwt_header_api_key;
    private $jwt_algorithm;

    private $applePushNotificationsConfig;
    private $androidPushNotificationsConfig;
    private $iOSUrl;
    private $androidUrl;

    private $dateTimezone;

    public function __construct(){
        $configVar = include(dirname(__FILE__) . '/../config/config.php');

        $this->setApiDatabaseHost($configVar['apiDatabase']['host']);
        $this->setApiDatabasePort($configVar['apiDatabase']['port']);
        $this->setApiDatabaseName($configVar['apiDatabase']['name']);
        $this->setApiDatabaseUser($configVar['apiDatabase']['user']);
        $this->setApiDatabasePass($configVar['apiDatabase']['password']);

        $this->setCisuiteDatabaseHost($configVar['cisuiteDatabase']['host']);
        $this->setCisuiteDatabasePort($configVar['cisuiteDatabase']['port']);
        $this->setCisuiteDatabaseName($configVar['cisuiteDatabase']['name']);
        $this->setCisuiteDatabaseUser($configVar['cisuiteDatabase']['user']);
        $this->setCisuiteDatabasePass($configVar['cisuiteDatabase']['password']);

        $this->setJwtGetUrl($configVar['jwt']['getUrl']);
        $this->setJwtVerifyUrl($configVar['jwt']['verifyUrl']);
        $this->setJwtKey($configVar['jwt']['key']);
        $this->setJwtHeaderAuthorization($configVar['jwt']['headerAuthorization']);
        $this->setJwtHeaderApiKey($configVar['jwt']['headerApiKey']);
        $this->setJwtAlgorithm($configVar['jwt']['algorithm']);

        $this->setDateTimezone('Australia/Brisbane');
        $this->setAndroidPushNotificationsConfig($configVar['androidPushNotifications']);
        $this->setApplePushNotificationsConfig($configVar['applePushNotifications']);
        $this->setIosUrl($configVar['iOSUrl']);
        $this->setAndroidUrl($configVar['androidUrl']);
    }

    public function getConfig(){
        return array(
            "timezone" => $this->getDateTimezone()
        );
    }

    /**
     * @return mixed
     */
    public function getApiDatabaseHost()
    {
        return $this->api_database_host;
    }

    /**
     * @param mixed $api_database_host
     */
    public function setApiDatabaseHost($api_database_host)
    {
        $this->api_database_host = $api_database_host;
    }

    /**
     * @return mixed
     */
    public function getApiDatabasePort()
    {
        return $this->api_database_port;
    }

    /**
     * @param mixed $api_database_port
     */
    public function setApiDatabasePort($api_database_port)
    {
        $this->api_database_port = $api_database_port;
    }

    /**
     * @return mixed
     */
    public function getApiDatabaseName()
    {
        return $this->api_database_name;
    }

    /**
     * @param mixed $api_database_name
     */
    public function setApiDatabaseName($api_database_name)
    {
        $this->api_database_name = $api_database_name;
    }

    /**
     * @return mixed
     */
    public function getApiDatabaseUser()
    {
        return $this->api_database_user;
    }

    /**
     * @param mixed $api_database_user
     */
    public function setApiDatabaseUser($api_database_user)
    {
        $this->api_database_user = $api_database_user;
    }

    /**
     * @return mixed
     */
    public function getApiDatabasePass()
    {
        return $this->api_database_pass;
    }

    /**
     * @param mixed $api_database_pass
     */
    public function setApiDatabasePass($api_database_pass)
    {
        $this->api_database_pass = $api_database_pass;
    }

    /**
     * @return mixed
     */
    public function getCisuiteDatabaseHost()
    {
        return $this->cisuite_database_host;
    }

    /**
     * @param mixed $cisuite_database_host
     */
    public function setCisuiteDatabaseHost($cisuite_database_host)
    {
        $this->cisuite_database_host = $cisuite_database_host;
    }

    /**
     * @return mixed
     */
    public function getCisuiteDatabasePort()
    {
        return $this->cisuite_database_port;
    }

    /**
     * @param mixed $cisuite_database_port
     */
    public function setCisuiteDatabasePort($cisuite_database_port)
    {
        $this->cisuite_database_port = $cisuite_database_port;
    }

    /**
     * @return mixed
     */
    public function getCisuiteDatabaseName()
    {
        return $this->cisuite_database_name;
    }

    /**
     * @param mixed $cisuite_database_name
     */
    public function setCisuiteDatabaseName($cisuite_database_name)
    {
        $this->cisuite_database_name = $cisuite_database_name;
    }

    /**
     * @return mixed
     */
    public function getCisuiteDatabaseUser()
    {
        return $this->cisuite_database_user;
    }

    /**
     * @param mixed $cisuite_database_user
     */
    public function setCisuiteDatabaseUser($cisuite_database_user)
    {
        $this->cisuite_database_user = $cisuite_database_user;
    }

    /**
     * @return mixed
     */
    public function getCisuiteDatabasePass()
    {
        return $this->cisuite_database_pass;
    }

    /**
     * @param mixed $cisuite_database_pass
     */
    public function setCisuiteDatabasePass($cisuite_database_pass)
    {
        $this->cisuite_database_pass = $cisuite_database_pass;
    }

    /**
     * @return mixed
     */
    public function getDateTimezone()
    {
        return $this->dateTimezone;
    }

    /**
     * @param mixed $dateTimezone
     */
    public function setDateTimezone($dateTimezone)
    {
        $this->dateTimezone = $dateTimezone;
    }

    /**
     * @return mixed
     */
    public function getJwtGetUrl()
    {
        return $this->jwt_get_url;
    }

    /**
     * @param $jwt_get_url
     */
    public function setJwtGetUrl($jwt_get_url)
    {
        $this->jwt_get_url = $jwt_get_url;
    }

    /**
     * @return mixed
     */
    public function getJwtVerifyUrl()
    {
        return $this->jwt_verify_url;
    }

    /**
     * @param $jwt_verify_url
     */
    public function setJwtVerifyUrl($jwt_verify_url)
    {
        $this->jwt_verify_url = $jwt_verify_url;
    }

    /**
     * @return mixed
     */
    public function getJwtKey()
    {
        return $this->jwt_key;
    }

    /**
     * @param $jwt_key
     */
    public function setJwtKey($jwt_key)
    {
        $this->jwt_key = $jwt_key;
    }

    /**
     * @return mixed
     */
    public function getJwtHeaderAuthorization()
    {
        return $this->jwt_header_authorization;
    }

    /**
     * @param $jwt_header_authorization
     */
    public function setJwtHeaderAuthorization($jwt_header_authorization)
    {
        $this->jwt_header_authorization = $jwt_header_authorization;
    }

    /**
     * @return mixed
     */
    public function getJwtHeaderApiKey()
    {
        return $this->jwt_header_api_key;
    }

    /**
     * @param $jwt_header_api_key
     */
    public function setJwtHeaderApiKey($jwt_header_api_key)
    {
        $this->jwt_header_api_key = $jwt_header_api_key;
    }

    /**
     * @return mixed
     */
    public function getJwtAlgorithm()
    {
        return $this->jwt_algorithm;
    }

    /**
     * @param mixed $jwt_algorithm
     */
    public function setJwtAlgorithm($jwt_algorithm)
    {
        $this->jwt_algorithm = $jwt_algorithm;
    }

    /**
     * @return mixed
     */
    public function getApplePushNotificationsConfig()
    {
        return $this->applePushNotificationsConfig;
    }

    /**
     * @param $applePushNotificationsConfig
     */
    public function setApplePushNotificationsConfig($applePushNotificationsConfig)
    {
        $this->applePushNotificationsConfig = $applePushNotificationsConfig;
    }

    /**
     * @return mixed
     */
    public function getAndroidPushNotificationsConfig()
    {
        return $this->androidPushNotificationsConfig;
    }

    /**
     * @param $androidPushNotificationsConfig
     */
    public function setAndroidPushNotificationsConfig($androidPushNotificationsConfig)
    {
        $this->androidPushNotificationsConfig = $androidPushNotificationsConfig;
    }

    /**
     * @return mixed
     */
    public function getIosUrl()
    {
        return $this->iOSUrl;
    }

    /**
     * @param $iOSUrl
     */
    public function setIosUrl($iOSUrl)
    {
        $this->iOSUrl = $iOSUrl;
    }

    /**
     * @return mixed
     */
    public function getAndroidUrl()
    {
        return $this->androidUrl;
    }

    /**
     * @param $androidUrl
     */
    public function setAndroidUrl($androidUrl)
    {
        $this->androidUrl = $androidUrl;
    }
}