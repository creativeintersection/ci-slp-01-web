<?php

require_once 'ErrorHanlder.php';
require_once 'HelperFunctions.php';

class UserController {

    private $apiDb;
    private $cisuiteDb;
    private $jwtGetUrl;
    private $jwtVerifyUrl;
    private $jwtKey;
    private $jwtHeaderAuthorization;
    private $jwtHeaderApiKey;

    public function __construct($apiDb, $cisuiteDb, $jwtGetUrl, $jwtVerifyUrl, $jwtKey, $jwtHeaderAuthorization, $jwtHeaderApiKey){
        $this->apiDb = $apiDb;
        $this->cisuiteDb = $cisuiteDb;
        $this->jwtGetUrl = $jwtGetUrl;
        $this->jwtVerifyUrl = $jwtVerifyUrl;
        $this->jwtKey = $jwtKey;
        $this->jwtHeaderAuthorization = $jwtHeaderAuthorization;
        $this->jwtHeaderApiKey = $jwtHeaderApiKey;
    }

    use ErrorHandler;
    use HelperFunctions;

    /*
     * Auth Functions
     */

    public function validateAPIKey($apikey){
        if(empty($apikey)){
            return false;
        }

        $query = "SELECT * FROM apikeys WHERE (apikey = :key) AND (disabled = 0) LIMIT 1;";
        $sql = $this->apiDb->prepare($query);
        $sql->bindParam('key', $apikey);
        $sql->execute();
        $queryResult = $sql->fetchAll(PDO::FETCH_OBJ);

        return !empty($queryResult);
    }

    public function getJwt($userInfo) {
        if(empty($userInfo)) {
            return false;
        }

        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL, $this->jwtGetUrl);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_POST, 1);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($curl,CURLOPT_POSTFIELDS, array(
            'key'     => $this->jwtKey,
            'payload' => base64_encode(serialize($userInfo))
        ));

        curl_setopt($curl,CURLOPT_HTTPHEADER, array(
            'authorization: ' . $this->jwtHeaderAuthorization,
            'x-apikey: ' . $this->jwtHeaderApiKey
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);
        curl_close($curl);

        if($err) {
            return false;
        }

        return $response;
    }

    public function validateJwt($jwt){
        if(empty($jwt)) {
            return false;
        }

        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL, $this->jwtVerifyUrl);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_POST, 1);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($curl,CURLOPT_POSTFIELDS, array(
            'key'   => $this->jwtKey,
            'token' => $jwt
        ));

        curl_setopt($curl,CURLOPT_HTTPHEADER, array(
            'authorization: ' . $this->jwtHeaderAuthorization,
            'x-apikey: ' . $this->jwtHeaderApiKey
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);
        curl_close($curl);

        if ($err || empty($response)) {
            return false;
        }

        $response = json_decode($response, true);
        if(empty($response)) {
            return false;
        }

        $jwt     = (isset($response['valid']) && $response['valid']) ? $jwt : null;
        $payload = isset($response['payload']) ? $response['payload'] : null;

        if (empty($jwt) && !empty($payload)) {
            $jwt = $this->getJwt($payload);

            if(empty($jwt) || isset($jwt['error'])) {
                return false;
            }
        }

        $response['jwt'] = $jwt;

        return $response;
    }
    
    public function transformUser($user) {
        return empty($user) ? null : array(
            'userId'       => $user->cprID,
            'username'     => $this->getUserFieldValue($user->cprID, FIELD_USERNAME_ID),
            'phone'        => $this->getUserFieldValue($user->cprID, FIELD_PHONE_ID),
            'email'        => $this->getUserFieldValue($user->cprID, FIELD_EMAIL_ID)
        );
    }

    public function loginAsUser($fields, $users) {
        // Validate input parameters
        $email = isset($fields['email']) ? $fields['email'] : '';
        $password = isset($fields['password']) ? $fields['password'] : '';
        if(empty($email)) {
            $this->customErrorMessage('Missing email address');
            exit;
        }
        if(empty($password)) {
            $this->customErrorMessage('Missing password');
            exit;
        }

        $user = null;
        // Find the first active candidate with matching username and password
        if (!empty($users)) {
            foreach ($users as $candidate) {
                $hasValidCredentials = !empty($candidate)
                    && ($candidate->cprActive === 1)
                    && password_verify($password, $this->getUserFieldValue($candidate->cprID, FIELD_PASSWORD_ID))
                ;

                if($hasValidCredentials) {
                    $user = $candidate;
                    break;
                }
            }
        }

        if(empty($user)) {
            $this->throwError(9);
            exit;
        }

        // Include the deviceType in the Cookies
        $deviceType = isset($fields['device_type']) ? trim($fields['device_type']) : null;
        $app = \Slim\Slim::getInstance();
        $app->setCookie('deviceType', $deviceType, '10 years'); // Long life cookie
        
        $userInfo = $this->transformUser($user) ;
        
        // Update User Session Info
        $this->updateUserToken($user->cprID, $fields);

        $jwt = $this->getJwt($userInfo);
        if(empty($jwt) || isset($jwt['error'])) {
            $this->throwError(9);
            exit;
        }

        return array(
            'jwt'  => $jwt,
            'user' => $userInfo
        );
    }

    public function getDeviceTokenInfo($fields) {
        $deviceType = trim(isset($fields['device_type']) ? $fields['device_type'] : '');
        if($deviceType == '') {
            return false;
        }

        $deviceToken = trim(isset($fields['device_token']) ? $fields['device_token'] : '');
        if(empty($deviceToken)) {
            return false;
        }

        $platform     = ($deviceType === '0') ? 'android' : 'ios';
        $dbTokenField = "{$platform}_push_notification_token";

        return array(
            'token_name' => $dbTokenField,
            'token_value' => $deviceToken
        );
    }


    public function clearCiPartnerToken($fields) {
        try {
            $tokenInfo = $this->getDeviceTokenInfo($fields);
            $dbTokenField = !empty($tokenInfo) ? $tokenInfo['token_name'] : '';
            $deviceToken = !empty($tokenInfo) ? $tokenInfo['token_value'] : '';

            $fieldId = $dbTokenField == 'android_push_notification_token' ? FIELD_ANDROID_TOKEN_ID : FIELD_IOS_TOKEN_ID;

            $updateUserField = $this->cisuiteDb->prepare("UPDATE ciPartner_fieldlink 
                                                            SET cflValue = NULL 
                                                            WHERE fk_cfl_cfdID = :fieldId AND cflValue = :fieldValue
                                                          ");
            $updateUserField->bindParam('fieldId', $fieldId);
            $updateUserField->bindParam('fieldValue', $deviceToken);
            $updateUserField->execute();

        }
        catch (\PDOException $e) {
            var_dump($e);
        }
    }
    
    public function updateUserToken($userId, $fields) {
        $tokenInfo = $this->getDeviceTokenInfo($fields);
        if (empty($tokenInfo)) {
            return $userId;
        }

        $dbTokenField = $tokenInfo['token_name'];
        $deviceToken = $tokenInfo['token_value'];

        // When a user logs in with a push token check
        // if that token is being used by anyone else in the user table (based on the platform)
        $this->clearCiPartnerToken($fields);
        
        // update user token
        $fieldId = $dbTokenField == 'android_push_notification_token' ? FIELD_ANDROID_TOKEN_ID : FIELD_IOS_TOKEN_ID;
        $this->updateSingleCiPartnerUserField($userId, $fieldId, $deviceToken);

        return $userId;
    }

    public function getAppMenuModules(){
        global $slpBaseUrl;

        $curl = curl_init();
//        curl_setopt($curl,CURLOPT_POST , 1);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER , true);
        curl_setopt($curl,CURLOPT_HEADER, false);
        
        curl_setopt($curl,CURLOPT_URL,$slpBaseUrl.'/cisuite-index.php?pid=214');

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $this->throwError(20);exit;
        } else {
            if(get_magic_quotes_gpc()){
                $response = stripslashes($response);
            }

            $response = trim((str_replace('},]','}]',$response)));
            $response = ((str_replace(array('\r','\n','\\'),'',$response)));
            $response = json_decode(json_encode($response));
            return $response;
        }
    }

    // $engine->newCIPartnerUser($_POST);
    // POST /cicms/admin/cipartner/user/new
    private function createCiPartnerRecord($fields) {
        try {
            $username = isset($fields['username']) ? $fields['username'] : '';
            $recordTemplateId = isset($fields['record_template']) ? $fields['record_template'] : 0;
            $recordCategoryId = isset($fields['record_category']) ? $fields['record_category'] : 0;
            
            if (empty($username) || empty($recordTemplateId) || empty($recordCategoryId)) {
                $this->throwError(23);
                exit;
            }

            $newUser = $this->cisuiteDb->prepare("INSERT INTO ciPartner_partner (
                                                      fk_cpr_cpdID,
                                                      cprTitle,
                                                      fk_cpr_catID,
                                                      cprActive,
                                                      viewPermissions
                                                  ) VALUES(
                                                      :recordTemplateId,
                                                      :username,
                                                      :recordCategoryId,
                                                      0,
                                                      0
                                                  )");
            $newUser->bindParam('recordTemplateId', $recordTemplateId);
            $newUser->bindParam('username', $username);
            $newUser->bindParam('recordCategoryId', $recordCategoryId);
            $newUser->execute();

            $userId = $this->cisuiteDb->lastInsertID();

            return $userId;
        }
        catch (\PDOException $e) {
            var_dump($e);
        }
    }

    private function updateCiPartnerRecord($fields, $curUserId) {
        try {
            $username = isset($fields['username']) ? $fields['username'] : '';
            $recordTemplateId = isset($fields['record_template']) ? $fields['record_template'] : 0;
            $recordCategoryId = isset($fields['record_category']) ? $fields['record_category'] : 0;

            if (empty($username) || empty($recordTemplateId) || empty($recordCategoryId)) {
                $this->throwError(23);
                exit;
            }

            $newUser = $this->cisuiteDb->prepare("UPDATE ciPartner_partner
                                                    SET cprTitle = :username,
                                                        DateUpdated = utc_timestamp()
                                                    WHERE cprID = :curUserId");
            $newUser->bindParam('username', $username);
            $newUser->bindParam('curUserId', $curUserId);
            $newUser->execute();

            return $curUserId;
        }
        catch (\PDOException $e) {
            var_dump($e);
        }
    }

    public function updateCiPartnerDate($userId) {
        try {
            if (empty($userId)) {
                $this->customErrorMessage('Missing User Id');
                exit;
            }

            $updatedDate = $this->cisuiteDb->prepare("UPDATE ciPartner_partner SET DateUpdated = utc_timestamp() WHERE cprID = :userId");
            $updatedDate->bindParam('userId', $userId);
            $updatedDate->execute();
        }
        catch (\PDOException $e) {
            var_dump($e);
        }
    }

    public function getUserFieldValue($userId, $fieldId) {
        $field = $this->existingUserField($userId, $fieldId);
        if (empty($field)) {
            return '';
        }
        return empty($field) ? '' : $field->cflValue;
    }

    private function existingUserField($userId, $fieldId) {
        try {
            if (empty($userId)) {
                $this->customErrorMessage('Missing User Id');
                exit;
            }
            if (empty($fieldId)) {
                $this->customErrorMessage('Missing Field Id');
                exit;
            }

            $findUserField = $this->cisuiteDb->prepare("SELECT * FROM ciPartner_fieldlink 
                                                    WHERE fk_cfl_cprID = :userId AND fk_cfl_cfdID = :fieldId AND cflActive = 1");
            $findUserField->bindParam('userId', $userId);
            $findUserField->bindParam('fieldId', $fieldId);
            $findUserField->execute();
            $findUserField = $findUserField->fetchAll(PDO::FETCH_OBJ);
            if (count($findUserField) > 0) {
                return $findUserField[0];
            }
            return false;
        }
        catch (\PDOException $e) {
            var_dump($e);
        }
    }

    public function updateSingleCiPartnerUserField($userId, $fieldId, $fieldValue) {
        try {
            if (empty($userId)) {
                $this->customErrorMessage('Missing User Id');
                exit;
            }
            if (empty($fieldId)) {
                $this->customErrorMessage('Missing Field Id');
                exit;
            }

            if (empty($this->existingUserField($userId, $fieldId))) {
                $updateUserField = $this->cisuiteDb->prepare("INSERT INTO ciPartner_fieldlink (
                                                              fk_cfl_cprID,
                                                              fk_cfl_cfdID,
                                                              cflValue
                                                          )
                                                          VALUES (
                                                              :userId,
                                                              :fieldId,
                                                              :fieldValue
                                                          )");
                $updateUserField->bindParam('userId', $userId);
                $updateUserField->bindParam('fieldId', $fieldId);
                $updateUserField->bindParam('fieldValue', $fieldValue);
                $updateUserField->execute();
            }
            else {
                $updateUserField = $this->cisuiteDb->prepare("UPDATE ciPartner_fieldlink 
                                                            SET cflValue = :fieldValue 
                                                            WHERE fk_cfl_cprID = :userId AND fk_cfl_cfdID = :fieldId
                                                          ");
                $updateUserField->bindParam('userId', $userId);
                $updateUserField->bindParam('fieldId', $fieldId);
                $updateUserField->bindParam('fieldValue', $fieldValue);
                $updateUserField->execute();
            }
        }
        catch (\PDOException $e) {
            var_dump($e);
        }
    }

    // $engine->editCIPartnerUserValues($productid,$data)
    // POST /cicms/admin/cipartner/user/:userId/edit/fields

    public function updateCiPartnerUserFields($userId, $fields) {
        try {
            if (empty($userId)) {
                $this->customErrorMessage('Missing User Id');
                exit;
            }
            $this->updateCiPartnerDate($userId);

            $tokenInfo = $this->getDeviceTokenInfo($fields);
            $dbTokenField = !empty($tokenInfo) ? $tokenInfo['token_name'] : '';
            $deviceToken = !empty($tokenInfo) ? $tokenInfo['token_value'] : '';

            $arrProcessing = array(
                FIELD_USERNAME_ID => isset($fields['username']) ? $fields['username'] : '',
                FIELD_PHONE_ID => isset($fields['phone']) ? $fields['phone'] : '',
                FIELD_EMAIL_ID => isset($fields['email']) ? $fields['email'] : '',
                FIELD_PASSWORD_ID => isset($fields['password']) ? $fields['password'] : '',
                FIELD_ANDROID_TOKEN_ID => $dbTokenField === 'android_push_notification_token' ? $deviceToken : '',
                FIELD_IOS_TOKEN_ID => $dbTokenField === 'ios_push_notification_token' ? $deviceToken : '',
                FIELD_PUSH_NOTIFIED =>  isset($fields['is_pushed']) ? ($fields['is_pushed'] == 'on' ? 1 : 0) : 0
            );

            foreach ($arrProcessing as $fieldId => $fieldValue) {
                if ($fieldId == FIELD_USERNAME_ID && empty($fieldValue)) {
                    $this->customErrorMessage('Username is required');
                    exit;
                }
                if ($fieldId == FIELD_EMAIL_ID && empty($fieldValue)) {
                    $this->customErrorMessage('Email is required');
                    exit;
                }
                
                $arrSkipIfEmpty = array(FIELD_PASSWORD_ID, FIELD_ANDROID_TOKEN_ID, FIELD_IOS_TOKEN_ID);
                if (in_array($fieldId, $arrSkipIfEmpty) && empty($fieldValue)) {
                    continue;
                }
                
                if ($fieldId == FIELD_PASSWORD_ID) {
                    $fieldValue = password_hash($fieldValue, PASSWORD_BCRYPT);
                }

                // update new value
                $this->updateSingleCiPartnerUserField($userId, $fieldId, $fieldValue);
            }
        }
        catch (\PDOException $e) {
            var_dump($e);
        }
    }

    public function createUser($fields){
        try{
            $username = isset($fields['username']) ? $fields['username'] : '';
            $email = isset($fields['email']) ? $fields['email'] : '';
            $password = isset($fields['password']) ? $fields['password'] : '';

            if (empty($username)) {
                $this->customErrorMessage('Missing name');
                exit;
            }

            if (empty($email)) {
                $this->customErrorMessage('Missing email address');
                exit;
            }

            if (empty($password)) {
                $this->customErrorMessage('Password is required');
                exit;
            }
            
            $userId = $this->getUserIdByEmail($email);
            
            if (!empty($userId)) {
                $this->throwError(14);
                exit;
            }

            $userId = $this->createCiPartnerRecord($fields);

            $this->updateCiPartnerUserFields($userId, $fields);

            $this->sendEmailConfirmation($userId);

            return $userId;
        }
        catch (\PDOException $e){
            var_dump($e);
        }
    }

    public function updateUser($fields, $curUserId){
        $username = isset($fields['username']) ? $fields['username'] : '';
        $email = isset($fields['email']) ? $fields['email'] : '';

        if (empty($username)) {
            $this->customErrorMessage('Missing name');
            exit;
        }

        if (empty($email)) {
            $this->customErrorMessage('Missing email address');
            exit;
        }

        $userId = $this->getUserIdByEmail($email);

        if (!empty($userId) && ((int)$userId !== (int)$curUserId)) {
            $this->throwError(14);
            exit;
        }

        $userId = $this->updateCiPartnerRecord($fields, $curUserId);

        $this->updateCiPartnerUserFields($userId, $fields);

        return $userId;
    }

    public function getAllUsers() {
        $getUsers = $this->cisuiteDb->prepare("SELECT * FROM ciPartner_partner
                                       WHERE cprDeleted = 0 AND cprActive = 1");
        $getUsers->execute();
        $getUsers = $getUsers->fetchAll(PDO::FETCH_OBJ);

        return $getUsers;
    }

    public function getSingleUserByUserId($userId) {
        if(empty($userId)){
            $this->customErrorMessage('Missing User ID');
            exit;
        }

        $getUsers = $this->cisuiteDb->prepare("SELECT * FROM ciPartner_partner
                                       WHERE cprID = :userId");
        $getUsers->bindParam('userId',$userId);
        $getUsers->execute();
        $getUsers = $getUsers->fetchAll(PDO::FETCH_OBJ);

        if (count($getUsers) > 0) {
            return $getUsers[0];
        }
        return false;
    }

    public function getUsersByUserId($userId) {
        if(empty($userId)){
            $this->customErrorMessage('Missing User ID');
            exit;
        }

        $getUsers = $this->cisuiteDb->prepare("SELECT * FROM ciPartner_partner
                                       WHERE cprID = :userId AND cprActive = 1");
        $getUsers->bindParam('userId',$userId);
        $getUsers->execute();
        $getUsers = $getUsers->fetchAll(PDO::FETCH_OBJ);

        return $getUsers;
    }

    private function sendEmailConfirmation($userId) {
        global $slpBaseUrl;

        $hash = md5( rand(0,1000) ); // Generate random 32 character hash and assign it to a local variable.

        $this->updateSingleCiPartnerUserField($userId, FIELD_HASH_ID, $hash);
        $email = $this->getUserFieldValue($userId, FIELD_EMAIL_ID);

        if (empty($email)) {
            $this->customErrorMessage('The user ('.$userId.') has no email address');
            exit;
        }

        $subject = 'Streamline Plus Signup | Verification';
        $message = '
Thanks for signing up!
Your account has been created, you can login with your credentials that you have registered with us after you activate your account by pressing the url below.

Please click this link to activate your account:
'.$slpBaseUrl.'/cicms/slp/verify?user_id='.$userId.'&hash='.$hash.'
        ';

        $this->sendSLPEmail($email, $subject, $message);
    }

    private function sendEmailByUserId($userId, $subject, $message) {
        $email = $this->getUserFieldValue($userId, FIELD_EMAIL_ID);

        if (empty($email)) {
            $this->customErrorMessage('The user ('.$userId.') has no email address');
            exit;
        }

        $this->sendSLPEmail($email, $subject, $message);
    }

    public function confirmUserHash($userId, $hash) {
        if(empty($userId) || empty($hash)){
            $this->throwError(22);exit;
        }

        $user = $this->getSingleUserByUserId($userId);

        if (empty($user)) {
            $this->throwError(22);exit;
        }

        $dbHash = $this->getUserFieldValue($userId, FIELD_HASH_ID);

        if (empty($hash) || $dbHash !== $hash) {
            $this->throwError(22);exit;
        }

        $this->setUserActive($userId);

        $subject = 'Streamline Plus | Account Activated';
        $message = '
Thanks for signing up!
Your account has been activated.';

        $this->sendEmailByUserId($userId, $subject, $message);

        return $userId;
    }

    private function setUserActive($userId) {
        $this->updateSingleCiPartnerUserField($userId, FIELD_HASH_ID, '');
        
        $setActive = $this->cisuiteDb->prepare("UPDATE ciPartner_partner
                                       SET cprActive = 1,
                                           DateUpdated = utc_timestamp()
                                       WHERE cprID = :userId");
        $setActive->bindParam('userId', $userId);
        $setActive->execute();
    }

//    private function updateUserPassword($email, $password) {
//        $updatePassword = $this->db->prepare("UPDATE user SET hash = NULL, password = :password WHERE email = :email");
//        $updatePassword->bindParam('password',$password);
//        $updatePassword->bindParam('email',$email);
//        $updatePassword->execute();
//    }

//    private function updateStripeCustomer($entityId, $customerId) {
//        $updateEntity = $this->db->prepare("UPDATE entity SET stripe_customer = :customerId WHERE entity_id = :entityId");
//        $updateEntity->bindParam('entityId',$entityId);
//        $updateEntity->bindParam('customerId',$customerId);
//        $updateEntity->execute();
//    }

    public function generatePasswordResetEmail($email){
        global $slpBaseUrl;

        if(!isset($email)){
            $this->throwError(22);exit;
        }

        $hash = md5( rand(0,1000) ); // Generate random 32 character hash and assign it to a local variable.

        $userId = $this->getUserIdByEmail($email);
        if (empty($userId)) {
            $this->customErrorMessage('User could not be found with the email');
            exit;
        }
        $this->updateSingleCiPartnerUserField($userId, FIELD_HASH_ID, $hash);

        $users = $this->getUsersByEmail($email);

        if (empty($users) || count($users) <= 0) {
            $this->throwError(22);exit;
        }

        $user = $users[0];

        $subject = 'Streamline Plus Signup | Reset Password Link';
        $message = '
Hi '.$user->cprTitle.',
A request to reset the password to your Streamline Plus account has been made. Click the link below to reset your email
'.$slpBaseUrl.'/reset-password?email='.$email.'&hash='.$hash.'
If you did not make this request, please ignore this email. If you are concerned about your account, please contact us.
        ';

        $this->sendSLPEmail($email, $subject, $message);

        return $user;
    }

    public function confirmHashForResetPassword($email, $hash) {
        if(!isset($email) || !isset($hash)){
            $this->customErrorMessage('Email or hash is missing');
            exit;
        }

        $users = $this->getUsersByEmail($email);

        if (empty($users) || count($users) <= 0) {
            $this->customErrorMessage('User could not be found with the email');
            exit;
        }

        $user = $users[0];

        $dbHash = $this->getUserFieldValue($user->cprID, FIELD_HASH_ID);

        if ((string)$dbHash != (string)$hash) {
            $this->customErrorMessage('Hash is not valid');
            exit;
        }

        return true;
    }

    public function updateNewPassword($email, $password) {
        global $slpBaseUrl;

        $users = $this->getUsersByEmail($email);
        $user = $users[0];

        if (empty($users) || count($users) <= 0) {
            $this->customErrorMessage('User could not be found with the email');
            exit;
        }

        $passwordHash = password_hash($password, PASSWORD_BCRYPT);
        $this->updateSingleCiPartnerUserField($user->cprID, FIELD_PASSWORD_ID, $passwordHash);

        $subject = 'Streamline Plus | Reset Password'; // Give the email a subject
        $message = '
Hi '.$user->cprTitle.'
Your account password has been reset, you can login with the new password at '.$slpBaseUrl.'/login
        ';

        $this->sendSLPEmail($email, $subject, $message);
    }

    public function getUsersByUsername($username) {
        if(empty($username)){
            $this->customErrorMessage('Username is missing');
            exit;
        }

        $getUsers = $this->cisuiteDb->prepare("SELECT * FROM ciPartner_partner WHERE cprTitle = :username");
        $getUsers->bindParam('username',$username);
        $getUsers->execute();
        $getUsers = $getUsers->fetchAll(PDO::FETCH_OBJ);

        return $getUsers;
    }

    private function getUserIdByEmail($email) {
        if(empty($email)){
            $this->customErrorMessage('Email is missing');
            exit;
        }

        // fk_cfl_cprID = :userId
        $field_email_id = FIELD_EMAIL_ID;

        $findUserField = $this->cisuiteDb->prepare("SELECT * FROM ciPartner_fieldlink 
                                                    WHERE cflValue = :fieldValue AND fk_cfl_cfdID = :fieldId AND cflActive = 1");
        $findUserField->bindParam('fieldValue', $email);
        $findUserField->bindParam('fieldId', $field_email_id);
        $findUserField->execute();
        $findUserField = $findUserField->fetchAll(PDO::FETCH_OBJ);

        return count($findUserField) > 0 ? $findUserField[0]->fk_cfl_cprID : '';
    }

    public function getUsersByEmail($email) {
        if(empty($email)){
            $this->customErrorMessage('Email is missing');
            exit;
        }

        $userId = $this->getUserIdByEmail($email);

        if (empty($userId)) {
            return NULL;
        }

        return $this->getUsersByUserId($userId);
    }

    public function getCiUsers() {
        $getCiUsers = $this->cisuiteDb->prepare("SELECT * FROM ci_users
                                                    WHERE isDeleted = 0");
        $getCiUsers->execute();
        $getCiUsers = $getCiUsers->fetchAll(PDO::FETCH_OBJ);
        return $getCiUsers;
    }

    public function emailFormRequest($userId, $strSubject, $strContent) {
        if (empty($userId)) {
            $this->customErrorMessage('Missing User ID');
            exit;
        }

        $user = $this->getSingleUserByUserId($userId);
        if (empty($user)) {
            $this->customErrorMessage('User ID is not valid');
            exit;
        }

        $email = $this->getUserFieldValue($userId, FIELD_EMAIL_ID);
        $phone = $this->getUserFieldValue($userId, FIELD_PHONE_ID);

        $subject = 'Streamline Plus | '.$strSubject;
        $message = '
Request From<br>
User: '.$user->cprTitle.'<br>
Email: '.$email.'<br>
Contact Number: '.$phone.'<br>
<br>
With following details<br>
'.$strContent.'
        ';

        $admins = $this->getCiUsers();

        foreach ($admins as $admin) {
            if (isset($admin->EMAIL)) {
                $this->sendSLPEmail($admin->EMAIL, $subject, $message);
            }
        }
    }
}