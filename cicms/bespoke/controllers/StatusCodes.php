<?php

class StatusCodes {

    public $messages = array(
        '1'=>array(
            'status_code'=>200,
            'status_message'=>'Success'
        ),
        '2'=>array(
            'status_code'=>422,
            'status_message'=>'Your email and/or password is missing or contains invalid characters. Please check your entries and try again.'
        ),
        '3'=>array(
            'status_code'=>500,
            'status_message'=>'The server is temporarily unavailable, please wait a few seconds and try again.'
        ),
        '4'=>array(
            'status_code'=>404,
            'status_message'=>'Server Missing (App)'
        ),
        '5'=>array(
            'status_code'=>404,
            'status_message'=>'Timeout (App)'
        ),
        '6'=>array(
            'status_code'=>404,
            'status_message'=>'General'
        ),
        '7'=>array(
            'status_code'=>404,
            'status_message'=>'No Route'
        ),
        '8'=>array(
            'status_code'=>403,
            'status_message'=>'API Key Failed'
        ),
        '9'=>array(
            'status_code'=>403,
            'status_message'=>'We were unable to match the email and/or password you entered. Please check your entries and try again.'
        ),
        '11'=>array(
            'status_code'=>403,
            'status_message'=>'General Error'
        ),
        '12'=>array(
            'status_code'=>401,
            'status_message'=>'Invalid Token'
        ),
        '13'=>array(
            'status_code'=>501,
            'status_message'=>'Apache/Nginx Pathing Issue'
        ),
        '14'=>array(
            'status_code'=>403,
            'status_message'=>'This email has already been used to register to the system. Please choose a different email address and try again.'
        ),
        '15'=>array(
            'status_code'=>403,
            'status_message'=>'Not Allowed'
        ),
        '16'=>array(
            'status_code'=>403,
            'status_message'=>'Webview Not Authorized'
        ),
        '17'=>array(
            'status_code'=>403,
            'status_message'=>'This username has already been used to register to the system. Please choose a different username and try again.'
        ),
        '18'=>array(
            'status_code'=>422,
            'status_message'=>'Email Not Verified'
        ),
        '19'=>array(
            'status_code'=>422,
            'status_message'=>'Password Not Verified'
        ),
        '20'=>array(
            'status_code'=>422,
            'status_message'=>'Menu Modules Link Broken'
        ),
        '21'=>array(
            'status_code'=>403,
            'status_message'=>'The login details is not active.'
        ),
        '22'=>array(
            'status_code'=>422,
            'status_message'=>'Email is not valid or Activation Link is expired.'
        ),
        '23'=>array(
            'status_code'=>422,
            'status_message'=>'Invalid/missing fields to process the request.'
        ),
        '24'=>array(
            'status_code'=>422,
            'status_message'=>'The registration process is completed. However, there is an error of converting the base64 string.'
        ),
        '25'=>array(
            'status_code'=>422,
            'status_message'=>'The registration process is completed. However, the image upload failed.'
        ),
        '26'=>array(
            'status_code'=>422,
            'status_message'=>'There is not enough sample in the inventory.'
        ),
        '27'=>array(
            'status_code'=>422,
            'status_message'=>'You are not allowed to update the record which was not created by you.'
        ),
        '28'=>array(
            'status_code'=>422,
            'status_message'=>'Email is not valid or Reset Link is expired.'
        ),
    );
}