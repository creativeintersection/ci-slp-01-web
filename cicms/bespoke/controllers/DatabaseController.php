<?php

class DatabaseController {
    protected $db;
    public function __construct($host, $port, $dbname, $user, $pass){

        $dsn = 'mysql:host=' . $host . ';port=' . $port . ';dbname=' . $dbname;

        try {
            $conn = new PDO($dsn, $user, $pass);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $conn->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
            $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $this->db = $conn;
        }
        catch(PDOException $e) {
            echo '<pre>';
            echo $e->getMessage();
            echo '</pre>';

        }
    }

    public function getConnection()
    {
        return $this->db;
    }
}