<?php

trait HelperFunctions
{
    public function formatAustraliaPhoneNumber($phone) {
        return preg_replace('/^0/','+61',$phone);
    }

    public function sanitiseFileName($filename) {
        // Remove anything which isn't a word, whitespace, number
        // or any of the following caracters -_~,;[]().
        // If you don't need to handle multi-byte characters
        // you can use preg_replace rather than mb_ereg_replace
        $filename = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $filename);
        // Remove any runs of periods
        $filename = mb_ereg_replace("([\.]{2,})", '', $filename);
        return $filename;
    }

    public function getActualFileName($pathWithEndingSlash, $originalName, $ext){
        $i = 1;
        $actualName = (string)$originalName;
        while(file_exists($pathWithEndingSlash.$actualName.'.'.$ext))
        {
            $actualName = (string)$originalName.$i;
            $i++;
        }
        return $actualName;
    }

    public function getDatetimeMicrosecondsAsFileName() {
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        $d = new \DateTime(date('Y-m-d H:i:s.'.$micro, $t));
        return $d->format('YmdHis-u');
    }

    public function deleteFile($path) {
        $fileLocation = $_SERVER['DOCUMENT_ROOT'].$path;
        if(file_exists($fileLocation)) {
            unlink($fileLocation);
        }
    }

    public function useCurl($url, $headers, $fields = null) {
        // Open connection
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL,$url);
        curl_setopt($curl,CURLOPT_POST , 1);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER , true);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($curl,CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        if ($response === FALSE) {
            die('Curl failed: ' . curl_error($curl));
        }
        curl_close($curl);
//        var_dump($response);var_dump($err);exit;
        return $response;
    }

    public static function generateV4UUID() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public function generateTemporaryPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    /**
     * Convert a date(time) string to another format or timezone
     *
     * DateTime::createFromFormat requires PHP >= 5.3
     *
     * @param string $dt
     * @param string $tz1
     * @param string $df1
     * @param string $tz2
     * @param string $df2
     * @return string
     */
    public function date_convert($dt, $tz1, $df1, $tz2, $df2) {
        // $dateFrom = $this->date_convert($dateFrom, $timezone, 'Y-m-d H:i:s', 'UTC', 'Y-m-d H:i:s');
        $res = '';
        if(!in_array($tz1, timezone_identifiers_list())) { // check source timezone
            trigger_error(__FUNCTION__ . ': Invalid source timezone ' . $tz1, E_USER_ERROR);
        } elseif(!in_array($tz2, timezone_identifiers_list())) { // check destination timezone
            trigger_error(__FUNCTION__ . ': Invalid destination timezone ' . $tz2, E_USER_ERROR);
        } else {
            // create DateTime object
            $d = \DateTime::createFromFormat($df1, $dt, new \DateTimeZone($tz1));
            // check source datetime
            if($d && \DateTime::getLastErrors()["warning_count"] == 0 && \DateTime::getLastErrors()["error_count"] == 0) {
                // convert timezone
                $d->setTimeZone(new \DateTimeZone($tz2));
                // convert dateformat
                $res = $d->format($df2);
            } else {
                trigger_error(__FUNCTION__ . ': Invalid source datetime ' . $dt . ', ' . $df1, E_USER_ERROR);
            }
        }
        return $res;
    }

    public function getLocalDateTime($strUtcDateTime, $tz) {
        return $this->date_convert($strUtcDateTime, 'UTC', 'Y-m-d H:i:s', $tz, 'Y-m-d H:i:s');
    }

    public function getUtcDateTime($strLocalDateTime, $tz) {
        return $this->date_convert($strLocalDateTime, $tz, 'Y-m-d H:i:s', 'UTC', 'Y-m-d H:i:s');
    }

    public function getDayOnly($strDateTime) {
        return date('l', strtotime($strDateTime));
    }

    public function getDateOnly($strDateTime) {
        return date('d-m-Y', strtotime($strDateTime));
    }

    public function getTimeOnly($strDateTime) {
        return date('h:ia', strtotime($strDateTime));
    }

    public function sendSLPEmail($email, $subject, $message) {
        global $slpBaseDomain;

        $theServer = 'noreply@'.$slpBaseDomain;
        $headers  = "From: ".$theServer."\n";
        $headers .= "Reply-To: ".$theServer."\n";
        $headers .= "X-Sender: ".$theServer."\n";
        $headers .= 'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\n";
//        $headers .= "Content-Type: text/plain; charset=utf-8\n";
        $headers .= "Content-Type: text/html; charset=iso-8859-1;\n";

        @mail($email, $subject, $message, $headers);
    }

    public function sendEmailConfirmationWithLoginDetails($email, $username, $password) {
        global $slpBaseUrl;
        $hash = md5( rand(0,1000) ); // Generate random 32 character hash and assign it to a local variable.

        $updateHash = $this->db->prepare("UPDATE user SET hash = :hash WHERE email = :email");
        $updateHash->bindParam('email',$email);
        $updateHash->bindParam('hash',$hash);

        $updateHash->execute();

        $subject = 'Streamline Plus Signup | Verification'; // Give the email a subject
        $message = '
Thanks for signing up!
Your account has been created, you can login with your credentials that you have registered with us after you activate your account by pressing the url below.
Your login details are:
Username: '.$username.'
Temporary Password: '.$password.'
After login, please change your password.
Please click this link to activate your account:
'.$slpBaseUrl.'/verify?email='.$email.'&hash='.$hash;

        $this->sendSLPEmail($email, $subject, $message);
    }
}