<?php

require_once 'ErrorHanlder.php';
require_once 'HelperFunctions.php';

class PagesController {

    private $apiDb;
    private $cisuiteDb;

    public function __construct($apiDb, $cisuiteDb){
        $this->apiDb = $apiDb;
        $this->cisuiteDb = $cisuiteDb;
    }

    use ErrorHandler;
    use HelperFunctions;

    public function getPageTitle($slug){
        $getPageTitle = $this->cisuiteDb->prepare("SELECT * FROM pages WHERE PAGE_ALIAS = :slug AND isLivePage = 1");
        $getPageTitle->bindParam('slug', $slug);
        $getPageTitle->execute();
        $getPageTitle = $getPageTitle->fetchAll(PDO::FETCH_OBJ);

        if(count($getPageTitle)){
            return $getPageTitle[0]->PAGE_TITLE;
        }
        return '';
    }

    public function getCisuitePagesByGroupId($groupId) {
        $cisuitePages = $this->cisuiteDb->prepare("SELECT pages.* FROM pages
                                                    LEFT JOIN cigroup_page_links cpl ON cpl.fk_col_cprID = pages.PAGE_ID
                                                    WHERE pages.PAGE_REL = 1 AND pages.isLivePage = 1 AND cpl.fk_col_copID = :groupId AND cpl.colActive = 1");
        $cisuitePages->bindParam("groupId", $groupId);
        $cisuitePages->execute();
        $cisuitePages = $cisuitePages->fetchAll(PDO::FETCH_OBJ);

        return $cisuitePages;
    }

    public function getCisuitePagesBySectionId($sectionId) {
        $cisuitePages = $this->cisuiteDb->prepare("SELECT pages.* FROM pages
                                                    WHERE pages.PAGE_REL = 1 AND pages.isLivePage = 1 AND pages.PG_SEC_ID = :sectionId");
        $cisuitePages->bindParam("sectionId", $sectionId);
        $cisuitePages->execute();
        $cisuitePages = $cisuitePages->fetchAll(PDO::FETCH_OBJ);

        return $cisuitePages;
    }

    public function getCisuiteAssetsByPageId($pageId, $groupId) {
        $cisuiteAssets = $this->cisuiteDb->prepare("SELECT * FROM assets
                                                    WHERE PAGE_REF_ID = :pageId AND ASSET_COMP_GR = :groupId
                                                    ORDER BY ASSET_COMP_GR ASC, ASSET_COMP_SORT ASC, ASSET_INST_ID ASC");
        $cisuiteAssets->bindParam("pageId", $pageId);
        $cisuiteAssets->bindParam("groupId", $groupId);
        $cisuiteAssets->execute();
        $cisuiteAssets = $cisuiteAssets->fetchAll(PDO::FETCH_OBJ);

        return $cisuiteAssets;
    }

    public function getAssetsByTypeId($assets, $typeId) {
        $arrAssets = array();
        
        foreach($assets as $asset) {
            if ((int)$asset->ASSET_INST_ID == (int)$typeId) {
                $arrAssets[] = $asset;
            }
        }
        
        return $arrAssets;
    }
}