<?php

require_once 'ErrorHanlder.php';
require_once 'HelperFunctions.php';

class PushNotifications {

    private $db;
    private $applePushNotificationsConfig;
    private $androidPushNotificationsConfig;
    private $iOSUrl;
    private $androidUrl;
    private $jwtHeaderAuthorization;
    private $jwtHeaderApiKey;

    public function __construct($db, $applePushNotificationsConfig, $androidPushNotificationsConfig, $iOSUrl, $androidUrl, $jwtHeaderAuthorization, $jwtHeaderApiKey) {
        $this->db = $db;
        $this->applePushNotificationsConfig = $applePushNotificationsConfig;
        $this->androidPushNotificationsConfig = $androidPushNotificationsConfig;
        $this->iOSUrl = $iOSUrl;
        $this->androidUrl = $androidUrl;
        $this->jwtHeaderAuthorization = $jwtHeaderAuthorization;
        $this->jwtHeaderApiKey = $jwtHeaderApiKey;
    }

    use ErrorHandler;
    use HelperFunctions;

    public function sendAndroidPushNotification($fields, $unreadMessageNumber, $isSilent) {
        $headers = array(
            'authorization: '.$this->jwtHeaderAuthorization,
            'x-apikey: '.$this->jwtHeaderApiKey
        );

        $payload = array(
            'title' => 'Streamline Plus Notification',
            'body'  => strip_tags($fields['body']),
            'badge' => $unreadMessageNumber,
            'sound' => ''
        );

        if ($isSilent) {
            $payload = array_merge($payload, array(
                'silent' => 1
            ));
            $customPayload = array();
        }
        else {
            $customPayload = array(
                'link' => $fields['link']
            );
        }

        $postData = base64_encode(
            serialize(
                array_merge(
                    array('payload'=>$payload),
                    array('config'=>$this->androidPushNotificationsConfig),
                    array('customPayload'=>$customPayload)
                )
            )
        );

        $newFields = array(
            'token' => $fields['device_token'],
            'body' => $postData
        );

        return $this->useCurl($this->androidUrl, $headers, $newFields);
    }

    public function sendApplePushNotification($fields, $unreadMessageNumber, $isSilent) {
        $headers = array(
            'authorization: '.$this->jwtHeaderAuthorization,
            'x-apikey: '.$this->jwtHeaderApiKey
        );

        $payload = array(
            'badge' => $unreadMessageNumber
        );

        if ($isSilent) {
            $payload = array_merge($payload, array(
                'title' => '',
                'body'  => '',
                'content-available' => 1,
                'sound' => ''
            ));
            $customPayload = array();
        }
        else {
            $payload = array_merge($payload, array(
                'title' => 'Streamline Plus Notification',
                'body'  => strip_tags($fields['body']),
                'sound' => 'default'
            ));
            $customPayload = array(
                'link' => $fields['link']
            );
        }

        $postData = base64_encode(
            serialize(
                array_merge(
                    array('payload'=>$payload),
                    array('config'=>$this->applePushNotificationsConfig),
                    array('customPayload'=>$customPayload)
                )
            )
        );

        $newFields = array(
            'token' => $fields['device_token'],
            'body' => $postData
        );

        return $this->useCurl($this->iOSUrl, $headers, $newFields);
    }

    // Sends Push's toast notification for Windows Phone 8 users
    public function WP($data, $uri) {
        $delay = 2;
        $msg =  "<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
            "<wp:Notification xmlns:wp=\"WPNotification\">" .
            "<wp:Toast>" .
            "<wp:Text1>".htmlspecialchars($data['mtitle'])."</wp:Text1>" .
            "<wp:Text2>".htmlspecialchars($data['mdesc'])."</wp:Text2>" .
            "</wp:Toast>" .
            "</wp:Notification>";

        $sendedheaders =  array(
            'Content-Type: text/xml',
            'Accept: application/*',
            'X-WindowsPhone-Target: toast',
            "X-NotificationClass: $delay"
        );

        $response = $this->useCurl($uri, $sendedheaders, $msg);

        $result = array();
        foreach(explode("\n", $response) as $line) {
            $tab = explode(":", $line, 2);
            if (count($tab) == 2)
                $result[$tab[0]] = trim($tab[1]);
        }

        return $result;
    }

    public function createNotification($fields, $toUserId){
        try {
            $deviceToken = isset($fields['device_token']) ? $fields['device_token'] : NULL;
            $devicePlatform = isset($fields['device_platform']) ? $fields['device_platform'] : 0;
            $body = isset($fields['body']) ? $fields['body'] : '';
            $link = isset($fields['link']) ? $fields['link'] : '';

            if (empty($toUserId)) {
                $this->customErrorMessage('Missing To User ID');
                exit;
            }

            $createNotification = $this->db->prepare("INSERT INTO push_notification (
                                                      is_read,
                                                      device_token,
                                                      device_platform,
                                                      body,
                                                      link,
                                                      to_user_id,
                                                      created_at
                                                  )
                                                 VALUES (
                                                      0,
                                                      :deviceToken,
                                                      :devicePlatform,
                                                      :body,
                                                      :link,
                                                      :toUserId,
                                                      utc_timestamp()
                                                  )");

            $createNotification->bindParam('deviceToken',$deviceToken);
            $createNotification->bindParam('devicePlatform',$devicePlatform);
            $createNotification->bindParam('body',$body);
            $createNotification->bindParam('link',$link);
            $createNotification->bindParam('toUserId',$toUserId);
            $createNotification->execute();

            $pushId = $this->db->lastInsertId();

            return $pushId;
        }
        catch (\PDOException $e) {
            var_dump($e);
        }
    }

    public function markPushNotificationAsRead($userId) {
        try {
            if(empty($userId)){
                $this->customErrorMessage('Missing User Id');
                exit;
            }

            $updateNotification = $this->db->prepare("UPDATE push_notification SET
                                                    is_read = 1
                                                WHERE to_user_id  = :userId");
            $updateNotification->bindParam('userId',$userId);
            $updateNotification->execute();

            return $userId;
        }
        catch (\PDOException $e) {
            var_dump($e);
        }
    }

    public function markPushNotificationAsReadForLink($userId, $link) {
        try {
            if(empty($userId)){
                $this->customErrorMessage('Missing User Id');
                exit;
            }

            $updateNotification = $this->db->prepare("UPDATE push_notification SET
                                                    is_read = 1
                                                WHERE to_user_id  = :userId AND link LIKE '%".$link."%'");
            $updateNotification->bindParam('userId',$userId);
            $updateNotification->execute();

            return $userId;
        }
        catch (\PDOException $e) {
            var_dump($e);
        }
    }

    public function getUnreadPushNotifications($toUserId) {
        try {
            if(empty($toUserId)){
                $this->customErrorMessage('Missing To User Id');
                exit;
            }

            $getNotifications = $this->db->prepare("SELECT * FROM push_notification
                                                    WHERE to_user_id = :toUserId AND is_read = 0");

            $getNotifications->bindParam('toUserId',$toUserId);
            $getNotifications->execute();
            $getNotifications = $getNotifications->fetchAll(PDO::FETCH_OBJ);

            return $getNotifications;
        }
        catch (\PDOException $e) {
            var_dump($e);
        }
    }
}
?>