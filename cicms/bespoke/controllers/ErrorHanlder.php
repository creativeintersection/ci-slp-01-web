<?php

trait ErrorHandler {
    public function throwError($type){
        $sc = new StatusCodes();
        $statuscodes = $sc->messages[$type];
        if ($statuscodes['status_code'] == null) {
            // header("HTTP/1.1 404");
            header('Content-Type: application/json');
            echo json_encode(array('error' => 'Unknown Error'));
            http_response_code(404);
        } else {
            header('Content-Type: application/json');
            echo json_encode(array('error' => $statuscodes['status_message']));
        }
    }
    
    public function customErrorMessage($message) {
        header('Content-Type: application/json');
        echo json_encode(array('error' => $message));
    }
}