//App::registerAutoloader();
require_once BASE_DIR . '../../vendor/autoload.php';

$app = new Slim\Slim(array(
'debug'               => $debugEnabled,

'mode'                => $env,
//    'log.enabled'         => false,
//    'log.path'            => BASE_DIR . '/logs',
//    'log.level'           => $debugEnabled ? \Slim\Log::DEBUG : \Slim\Log::ERROR,
//    'log.writer'          => new \Slim\Logger\DateTimeFileWriter(),

//'cookies.encrypt'     => true,
//'cookies.secure'      => false,  // Works only over HTTPS.
//'cookies.httponly'    => true,
//'cookies.secret_key'  => 'CHANGEME',
//'cookies.cipher'      => MCRYPT_RIJNDAEL_256,
//'cookies.cipher_mode' => MCRYPT_MODE_CBC,
//'cookies.lifetime'    => '2 days',

//'view'                => new Slim\View(), // NOTE: this is the default
'templates.path'      => BASE_DIR . '/',
'settings'            => array('determineRouteBeforeAppMiddleware' => true)
));

//$app->get('/', function () use ($app, $bespokeController) {
//    $app->render('slp/views/home.php');
//});
//
//$cisuiteExternalPages = $bespokeController->getCisuiteExternalPages();
//$cisuiteInternalPages = $bespokeController->getCisuiteInternalPages();
//
//foreach($cisuiteExternalPages as $pageKey=>$pageObject){
//
//    $path = $pageObject->PAGE_ALIAS;
//    $pageId = $pageObject->PAGE_ID;
//    $pageTitle = $pageObject->PAGE_TITLE;
//
//    if(empty($path)){
//        continue;
//    }
//
//    if ((string)$path == 'activation') {
//        $app->get('/'.$path, function () use ($pageId, $pageTitle, $app, $bespokeController) {
//            global $slpBaseUrl;
//            echo file_get_contents(
//                $slpBaseUrl.'/cisuite-index.php?preview=34'
//                .'&pid='.$pageId
//                .'&page_title='.urlencode($pageTitle)
//                .'&user_id='.urlencode(isset($_GET['user_id']) ? $_GET['user_id'] : '')
//                .'&hash='.urlencode(isset($_GET['hash']) ? $_GET['hash'] : '')
//            );
//        });
//    }
//    else {
//        $app->get('/'.$path, function () use ($pageId, $pageTitle, $app, $bespokeController) {
//            global $slpBaseUrl;
//            echo file_get_contents(
//                $slpBaseUrl.'/cisuite-index.php?preview=34'
//                .'&pid='.$pageId
//                .'&page_title='.urlencode($pageTitle)
//            );
//        });
//    }
//}
//
//foreach($cisuiteInternalPages as $pageKey=>$pageObject){
//
//    $path = $pageObject->PAGE_ALIAS;
//    $pageId = $pageObject->PAGE_ID;
//    $pageTitle = $pageObject->PAGE_TITLE;
//
//    if(empty($path)){
//        continue;
//    }
//
//    if ((string)$path == 'settings') {
//        $app->get('/'.$path, 'validateJwt', function () use ($pageId, $pageTitle, $app, $bespokeController) {
//            global $slpBaseUrl;
//            $userId = $_SESSION['slp']['userId'];
//
//            $phone = $bespokeController->getUserFieldValue($userId, FIELD_PHONE_ID);
//            $email = $bespokeController->getUserFieldValue($userId, FIELD_EMAIL_ID);
//            $isPushed = $bespokeController->getUserFieldValue($userId, FIELD_PUSH_NOTIFIED);
//
//            if(empty($data)) {
//                $data = array();
//            }
//
//            includeCommonViewData($data);
//
//            $usingApp = isset($data['usingApp']) ? $data['usingApp'] : '';
//            $appJWT = isset($data['appJWT']) ? $data['appJWT'] : '';
//            $appUser = isset($data['appUser']) ? $data['appUser'] : array();
//
//            $strGet =
//                '&using_app='.urlencode($usingApp)
//                .'&app_jwt='.urlencode($appJWT)
////                .'&app_user='.http_build_query($appUser)
//            ;
//
//            echo file_get_contents(
//                $slpBaseUrl.'/cisuite-index.php?preview=34'
//                .'&pid='.$pageId
//                .'&page_title='.urlencode($pageTitle)
//                .'&user_name='.urlencode($_SESSION['slp']['username'])
//                .'&phone='.urlencode($phone)
//                .'&email='.urlencode($email)
//                .'&is_pushed='.urlencode($isPushed)
//                .$strGet
//            );
//        });
//    }
//    else {
//        $app->get('/'.$path, 'validateJwt', function () use ($pageId, $pageTitle, $app, $bespokeController) {
//            global $slpBaseUrl;
//
//            if(empty($data)) {
//                $data = array();
//            }
//
//            includeCommonViewData($data);
//
//
//            $usingApp = isset($data['usingApp']) ? $data['usingApp'] : '';
//            $appJWT = isset($data['appJWT']) ? $data['appJWT'] : '';
//            $appUser = isset($data['appUser']) ? $data['appUser'] : array();
//
//            $strGet =
//                '&using_app='.urlencode($usingApp)
//                .'&app_jwt='.urlencode($appJWT)
////                .'&app_user='.http_build_query($appUser)
//            ;
//
//            echo file_get_contents(
//                $slpBaseUrl.'/cisuite-index.php?preview=34'
//                .'&pid='.$pageId
//                .'&page_title='.urlencode($pageTitle)
//                .'&user_name='.urlencode($_SESSION['slp']['username'])
//                .$strGet
//            );
//        });
//    }
//}

//$app->notFound(function() use($app){
//    if(isAjaxRequest() || isset($_SESSION['slp']['jwt'])) {
//        sendNotFoundResponse();
//        exit;
//    }
//    else{
//        $app->redirect('/login');
//    }
//});

//$app->get('/coming-soon', function () use ($app, $bespokeController) {
//    renderView('coming-soon.php', array(
//        'pageTitle'=>'Coming Soon'
//    ));
//});

//$app->get('/settings', 'validateJwt', function () use ($app, $bespokeController) {
//    $userId = $_SESSION['slp']['userId'];
//
//    $phone = $bespokeController->getUserFieldValue($userId, FIELD_PHONE_ID);
//    $email = $bespokeController->getUserFieldValue($userId, FIELD_EMAIL_ID);
//    $isPushed = $bespokeController->getUserFieldValue($userId, FIELD_PUSH_NOTIFIED);
//
//    renderView('slp/views/settings.php', array(
//        'pageTitle'=>'Settings | Streamline Plus',
//        'username'=>$_SESSION['slp']['username'],
//        'phone'=>$phone,
//        'email'=>$email,
//        'isPushed'=>$isPushed
//    ));
//});