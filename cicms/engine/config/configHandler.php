<?php

require realpath(__DIR__ . '/..').  '/../config/cicms_userconfig.php';


/**
 * Created by Jayden Balm, CreativeIntersection
 */

class ConfigHandler{
    private $databaseLogin_host;
    private $databaseLogin_user;
    private $databaseLogin_pass;
    private $databaseLogin_table;

    private $googlemaps_api_key;
    private $AdminFCMKey;
    private $PoweredByFCMKey;

    private $poweredappalias;
    private $poweredappaliasid;

    private $adminfavouritesalias;
    private $adminfavouritesaliasid;

    private $streamflowpreviewpage;

    private $dateTimezone;

    public function __construct(){
        $this->ConfigHandler();
    }

    public function ConfigHandler(){
        global $cicms_db_host,$cicms_db_name,$cicms_db_user,$cicms_db_pass;

        $this->setDatabaseLoginHost($cicms_db_host);
        $this->setDatabaseLoginTable($cicms_db_name);
        $this->setDatabaseLoginUser($cicms_db_user);
        $this->setDatabaseLoginPass($cicms_db_pass);

        $this->setPoweredAppAlias('Fh6383*tN');
        $this->setPoweredAppAliasID(100);

        $this->setAdminFavouritesAlias('hJ4452tJ');

        $this->setGooglemapsApiKey('AIzaSyC5We5X1ATDC0pUNvpv8zlkDR_nN0VSPO4');

        $this->setAdminFCMKey('AAAAKP0P2y8:APA91bHoXomddEHH5WCbYMU3UQ-lwbO2v02RheRIIlAdmC0gt0RVtN6MIGZv_u6rDZRnanRf_sniydGKYsqsbpCZI7_rcNiO60p5ZHCWx-2w6NWu2L62TItFu3vhEsbriLDn3dHSwmYz');
        $this->setPoweredByFCMKey('AAAA2EvLiaM:APA91bHm2f1idhfbsykkiqImlWD3RrBV2cSxPQCiABzz0tN5_b2kZLVi8RqjCaMOJJ8VALYAcFVgHW1R09aEBGItxObDSzovH-0k5d0t7Q5D9BXuOmkMik-_0gGN8Cz8QpgJ-XSKzpZB');

        $this->setDateTimezone('Australia/Brisbane');

        $this->setStreamflowPreviewPage(166);

    }

    public function getConfig(){
        return array(
            "poweredapp_alias"=>$this->getPoweredAppAlias(),
            "poweredapp_aliasid"=>$this->getPoweredAppAliasID(),
            "admin_favouritesalias"=>$this->getAdminFavouritesAlias(),
            "admin_favouritesaliasid"=>$this->getAdminFavouritesAliasID(),
            "admin_fcmkey"=>$this->getAdminFCMKey(),
            "poweredapp_fcmkey"=>$this->getPoweredByFCMKey(),
            "timezone"=>$this->getDateTimezone(),
            "streamflow_previewpage"=>$this->getStreamflowPreviewPage()
        );
    }


    /**
     * @return mixed
     */
    public function getAdminFavouritesAlias()
    {
        return $this->adminfavouritesalias;
    }

    /**
     * @return mixed
     */
    public function getAdminFavouritesAliasID()
    {
        return $this->adminfavouritesaliasid;
    }


    /**
     * @param mixed
     */
    public function setAdminFavouritesAlias($adminfavouritesalias)
    {
        $this->adminfavouritesalias = $adminfavouritesalias;
    }


    /**
     * @return mixed
     */
    public function getPoweredAppAlias()
    {
        return $this->poweredappalias;
    }

    /**
     * @param mixed
     */
    public function setPoweredAppAlias($poweredappalias)
    {
        $this->poweredappalias = $poweredappalias;
    }


    /**
     * @return mixed
     */
    public function getPoweredAppAliasID()
    {
        return $this->poweredappaliasid;
    }

    /**
     * @param mixed
     */
    public function setPoweredAppAliasID($poweredappaliasid)
    {
        $this->poweredappaliasid = $poweredappaliasid;
    }


    /**
     * @return mixed
     */
    public function getStreamflowPreviewPage()
    {
        return $this->streamflowpreviewpage;
    }

    /**
     * @param mixed $streamflowpreviewpage
     */
    public function setStreamflowPreviewPage($streamflowpreviewpage)
    {
        $this->streamflowpreviewpage = $streamflowpreviewpage;
    }


    /**
     * @return mixed
     */
    public function getDateTimezone()
    {
        return $this->dateTimezone;
    }

    /**
     * @param mixed $dateTimezone
     */
    public function setDateTimezone($dateTimezone)
    {
        $this->dateTimezone = $dateTimezone;
    }



    /**
     * @return mixed
     */
    public function getGooglemapsApiKey()
    {
        return $this->googlemaps_api_key;
    }

    /**
     * @param mixed $googlemaps_api_key
     */
    public function setGooglemapsApiKey($googlemaps_api_key)
    {
        $this->googlemaps_api_key = $googlemaps_api_key;
    }

    /**
     * @return mixed
     */
    public function getDatabaseLoginHost()
    {
        return $this->databaseLogin_host;
    }

    /**
     * @param mixed $databaseLogin_host
     */
    public function setDatabaseLoginHost($databaseLogin_host)
    {
        $this->databaseLogin_host = $databaseLogin_host;
    }

    /**
     * @return mixed
     */
    public function getDatabaseLoginUser()
    {
        return $this->databaseLogin_user;
    }

    /**
     * @param mixed $databaseLogin_user
     */
    public function setDatabaseLoginUser($databaseLogin_user)
    {
        $this->databaseLogin_user = $databaseLogin_user;
    }

    /**
     * @return mixed
     */
    public function getDatabaseLoginPass()
    {
        return $this->databaseLogin_pass;
    }

    /**
     * @param mixed $databaseLogin_pass
     */
    public function setDatabaseLoginPass($databaseLogin_pass)
    {
        $this->databaseLogin_pass = $databaseLogin_pass;
    }

    /**
     * @return mixed
     */
    public function getDatabaseLoginTable()
    {
        return $this->databaseLogin_table;
    }

    /**
     * @param mixed $databaseLogin_table
     */
    public function setDatabaseLoginTable($databaseLogin_table)
    {
        $this->databaseLogin_table = $databaseLogin_table;
    }

    /**
     * @return mixed
     */
    public function getAdminFCMKey()
    {
        return $this->AdminFCMKey;
    }

    /**
     * @param mixed $fcm_api_key
     */
    public function setAdminFCMKey($AdminFCMKey)
    {
        $this->AdminFCMKey = $AdminFCMKey;
    }

    /**
     * @return mixed
     */
    public function getPoweredByFCMKey()
    {
        return $this->PoweredByFCMKey;
    }

    /**
     * @param mixed $fcm_api_key
     */
    public function setPoweredByFCMKey($PoweredByFCMKey)
    {
        $this->PoweredByFCMKey = $PoweredByFCMKey;
    }



    public function modules(){
        return array(
            "cicms"=>array(
                "path"=>"",
                "label"=>"Content",
                "icon"=>"fa-database",
                "enabled"=>true
            ),
            "cicart"=>array(
                "path"=>"cicart",
                "label"=>"Shop",
                "icon"=>"fa-shopping-cart",
                "submodules"=>array(
                    "cicart"=>array(
                        "path"=>"",//Empty Because it links to itself (/cicart/, it would be /cicart/[value] if path is entered
                        "label"=>"Products",
                        "icon"=>"fa-shopping-basket",
                        "enabled"=>true
                    ),
                    "productoptions"=>array(
                        "path"=>"productoptions",
                        "label"=>"Product Options",
                        "icon"=>"fa-bar-chart",
                        "enabled"=>true
                    )
                ),
                "enabled"=>false
            ),
            "ciuser"=>array(
                "path"=>"ciuser",
                "label"=>"Users",
                "icon"=>"fa-user",
                "enabled"=>true
            ),
            "cigroups"=>array(
                "path"=>"cigroups",
                "label"=>"Groups",
                "icon"=>"fa-object-group",
                "enabled"=>true
            ),
            "cipartner"=>array(
                "path"=>"cipartner",
                "label"=>"Records",//Label in Modules List
                "label_menu"=>"Records",//Label for Menu
                "label_nonplural"=>"Record",//Label for Menu
                "label_category"=>"New Record Category",//Label for New Category
                "label_record"=>"New Record",//Label for new Record
                "icon"=>"fa-id-card",
                "enabled"=>true
            ),
            "cisurvey"=>array(
                "path"=>"cisurvey",
                "label"=>"Surveys",
                "label_menu"=>"Surveys",
                "label_nonplural"=>"Survey",
                "label_category"=>"New Survey Category",
                "label_record"=>"New Survey",
                "icon"=>"fa-wpforms",
                "enabled"=>false
            ),
            "cistreamflow"=>array(
                "path"=>"cistreamflow",
                "label"=>"Streams",
                "label_menu"=>"Streams",
                "label_nonplural"=>"Stream",
                "label_category"=>"New Stream Section",
                "label_record"=>"New Stream",
                "icon"=>"fa-book",
                "enabled"=>false
            ),
            "ciorders"=>array(
                "path"=>"ciorders/orders",//Slash orders to bypass error thrown from mobile
                "label"=>"Orders",
                "label_menu"=>"Orders",
                "label_nonplural"=>"Order",
                "label_category"=>"New Order Section",//Unused For this Deployment
                "label_record"=>"New Order",//Unused For this Deployment
                "icon"=>"fa-archive",
                "enabled"=>false
            ),
            "notifications"=>array(
                "path"=>"notifications",
                "label"=>"Notifications",
                "icon"=>"fa-bell",
                "enabled"=>true
            )
        );
    }


}