/*
  ciSUITE: Database Setup Script
 */

# Setup ciPartner definitions/fields
CREATE TABLE IF NOT EXISTS`ciPartner_partcdefs` (
  `cpdID` int(4) NOT NULL,
  `cpdCategory` varchar(100) NOT NULL DEFAULT '',
  `cpdName` varchar(100) NOT NULL DEFAULT '',
  `cpdDescription` varchar(255) NOT NULL DEFAULT '',
  `cpdFields` varchar(100) NOT NULL DEFAULT '',
  `cpdOptions` varchar(255) NOT NULL DEFAULT '',
  `cpdTempalteID` int(4) DEFAULT NULL,
  `cpdCatPageID` int(4) NOT NULL DEFAULT '0',
  `cpdProductPageID` int(4) NOT NULL DEFAULT '0',
  `fk_cpd_domID` int(4) NOT NULL DEFAULT '0',
  `cpdSort` int(4) NOT NULL DEFAULT '0',
  `cpdActive` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `ciPartner_partcdefs` (`cpdID`, `cpdCategory`, `cpdName`, `cpdDescription`, `cpdFields`, `cpdOptions`, `cpdTempalteID`, `cpdCatPageID`, `cpdProductPageID`, `fk_cpd_domID`, `cpdSort`, `cpdActive`) VALUES
  (3, 'Stick N Find Location Record', 'Stick N Find Location Record', 'Stick N Find Location Record', '2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010', '', NULL, 0, 0, 0, 0, NULL),
  (4, 'Lat/Lng Location Record', 'Lat/Lng Location Record', 'Lat/Lng Location Record', '2100,2101,2102,2103,2104,2105,2106,2107,2108', '', NULL, 0, 0, 0, 0, NULL);

ALTER TABLE `ciPartner_partcdefs`
ADD PRIMARY KEY (`cpdID`);

CREATE TABLE IF NOT EXISTS `ciPartner_fields` (
  `cfdID` int(4) NOT NULL,
  `cfdName` varchar(255) NOT NULL DEFAULT '',
  `cfdDescription` varchar(255) NOT NULL DEFAULT '',
  `cfdType` enum('text','longtext','decimal','integer','boolean','image','file','html','password','location') NOT NULL DEFAULT 'text',
  `cfdTable` varchar(255) DEFAULT NULL,
  `cfdField` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Cart: Field Descriptions';

INSERT INTO `ciPartner_fields` (`cfdID`,`cfdName`,`cfdDescription`,`cfdType`,`cfdTable`,`cfdField`) VALUES
  # Stick N Find Location Record Fields
  (2001, 'Identifier','A Unique Identifier for this Record','','',''),
  (2002, 'Link On Entry','','','',''),
  (2003, 'Link On Exit','','','',''),
  (2004, 'Notify On Entry','Do you want to Notify App Users when this Record is in range?','','',''),
  (2005, 'Notify On Exit','Do you want to Notify App Users when this Record has gone out of range?','','',''),
  (2006, 'Proximity UUID','The UUID of the Stick N Find item for this Record','','',''),
  (2007, 'Major','The Major value of the Stick N Find item for this Record','','',''),
  (2008, 'Minor','The Minor value of the Stick N Find item for this Record','','',''),

  # Lat/Lng Location Record Fields
  (2101, 'Identifier','A Unique Identifier for this Record','','',''),
  (2102, 'Link On Entry','','','',''),
  (2103, 'Link On Exit','','','',''),
  (2104, 'Longitude','Longitude for the Location Record','','',''),
  (2105, 'Latitude','Latitude for the Location Record','','',''),
  (2106, 'Radius','The Radius for the Location to trigger if it is in range (e.g. if 2km, enter 2000m)','','',''),
  (2107, 'Notify On Entry','Do you want to Notify App Users when this Record is in range?','','',''),
  (2108, 'Notify On Exit','Do you want to Notify App Users when this Record has gone out of range?','','','')

CREATE TABLE IF NOT EXISTS `cigroup_page_links` (
  `colID` int(4) NOT NULL,
  `fk_col_cprID` int(4) NOT NULL DEFAULT '0',
  `fk_col_copID` int(4) NOT NULL DEFAULT '0',
  `colActive` tinyint(1) DEFAULT NULL,
  `versionNum` tinyint(5) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS  `cigroup_reference` (
  `ID` tinyint(4) NOT NULL,
  `MODULE_NAME` varchar(50) NOT NULL DEFAULT '',
  `MODULE_ID` tinyint(4) NOT NULL DEFAULT '0',
  `GROUP_REF` varchar(50) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS  `cigroup_sections` (
  `grpSId` int(4) NOT NULL,
  `grpSName` varchar(50) NOT NULL DEFAULT '',
  `grpSPageId` int(4) NOT NULL DEFAULT '0',
  `grpSVisible` tinyint(1) NOT NULL DEFAULT '0',
  `canAdd` tinyint(1) NOT NULL DEFAULT '1',
  `grpSSort` int(4) NOT NULL DEFAULT '0',
  `grpSDelete` int(11) NOT NULL DEFAULT '0',
  `viewPermissions` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS  `cigroup_types` (
  `grpID` int(4) NOT NULL,
  `grpValue` varchar(255) NOT NULL DEFAULT '',
  `grpDescription` varchar(255) NOT NULL DEFAULT '',
  `grpCategory` varchar(25) NOT NULL DEFAULT '',
  `grpCatSort` int(2) DEFAULT '0',
  `grpDelete` tinyint(1) NOT NULL DEFAULT '0',
  `viewPermissions` int(11) DEFAULT NULL,
  `grpActive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `cigroup_page_links`
  ADD PRIMARY KEY (`colID`),
  ADD UNIQUE KEY `idx_PageOptions` (`fk_col_cprID`,`fk_col_copID`,`versionNum`);

ALTER TABLE `cigroup_reference`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `cigroup_sections`
  ADD PRIMARY KEY (`grpSId`);

ALTER TABLE `cigroup_types`
  ADD PRIMARY KEY (`grpID`);

ALTER TABLE `cigroup_page_links`
  MODIFY `colID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

ALTER TABLE `cigroup_reference`
  MODIFY `ID` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

ALTER TABLE `cigroup_sections`
  MODIFY `grpSId` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

ALTER TABLE `cigroup_types`
  MODIFY `grpID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

CREATE TABLE IF NOT EXISTS `cart_option_categories` (
  `cart_option_categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `cart_option_categories_name` varchar(255) NOT NULL,
  `cart_option_categories_descriptor` varchar(255) DEFAULT NULL,
  `cart_option_categories_sort` int(11) NOT NULL,
  `cart_option_categories_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cart_option_categories_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

ALTER TABLE ci_users ADD column isDeleted int(11) NOT NULL DEFAULT 0;
ALTER TABLE ci_users ADD column DateModified datetime NOT NULL;

ALTER TABLE ci_users MODIFY column SURNAME varchar(255) NULL;
ALTER TABLE ci_users MODIFY column F_NAME varchar(255) NULL;
ALTER TABLE ci_users MODIFY column EMAIL varchar(1000) NULL;
ALTER TABLE ci_users MODIFY column USER_NAME varchar(255) NULL;
ALTER TABLE ci_users MODIFY column PASS_WD varchar(255) NULL;

ALTER TABLE assets MODIFY column updateDate timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE assets MODIFY column ASSET_INST_ID int(255) NULL DEFAULT NULL;

ALTER TABLE cart_options ADD column copSKUable int(1) NOT NULL DEFAULT 0;
ALTER TABLE cart_options ADD column copBarcodeable int(1) NOT NULL DEFAULT 0;

ALTER TABLE cart_optionslink ADD column colSKUValue varchar(255) NULL DEFAULT NULL;
ALTER TABLE cart_optionslink ADD column colBarcodeValue varchar(255) NULL DEFAULT NULL;

ALTER TABLE cart_products ADD column cprSKU varchar(255) NULL DEFAULT NULL;
ALTER TABLE cart_products ADD column cprBarcode varchar(255) NULL DEFAULT NULL;

ALTER TABLE cigroup_sections ADD column grpSDelete int(11) NOT NULL DEFAULT 0;

ALTER TABLE accesslevels ADD PRIMARY KEY(ID);
ALTER TABLE accesslevels ADD column AccessLevelActive int(11) NOT NULL DEFAULT 0;
UPDATE accesslevels SET AccessLevelActive = 1 WHERE ID = 1;
UPDATE accesslevels SET AccessLevelName = 'Administrator',AccessLevelActive = 1 WHERE ID = 2

ALTER TABLE master_sections MODIFY column VIS smallint(1) NOT NULL DEFAULT 1;