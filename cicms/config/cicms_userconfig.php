<?php
/*************************************************************************
* ============================ WARNING =================================
* This file contains configuration options vital to the succesful 
* operation of the CICMS engine. Please do not make any changes here
* unless you know exactly what to change. Please feel free to contact
* Creative Intersection (www.cisuite.com) who will be able to provide
* assistance.
*************************************************************************/

global $cicms_db_name, $domainID, $hasOptions, $hasGroups, $hasSurveyGroups, $isNotionalHierarchy, $hasSurveyPreview, $page404, $SiteURL, $hasVersioning, $DefaultPageID, $hasStripslashes, $useNotionalHash, $useNotionalIDs, $maxNotionalLevels, $hasNotionals, $usePageAlias, $useOnlyAlias, $hasSearch, $hasTemplateFiles, $GFKInterfaceLogo, $GFKComingSoon, $AuthKey, $MenuCachedFileName, $partnerAlphaSort, $isGroupsedAccess, $isGroupsedSectID, $deploymentColors;

$cicms_db_host = "localhost";
$cicms_db_name = "streamli_cisuite";
$cicms_db_user = "streamli_admin";
$cicms_db_pass = "F@4p!QlBJ&#n";
$cicms_db_type = "mysql";
$HasDomains = false;
$SiteTitle = "Streamline Plus";
$SiteName = "Streamline Plus";
$DefaultPageID = 226;
$SiteURL = "https://streamlineplus.com.au";
$SiteLicense = "1yID-uelS-r3Xw-4wEz-JJhP";
$AuthKey = "dTFlbFM2MnIyMzZYdzI0dy4zRXpFSkpoKzJQMXk0MjM5MEkxRA==";
$GFKInterfaceLogo = "";
$GFKComingSoon = "";

$page404 = "error_404";
/**
 * ID of the page where the preview survey will be run against
 * This page is normally and should be not seen or be able to be
 * created/edited, nor will it's template be accessible
 */
$previewsurveyid = 150;
$domainID = 1;
$hasOptions = false;
$hasGroups = true;
$hasSurveyGroups = false;
$hasSurveyPreview = false;
$hasVersioning = false;
$useNotionalHash = false;
$useNotionalIDs = false;
$maxNotionalLevels = 3;
$hasNotionals = true;
$usePageAlias = true;
$hasSearch = true;
$hasTemplateFiles = true;
$MenuCachedFileName = "menucached.php";
$partnerAlphaSort = true;
$isGroupsedAccess = false;
$isGroupsedSectID = 1;
$useOnlyAlias = true;
$isNotionalHierarchy=true;//Used for 2017+ engines

$deploymentColors = array(
    "7AC429", "SLP - Light Green", //Every two array items is a color group (hex code and title)
    "165A00", "SLP - Dark Green",
    "9BADC1", "SLP - Light Blue",
    "536D8C", "SLP - Mid Blue",
    "1D2937", "SLP - Dark Blue",
    "4F4F4F", "SLP - Grey",
    "1A1A1A", "SLP - Black",
);
$deploymentColors = implode('","',$deploymentColors);

global $CICMSMID, $CICARTMID, $CIGROUPSMID, $CILOCATEMID, $CIMAILMID, $CINEWSMID, $CIPARTNERMID, $CISURVEYMID, $CIUSERMID;
$CICMSMID = 1;
$CICARTMID = 2;
$CIGROUPSMID = 3;
$CILOCATEMID = 4;
$CIMAILMID = 5;
$CINEWSMID = 6;
$CIPARTNERMID = 7;
$CISURVEYMID = 8;
$CIUSERMID = 9;
$CIFORUMMID = 10;

$hasStripslashes = true;
$emailBounce= "minh.ta@creativeintersection.com";
$ciMailFromAddy = "Mailing List <newsletter@creativeintersection.com>";

// CICART VARIABLES
define("CICART_PAGING_LIMIT", "1", true);				// number of cart items to display on the site
define("CICART_PREVIOUS_PAGE", "<< Previous", true);	// text / image for previous page
define("CICART_NEXT_PAGE", "Next >>", true);			// text / image for next page\n
