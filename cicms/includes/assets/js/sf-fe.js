;(function(){

    $('ul li.sf-menuitem-title').on('click', function(e){
        var $this = $(this);
        var $parent = $this.parent();

        $parent.find('.items').slideToggle();
    });

})(jQuery);