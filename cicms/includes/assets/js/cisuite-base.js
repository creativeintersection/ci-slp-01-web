$(document).ready(function(){
    //$('section[data-type="background"]').each(function(){var $bgobj = $(this);var $window = $(window);$(window).scroll(function() {var yPos = -($window.scrollTop() / $bgobj.data('speed'));var coords = '50% '+ yPos + 'px';$bgobj.css({ backgroundPosition: coords });});});
    $('.navbar a[href^="#"]').bind('click.smoothscroll',function (e) {
        e.preventDefault();
        var target = this.hash,$target = $(target);
        var navbar_h = $('.navbar').height();

        if(navigator.userAgent.indexOf("Firefox") > 0){
            var scrollToPosition = $target.offset().top - navbar_h;

            $('html').animate({ 'scrollTop': scrollToPosition }, 900, function(){
                window.location.hash = "" + target;
                // This hash change will jump the page to the top of the div with the same id
                // so we need to force the page to back to the end of the animation
                $('html').animate({ 'scrollTop': scrollToPosition }, 0);
            });
        }else{
            $('html, body').stop().animate({'scrollTop': $target.offset().top-90}, 900, 'swing', function () {
                window.location.hash = target;
            });
        }
    });


    $("#contact_form").submit(function(e){
        e.preventDefault();
        $('.success').hide();
        $('.error').hide();
        var form = $("#contact_form").serializeArray();
        if($('#cisuite-ct-spam').val()=='7') {
            $.post('/assets/php/contactform.php',form,function(data){
                if(data==1){
                    $('.success').show();
                }
            });
        }else{
            $('.error').show();
            $('.error').text("Your spam checker was incorrect, please try again.");
        }
    });

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $('section[data-type="background"]').each(function () {
            var $bgobj = $(this); // assigning the object
            $bgobj.css('background-size','cover!important;');
        });

    }else{
        $('section[data-type="background"]').each(function () {
            var $bgobj = $(this); // assigning the object

            $(window).scroll(function () {
                var yPos = -($(window).scrollTop() / ($bgobj.data('speed') * 0.2));

                // Put together our final background position
                //var coords = yPos + 'px';
                var coords = '50% ' + yPos + 'px';

                // Move the background
                $bgobj.css({ backgroundPosition: coords });
            });
        });
    }
});
