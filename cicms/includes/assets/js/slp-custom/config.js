/**
 * Global Variables
 */
var apikey = '2b29c814383e403a9617ce9c3c80ea6b';
var windowWidthBreakpoint = 767;

var htmlLargeSpinner = '<div class="slp-spinner" style="display:block; max-width100%; margin:120px auto; text-align:center;"><h2>Processing..</h2><img src="/cicms/includes/assets/img/slp/slp-spinner-lg.svg"  alt="loading..."/></div>';
var htmlSmallSpinner = '<li><div class="slp-spinner" style="display:block; max-width:100%; margin:5px auto; text-align:center;"><img src="/cicms/includes/assets/img/slp/slp-spinner-sm.svg"  alt="loading..." style="max-width:100%;"/></div></li>';

var isDoneSubmitting = true;

/**
 * Routing
 */

var urlLoginSubmit = '/cicms/slp/api/auth/login';
var urlLoginView = '/login';

var urlRegisterSubmit = '/cicms/slp/api/auth/register';
var urlForgotPasswordSubmit = '/cicms/slp/api/auth/forgot-password';
var urlResetPasswordSubmit = '/cicms/slp/api/auth/reset-password';

var urlLogout = '/cicms/slp/api/logout';

var urlDashboard = '/calculator';

var urlSettingsGetSubmit = '/cicms/slp/api/jwt/settings';

var urlActivation = '/cicms/slp/api/activation';

var urlOrderFormSubmit = '/cicms/slp/api/jwt/order-form';

var urlOrderWarrantiesSubmit = '/cicms/slp/api/jwt/order-warranties';

var urlCadDrawingsSubmit = '/cicms/slp/api/jwt/cad-drawings';

var urlMaintenanceManualSubmit = '/cicms/slp/api/jwt/maintenance-manual';

var urlInternalContactSubmit = '/cicms/slp/api/jwt/internal-contact';

var urlUnreadNotificationsNumber = '/cicms/slp/api/jwt/notifications/unread';