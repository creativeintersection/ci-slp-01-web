$(document).ready(function(){
    $(document.body).on('click touch', '.popup-container-trigger', showPopup);

    $(document.body).on('touchstart', '.popup-container-trigger', function() {
        $(this).on('touchend', function(){
            showPopup();
            $(this).off('touchend');
        });
        //behaviour for move
        $(this).on('touchmove', function(){
            $(this).off('touchend');
        });
    });

    $(document.body).on('click touch touchstart', '.popup-container,.popup-close', function(e){
        $(this).fadeOut();
        $(this).find('.popup-container-content').empty();
    });

    $(document.body).on('click touchstart touch', '.popup-container-content iframe,.popup-container-content img', function(e){
        e.stopPropagation();
    });
});

var showPopup = function() {
    var $container = $(this).parent().parent().find('.popup-container');
    var $datavalue = $container.data('value');
    var $subcontainer = $container.find('.popup-container-content');
    if($container.data('popuptype') == "video"){
        $subcontainer.removeClass('image');
        $subcontainer.append('<iframe allowfullscreen style="width: 100%;height: 100%;border: none;" src="'+$datavalue+'"></iframe>');

        $subcontainer.append('<span class="popup-close">Close</span>');
        $container.hide().removeClass('hidden').fadeIn();
    }
    else if($container.data('popuptype') == "image"){
        var $datavalue = $container.prev().text();
        $subcontainer.addClass('image');
        $subcontainer.append('<div><img src="'+$datavalue+'" style="position:absolute;top:0;left:0;right:0;bottom:0;margin:auto;max-width:100%;"/></div>');

        var $top = $subcontainer.find('img').position().top;
        $subcontainer.append('<span class="popup-close">Close</span>');
        $container.hide().removeClass('hidden').fadeIn();
    }
};