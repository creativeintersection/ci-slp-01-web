$(document).ready(function() {
    contact.init();
});

var contact = {
    handleError : function(idElement, boolHasError, errMessage){
        errMessage = (typeof errMessage === 'undefined') ? 'default' : errMessage;
        var currentElement = $('#'+idElement);
        var parent = currentElement.parent();
        var grandParent = parent.parent();
        var errClass = idElement+'Error';
        if(parent.find('.glyphicon-remove').length > 0 && !boolHasError){
            parent.find('.glyphicon-remove').remove();
            grandParent.removeClass('has-error has-feedback');
            grandParent.find('.'+errClass).remove();
        }
        if(parent.find('.glyphicon-remove').length == 0 && boolHasError){
            parent.append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            grandParent.addClass('has-error has-feedback');
            grandParent.append('<label class="col-sm-offset-2 col-sm-8 control-label '+errClass+'">'+errMessage+'</label>');
        }
        if(parent.find('.glyphicon-remove').length > 0 && boolHasError && grandParent.find('.'+errClass).html() != errMessage)
        {
            grandParent.find('.'+errClass).html(errMessage);
        }
    },

    checkEmpty : function(idElement, errMessage){
        var currentElement = $('#'+idElement);
        var boolSuccess = false;
        if(currentElement.val()==''){
            contact.handleError(idElement, true, errMessage);
            if($('.errMessage').hasClass('hidden')==false){
                $('.errMessage').html('');
                $('.errMessage').addClass('hidden');
            }
        }
        else{
            contact.handleError(idElement, false);
            boolSuccess = true;
        }
        return boolSuccess;
    },

    matchPattern : function(idElement, strPattern, errMessage){
        var currentElement = $('#'+idElement);
        var boolSuccess = false;
        var valElement = currentElement.val();
        // if(valElement.match(strPattern)){
        if(strPattern.test(valElement)){
            contact.handleError(idElement, false);
            boolSuccess = true;
        }
        else{
            contact.handleError(idElement, true, errMessage);
        }
        return boolSuccess;
    },

    checkValid : function(idElement, errMessage, typeValid){
        var currentElement = $('#'+idElement);
        var boolSuccess = false;
        if(currentElement.val()!=''){
            switch(typeValid){
                case 'phone number':
                    var strPattern = /^\d{10}$/;
                    boolSuccess = contact.matchPattern(idElement, strPattern, errMessage);
                    break;
                case 'email':
                    var strPattern = /^\S+@\S+\.\S+$/;
                    boolSuccess = contact.matchPattern(idElement, strPattern, errMessage);
                    break;
                default:
                    boolSuccess = false;
            }
        }
        return boolSuccess;
    },

    resetForm : function(){
        $('#txtName').val('');
        $('#txtEmail').val('');
        $('#txtMessage').val('');
        contact.handleError('txtName', false);
        contact.handleError('txtEmail', false);
        contact.handleError('txtMessage', false);
        if($('.errMessage').hasClass('hidden')==false){
            $('.errMessage').html('');
            $('.errMessage').addClass('hidden');
        }
    },

    contactPost : function(data,callback){
        $.post('/process_email.php',data,callback,'json');
    },

    init : function(){
        var commandSent = false;

        $('#txtName').blur(function(){
            contact.checkEmpty('txtName', 'Name is required');
        });

        $('#txtEmail').blur(function(){
            var boolSuccess = contact.checkEmpty('txtEmail', 'Email address is required');
            if(boolSuccess){
                contact.checkValid('txtEmail', 'Email address is invalid', 'email');
            }
        });

        $('#txtMessage').blur(function(){
            contact.checkEmpty('txtMessage', 'Message is required');
        });

        $('#frmContact').submit(function(){
            commandSent = true;
            var boolEmptyNameSuccess = contact.checkEmpty('txtName', 'Name is required');
            var boolEmptyEmailSuccess = contact.checkEmpty('txtEmail', 'Email address is required');
            var boolValidEmailSuccess = contact.checkValid('txtEmail', 'Email address is invalid', 'email');
            var boolEmptyMsgSuccess = contact.checkEmpty('txtMessage', 'Message is required');

            if(boolEmptyNameSuccess && boolEmptyEmailSuccess && boolValidEmailSuccess && boolEmptyMsgSuccess){
                contact.contactPost($(this).serialize(),function(r){
                    if(r.error){
                        var html = '<div class="alert alert-danger fade in" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error(s):</strong><br><ul>'+r.error+'</ul></div>';
                        $('.errMessage').html(html);
                        $('.errMessage').removeClass('hidden');
                    }
                    else
                    {
                        contact.resetForm();
                        grecaptcha.reset();
                        alert('Your enquiry has been submitted.');
                    }
                    commandSent = false;
                });
            }
            return false;
        });
    }
};

var helpers = {
    getParameterByName : function (name, url) {
        var values = [];
        if (!url) url = location.href;

        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");

        var pattern = name + '=([^&#]+)';
        var o_reg = new RegExp(pattern,'ig');
        while (true){
            var matches = o_reg.exec(url);
            if (matches && matches[1]){
                values.push(matches[1]);
            } else {
                break;
            }
        }

        if (!values.length){
            return null;
        } else {
            return values.length == 1 ? values[0] : values;
        }
    },
    shortenText : function(text, intChar) {
        var first = text;
        var last = '';
        if(text.length > intChar){
            first = text.substr(0, intChar) + '...';
            last = text.substr(intChar-1, text.length - intChar);
        }
        return {
            first: first,
            last: last
        }
    }
};

var newFormHandler = {

    escapeSquareBrackets : function(elementName) {
        return elementName ? elementName.replace(/(:|\.|\[|\])/g,'\\$1') : elementName;
    },

    handleError : function(currentElement, boolHasError, errMessage, colClass){
        // remove this because of select2
        // errMessage = (typeof errMessage === 'undefined') ? 'default' : errMessage;
        errMessage = (typeof errMessage === 'undefined') ? '' : errMessage;
        colClass = (typeof colClass === 'undefined') ? '' : colClass;


        // escape square brackets in idElement
        // if (idElement) {
        //     idElement = idElement.replace(/(:|\.|\[|\])/g,'\\$1');
        // }
        var idElement = currentElement.attr('id'); // get id of current element
        if (!idElement) {
            idElement = currentElement.attr('name'); // get name of current element
        }

        var parent = currentElement.parent();
        if (currentElement.hasClass('selectpicker')) {
            parent = parent.parent();
        }
        var grandParent = parent.parent();
        var errClass = idElement+'Error';

        if (currentElement.is(':radio')) {
            grandParent = grandParent.parent();
            if(grandParent.find('.glyphicon-remove').length > 0 && !boolHasError){
                grandParent.find('.glyphicon-remove').remove();
                grandParent.removeClass('has-error has-feedback');
                grandParent.find('.'+newFormHandler.escapeSquareBrackets(errClass)).remove();
            }
        }
        else {
            if(parent.find('.glyphicon-remove').length > 0 && !boolHasError){
                parent.find('.glyphicon-remove').remove();
                grandParent.removeClass('has-error has-feedback');
                grandParent.find('.'+newFormHandler.escapeSquareBrackets(errClass)).remove();
            }
        }
        if(parent.find('.glyphicon-remove').length == 0 && boolHasError){
            parent.append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
            grandParent.addClass('has-error has-feedback');
            grandParent.append('<label class="'+colClass+' control-label '+errClass+'">'+errMessage+'</label>');
        }
        if(parent.find('.glyphicon-remove').length > 0 && boolHasError && grandParent.find('.'+newFormHandler.escapeSquareBrackets(errClass)).html() != errMessage)
        {
            grandParent.find('.'+newFormHandler.escapeSquareBrackets(errClass)).html(errMessage);
        }

        if (errMessage) {
            $('.err-message ul').html('<li>'+errMessage+'</li>');
            $('.err-message').css('display', 'block');
        }
        else {
            $('.err-message').css('display', 'none');
        }
    },

    checkEmpty : function(currentElement, errMessage, colClass){
        var boolSuccess = false;

        if (currentElement.is(':radio')) {
            var elName = currentElement.attr('name'); // get name of current element
            var hasError = true;
            $('[name="'+newFormHandler.escapeSquareBrackets(elName)+'"]').each(function () {
                if ($(this).is(':checked')) {
                    hasError = false;
                }
            });
        }
        else if (currentElement.is(':checkbox')) {
            var hasError = false; // checkbox required means nothing -> checked = yes, unchecked = no
        }
        else {
            var elValue = currentElement.val() ? currentElement.val().trim() : '';
            var hasError = elValue == '' ? true : false;
        }
        if(hasError){
            newFormHandler.handleError(currentElement, true, errMessage, colClass);
        }
        else{
            newFormHandler.handleError(currentElement, false);
            boolSuccess = true;
        }
        return boolSuccess;
    },

    checkElementChange : function (currentElement, elementCb) {
        currentElement.on('keyup blur change', elementCb);
        currentElement.bind('input propertychange', elementCb);
    },

    checkRetypePassword : function (currentElement, passwordElement, colClass) {
        newFormHandler.checkElementChange(currentElement, function () {
            if (currentElement.val() !== passwordElement.val()) {
                newFormHandler.handleError(currentElement, true, 'Please retype the same password', colClass);
            }
        });
    },

    formInit : function (form, colClass) {
        $('#'+form+' *').filter(':input').each(function(){
            var self = $(this);
            var label = self.closest('.form-group').find('label').first();
            var message = 'Required';
            var labelText = '';
            if (label) {
                labelText = label.text();
                if (labelText) {
                    labelText = labelText.replace(' *', '');
                }
                message = labelText+' is required.';
            }
            var elementCb = function () {
                newFormHandler.checkEmpty(self, message, colClass);
            };

            if (self.attr('data-required') == 'yes') {
                label.html(labelText+' *');
                newFormHandler.checkElementChange(self, elementCb);
            }
        });
    },    
    
    resetForm : function(form){
        $('#'+form+' *').filter(':input').not(':button').each(function(){
            var self = $(this);
            self.val('');
            
            newFormHandler.handleError(self, false);
        });

        //if($('.errMessage').hasClass('hidden')==false){
        //    $('.errMessage').html('');
        //    $('.errMessage').addClass('hidden');
        //}
        $('.err-message').css('display', 'none');
    },

    formCheckAllFieldNameIsEmpty : function (form, colClass) {
        var boolSuccess = true;
        $('#'+form+' *').filter(':input').not(':button,:hidden').each(function(){
            var self = $(this);
            var label = self.closest('.form-group').find('label').first();
            var message = 'Required';
            var labelText = '';
            if (label) {
                labelText = label.text();
                if (labelText) {
                    labelText = labelText.replace(' *', '');
                }
                message = labelText+' is required.';
            }
            var elementCb = function () {
                newFormHandler.checkEmpty(self, message, colClass);
            };
            if (self.attr('data-required') == 'yes') {
                if (label && labelText) {
                    label.html(labelText+' *');
                }
                newFormHandler.checkElementChange(self, elementCb);
                boolSuccess = boolSuccess && newFormHandler.checkEmpty(self, message, colClass);
            }
        });
        return boolSuccess;
    },

    postForm : function(link, data,callback){
        $.post(link, data, callback, 'json');
    },

    getForm : function(link, data,callback){
        $.get(link, data, callback, 'json');
    }
};

var storeUsingApp = function(usingApp) {
    window.localStorage.setItem('slp_using_app', usingApp);
};

var getUsingApp = function() {
    return window.localStorage.getItem('slp_using_app');
};

var storeJwt = function(jwt) {
    window.localStorage.setItem('slp_jwt', jwt);
};

var getJwt = function() {
    return window.localStorage.getItem('slp_jwt');
};

var storeUser = function(user) {
    window.localStorage.setItem('slp_user', JSON.stringify(user));
};

var getUserId = function() {
    var user = JSON.parse(window.localStorage.getItem('slp_user') || 'null');

    return user ? user.userId : '';
};

var getUserFullname = function() {
    var user = JSON.parse(window.localStorage.getItem('slp_user') || 'null');

    return user ? (user.firstname + ' ' + user.lastname).trim() : '';
};

var clearUserSession = function() {
    window.localStorage.removeItem('slp_using_app');
    window.localStorage.removeItem('slp_jwt');
    window.localStorage.removeItem('slp_user');
};

var logout = function() {
    newFormHandler.getForm(urlLogout, {
        apikey : apikey
    }, function(r) {
        if(r.success){
            console.log('successfully logged out.');
            clearUserSession();
            window.location.href = urlLoginView;
        }
        else {
            alert('error');
        }
    });

    return false;
};

var getUnreadPushNumber = function () {
    var usingApp = getUsingApp();
    var pageTitle = document.title;
    pageTitle = pageTitle.replace(' | Streamline Plus', '');
    var link = [location.host, location.pathname].join('');
    
    var jwt = getJwt();

    newFormHandler.postForm(urlUnreadNotificationsNumber, {
        jwt: jwt,
        apikey: apikey,
        link: link
    }, function(r) {
        if(r.error){
            alert(r.error);
        }
        else {
            if (usingApp) {
                window.location = '/xxupdatebadgexx?badge='+parseInt(r.number || '0')+'&page_title='+encodeURIComponent(pageTitle);
            }
        }
    });

    return false;
};