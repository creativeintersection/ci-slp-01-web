    </div>

    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="row slp-margin-top-btm">
                        <div class="col-xs-4">
                            <p>Address</p>
                        </div>
                        <div class="col-xs-8">
                            <div><!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10001" x--><!--xCICMS_ENDx--></div>
                        </div>
                    </div>
                    <div class="row slp-margin-top-btm">
                        <div class="col-xs-4">
                            <p>Phone</p>
                        </div>
                        <div class="col-xs-8">
                            <p><!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10002" x--><!--xCICMS_ENDx--></p>
                        </div>
                    </div>
                    <div class="row slp-margin-top-btm">
                        <div class="col-xs-4">
                            <p>Email</p>
                        </div>
                        <div class="col-xs-8">
                            <p><!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10003" x--><!--xCICMS_ENDx--></p>
                        </div>
                    </div>
                    <div class="row slp-margin-top-btm">
                        <div class="col-xs-4">
                            <p>Follow Us</p>
                        </div>
                        <div class="col-xs-8">
                            <a href="<!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10004" x--><!--xCICMS_ENDx-->" class="nav-social-button btn-facebook slp-btn-inline-center">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                            <a href="<!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10005" x--><!--xCICMS_ENDx-->" class="nav-social-button btn-twitter slp-btn-inline-center">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                            <a href="<!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10006" x--><!--xCICMS_ENDx-->" class="nav-social-button btn-pinterest slp-btn-inline-center">
                                <i class="fa fa-pinterest" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <p class="slp-margin-top-btm"><a href="<!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10007" x--><!--xCICMS_ENDx-->" title="Privacy Policy">Privacy Policy</a></p>
                    <p class="slp-margin-top-btm"><a href="<!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10008" x--><!--xCICMS_ENDx-->" title="Terms and Conditions">Terms and Conditions</a></p>
                    <div class="row slp-margin-top-btm">
                        <div class="col-md-6">
                            <a href="<!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10009" x--><!--xCICMS_ENDx-->" title="Apple App">
                                <img class="app-store-logo" src="/cicms/includes/assets/img/slp/apple-store-logo.png" alt="Apple App">
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="<!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10010" x--><!--xCICMS_ENDx-->" title="Android App">
                                <img class="app-store-logo" src="/cicms/includes/assets/img/slp/google-play-logo.png" alt="Android App">
                            </a>
                        </div>
                    </div>
                    <p class="slp-margin-top-btm"><!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10011" x--><!--xCICMS_ENDx--></p>
                    <p class="slp-margin-top-btm"><!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10012" x--><!--xCICMS_ENDx--></p>
                </div>
            </div>
        </div>
    </div>

</body>
</html>