<?php
    require_once __DIR__ . '/../../bespoke/BespokeController.php';

    global $bespokeController;
    $bespokeController = new BespokeController();

    if (!function_exists('setClearSessionVars')) {
        function setClearSessionVars($jwt) {
            global $bespokeController;

            $authenticated = $bespokeController->validateJwt($jwt);
            if(empty($authenticated) || isset($authenticated['error'])) {
                session_destroy();

                return false;
            }

            $userId = $authenticated['payload']['userId'];

            $_SESSION['slp']['jwt']          = $authenticated['jwt'];
            $_SESSION['slp']['userId']       = $userId;
            $_SESSION['slp']['username']     = $authenticated['payload']['username'];

            return $authenticated;
        }
    }

    if (!function_exists('validateJwt')) {
        function validateJwt() {
            if (isset($_SESSION['slp']['jwt'])) {
                $jwt = $_SESSION['slp']['jwt'];
                if (session_status() !== PHP_SESSION_ACTIVE) {
                    session_start();
                    session_regenerate_id();
                }
                return setClearSessionVars($jwt);
            }

            if (isset($_REQUEST['jwt']) && !empty($_REQUEST['jwt'])) {
                if (session_status() !== PHP_SESSION_ACTIVE) {
                    session_start();
                    session_regenerate_id();
                }
                return setClearSessionVars($_REQUEST['jwt']);
            }
            return false;
        }
    }

    if (!function_exists('getPageTitle')) {
        function getPageTitle() {
            global $bespokeController;

            $uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
            $slug = array_pop($uriSegments);

            if (!empty($slug)) {
                return $bespokeController->getPageTitle($slug);
            }

            return '';
        }
    }

    $validated = validateJwt();
    $usingApp = false;
    $pageTitle = getPageTitle();

    if ($validated) {
        $jwt = $_SESSION['slp']['jwt'];
        $deviceType = isset($_REQUEST['device_type'])
            ? trim($_REQUEST['device_type'])
            : (isset($_SESSION['slp']['deviceType'])
                ? $_SESSION['slp']['deviceType']
                : ''
            );
        if ($deviceType != '') {
            $usingApp = filter_var($deviceType, FILTER_VALIDATE_INT) === false
                ? in_array($deviceType, array('android', 'ios'))
                : in_array(strval($deviceType), array('0', '1'));
        }
        $username = isset($_SESSION['slp']['username']) ? $_SESSION['slp']['username'] : '';
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?= isset($_GET['page_title']) && !empty($_GET['page_title'])
            ? $_GET['page_title']
            : (isset($pageTitle) && !empty($pageTitle)
                ? $pageTitle
                : 'Streamline Plus') ?></title>

    <!-- Bootstrap -->
    <link href="/cicms/includes/assets/css/slp-library/jquery-ui-1.12.1.min.css" rel="stylesheet">
    <link href="/cicms/includes/assets/css/slp-library/bootstrap-3.3.7.min.css" rel="stylesheet">
    <link href="/cicms/includes/assets/css/slp-library/font-awesome-4.6.3.min.css" rel="stylesheet">
    <link href="/cicms/includes/assets/css/slp-custom/app.css" rel="stylesheet">

    <!-- favicons - start -->
    <link rel="apple-touch-icon" sizes="180x180" href="/cicms/includes/assets/img/slp/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/cicms/includes/assets/img/slp/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/cicms/includes/assets/img/slp/favicons/favicon-16x16.png">
    <link rel="manifest" href="/cicms/includes/assets/img/slp/favicons/site.webmanifest">
    <link rel="mask-icon" href="/cicms/includes/assets/img/slp/favicons/safari-pinned-tab.svg" color="#1d2937">
    <link rel="shortcut icon" href="/cicms/includes/assets/img/slp/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-config" content="/cicms/includes/assets/img/slp/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- favicons - end -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Common Scripts -->
    <script src="/cicms/includes/assets/js/slp-library/jquery-1.12.4.min.js"></script>
    <script src="/cicms/includes/assets/js/slp-library/jquery-ui-1.12.1.min.js"></script>
    <script src="/cicms/includes/assets/js/slp-library/jquery.ui.touch-punch.min.js"></script>
    <script src="/cicms/includes/assets/js/slp-library/bootstrap-3.3.7.min.js"></script>
    <script src="/cicms/includes/assets/js/slp-custom/config.js"></script>
    <script src="/cicms/includes/assets/js/slp-custom/app.js"></script>
    <script src="/cicms/includes/assets/js/slp-custom/ci-main-script.js"></script>

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <script type="text/javascript">
        storeUsingApp('<?= isset($_GET['using_app']) ? $_GET['using_app'] : '' ?>');
        <?= (!$_GET['using_app'] || empty($_GET['app_jwt'])) ? "" : "storeJwt('".$_GET['app_jwt']."');\n" ?>

        $(function () {
            var usingApp = getUsingApp();

            if (usingApp) {
                $('.footer').hide();
                $('.slp-heading').first().hide();
            }
            else {
                $('.footer').show();
                $('.slp-heading').first().show();
            }
        });

        var activationUserId = '<?= isset($_GET['user_id']) ? $_GET['user_id'] : '' ?>';
        var activationHash = '<?= isset($_GET['hash']) ? $_GET['hash'] : '' ?>';
    </script>
</head>
<body>
<? if(!$usingApp) { ?>
    <div class="slp-top-header">
        <div class="container">
            <div class="slp-top-left">
                <a href="/" class="img-responsive" title="Home"><img src="/cicms/includes/assets/img/slp/slp-logo.png"></a>
            </div>
            <div class="slp-top-right">
                <a href="<!--xCICMS_START TYPE="advmenu" SECTION="2" x---><!--xMENU ID="10000" x--><!--xCICMS_ENDx-->" class="slp-btn-blue slp-btn-inline-center" title="Login">Login</a>
            </div>
        </div>
    </div>

    <nav id="slp-navbar" class="navbar navbar-default navbar-changed" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#slp-navbar-inner" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="slp-navbar-inner">
                <ul class="nav navbar-nav">
                    <!--xCICMS_START TYPE="notional_menu" SECTION="1" x--->
                    <li class="dropdown">
                        <a class="navbar-item" href="/<!--xCICMS_MENU_LINKx-->"><!--xCICMS_MENU_CAPTIONx--></a>
                        <ul class="dropdown-menu">
                            <!--xCICMS_NOTIONALMENUx-->
                            <li class="dropdown-submenu">
                                <a class="dropdown-toggle navbar-item" href="/<!--xCICMS_NOTIONALMENU_LINKx-->"><!--xCICMS_NOTIONALMENU_CAPTIONx--> </a>
                                <!--xCICMS_NOTIONALMENU_PLACEHOLDERx-->
                            </li>
                            <!--xCICMS_NOTIONALMENUx-->
                        </ul>
                    </li>
                    <!--xCICMS_ENDx-->
                </ul>
            </div>
        </div>
    </nav>
<? } ?>

    <div class="main-wrapper">