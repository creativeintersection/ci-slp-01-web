<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/cicms/engine/cisuite_engine.php';

$engine = new Engine();

if(isset($_GET['mfc_request'])){
    $req = explode('/',$_GET['mfc_request']);
    $_GET['sfid'] = $req[0];
    $_GET['node'] = $req[2];
}

$user = session_id();
$currentPosition = $engine->getCIStreamflowCourseProgress($_GET['sfid'],$user);
$allowedToView = ($engine->getCIStreamflowCourseOrder($_GET['sfid'],$_GET['node'],$user));

if(!$allowedToView && !empty($currentPosition)){
    $location = "/courses/".$currentPosition['progress']->cistreamflow_courseposition_streamID.'/'.$currentPosition['parent'].'/'.$currentPosition['node'];
    header("Location: ".$location);
} else if(!empty($_GET['node']) && !empty($_GET['sfid'])) {
    $engine->saveCIStreamflowCourseProgress($_GET['sfid'], $_GET['node'], $user);
}
