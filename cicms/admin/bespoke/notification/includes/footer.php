
</div>
</div>
<script>
    var maxfilesize = '<?=ini_get('upload_max_filesize')?>';
    var ismobile = <?=json_encode($ismobile);//Look in header.php for this?>;
    var previewsurveyid = '<?=$previewsurveyid?>';
    var previewstreamflowpage= '<?=$streamflow_previewpage?>';
    var streamflowid = '<?=$streamflowid?>';
    var currentmodule = '<?=$label_nonplural?>';
</script>
<script type="text/javascript" src="/cicms/admin/assets/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/cicms/admin/assets/js/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="/cicms/admin/assets/js/pwstrength-bootstrap.min.js"></script>
<script type="text/javascript" src="/cicms/admin/assets/js/app.js"></script>
<script type="text/javascript" src="/cicms/admin/assets/js/moment.js"></script>

<script type="text/javascript" src="/cicms/admin/assets/js/cisuite-google.js"></script>
<script type="text/javascript" src="/cicms/admin/assets/js/cisuite.cache.js"></script>

<script type="text/javascript" src="/cicms/admin/assets/js/cisuite.cicms.js"></script>
<script type="text/javascript" src="/cicms/admin/assets/js/cisuite.cisurvey.js"></script>
<script type="text/javascript" src="/cicms/admin/assets/js/cisuite.cistreamflow.js"></script>
<script type="text/javascript" src="/cicms/admin/assets/js/cisuite.cicart.js"></script>
<script type="text/javascript" src="/cicms/admin/assets/js/cisuite.cicart.options.js"></script>
<script type="text/javascript" src="/cicms/admin/assets/js/cisuite.ciorders.js"></script>

<script type="text/javascript" src="/cicms/admin/assets/js/vendor.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=<?=$googleMapsAPIKey?>&libraries=places" async defer></script>
</body>
</html>