<nav class="navbar navbar-default cisuite-searchbar-nav" id="navbar">
    <div class="container-fluid">
        <div class="navbar-collapse collapse in">
            <ul class="nav navbar-nav navbar-mobile">
                <li>
                    <button type="button" class="sidebar-toggle">
                        <i class="fa fa-bars"></i>
                    </button>
                </li>
                <li class="logo">
                    <a class="navbar-brand" href="#"><img src="/cicms/admin/assets/images/cisuite_logo.png"/></a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-left">
                <li class="cisuite-survey-search navbar-search hidden-sm">
                    <input id="search" class="cisuite-search-input" data-searchlocation="cisurvey" type="text" placeholder="Search..">
                    <button class="btn-search"><i class="fa fa-search"></i></button>
                </li>
            </ul>
        </div>
        <div class="row cisuite-search-results">
            <div class="no-results">Please Refine your search to see results</div>
            <div class="cisuite-search-results-list visible-md visible-lg hidden-sm hidden-xs"></div>
        </div>
    </div>
</nav>
<div class="overlay"></div>
