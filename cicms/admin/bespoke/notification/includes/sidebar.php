<aside class="app-sidebar" id="sidebar">
    <div class="sidebar-header">
        <a class="sidebar-brand" href="/cicms/admin/"><img src="/cicms/admin/assets/images/cisuite_logo.png"/></a>
        <button type="button" class="sidebar-toggle">
            <i class="fa fa-times"></i>
        </button>
        <p style="text-align: center;"><?=$sessiondetails['username']?><a href="/cicms/logout"><i class="fa fa-sign-out" data-toggle="tooltop" title="Logout" aria-hidden="true"></i></a></p>
    </div>
    <div class="sidebar-menu">
        <ul class="sidebar-nav">
            <li class="dropdown modules-nav">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <div class="icon">
                        <i class="fa fa-object-group" aria-hidden="true"></i>
                    </div>
                    <div class="title">Modules</div>
                </a>
                <div class="dropdown-menu">
                    <ul>
                        <?php

                        $label = '';
                        $label_record = '';
                        $label_category = '';
                        $label_menu = '';
                        $label_nonplural = '';

                        foreach($modules as $k=>$v){
                            if($v['enabled']==true){

                                if(sizeof($v['submodules'])>0) {
                                    $nopages = '';
                                }else{
                                    $nopages = 'no-before';
                                }

                                if($k=='cisurvey') {
                                    $label = $v['label'];
                                    $label_record = $v['label_record'];
                                    $label_category = $v['label_category'];
                                    $label_menu = $v['label_menu'];
                                    $label_nonplural = $v['label_nonplural'];
                                }

                                if(!$ismobile){
                                    echo '<li class="section structure-section owxme '.$nopages.'"><a href="/cicms/admin/'.$v['path'].'"><i class="fa '.$v['icon'].'" aria-hidden="true"></i><p>'.$v['label'].'</p></a>';
                                }else {
                                    echo '<li class="section structure-section '.$nopages.'"><a href="/cicms/admin/' . $v['path'] . '"><i class="fa ' . $v['icon'] . '" aria-hidden="true"></i></a><p>' . $v['label'] . '</p>';
                                }
                                if(sizeof($v['submodules'])>0) {
                                    echo '<div class="dropdown-submenu dropdown-menu structure-pages-1"><ul>';
                                    foreach ($v['submodules'] as $x => $b) {
                                        if($b['enabled']==true) {
                                            echo '<li class="section page-section"><i class="fa ' . $b['icon'] . '" aria-hidden="true"></i><a href="/cicms/admin/' . $v['path'] . '/' . $b['path'] . '">' . $b['label'] . '</a>';
                                        }
                                    }
                                    echo '</ul></div>';
                                }
                                echo '</li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
            </li>
            <li class="cicms_topbar_spacer">&nbsp;</li>
            <li class="dropdown <?=$curpage=="dashboard"?'active':''?>">
                <a href="/cicms/admin/entity">
                    <div class="icon">
                        <i class="fa fa-tasks" aria-hidden="true"></i>
                    </div>
                    <div class="title">Dashboard</div>
                </a>
            </li>
        </ul>
    </div>
</aside>