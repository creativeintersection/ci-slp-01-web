<?php
session_start();

if (isset($_POST['epbf-name'])) {
    $theSender = $_POST['epbf-name'] . " <" . $_POST['epbf-email'] . ">";
    $theServer = "cx4.creativeintersection.com";
    $theRecipient = "richard@streamlineplus.com.au";
    // $theRecipient = "minh.ta@creativeintersection.com";

    if (empty($_POST['g-recaptcha-response'])) {
        die(json_encode(array('error' => '<li>Please answer the captcha</li>')));
    }

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_POSTFIELDS, array("secret" => '6LdL1FoUAAAAAE90sCoVPyF7VMkbgk5-IrfC1csK', "response" => $_POST['g-recaptcha-response']));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    $res = json_decode($response, true);

    if ($err) {
        die(json_encode(array('error' => '<li>Error when verifying the captcha</li>')));
    } else {
        if (isset($res['success']) && $res['success'] == true) {
            unset($_POST['g-recaptcha-response']);

            $theSubject = "*** StreamlinePlus: Website Enquiry ***";
            $theMessage = "";
            foreach ($_POST as $key => $value) {
                if (strtolower($key) != "digitcode" && strtolower($key) != "submit") {
                    $theMessage .= "\n" . str_replace('epbf-', '', $key) . ": " . $value . "\n";
                }
            }
            $theMessage .= "\n\nIP Address: " . $_SERVER['REMOTE_ADDR'] . "";

            $headers = "From: " . $theServer . "\n";
            $headers .= "Reply-To: " . $theSender . "\n";
            $headers .= "X-Sender: " . $theServer . "\n";
            $headers .= 'X-Mailer: PHP/' . phpversion();
            $headers .= "Return-Path: " . $theRecipient . "\n"; // Return path for errors
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: text/html; charset=iso-8859-1;\n";

            if (mail($theRecipient, $theSubject, $theMessage, $extraHeaders)) {
                echo json_encode('success');
            } else {
                die(json_encode(array('error' => '<li>Error when sending the form</li>')));
            }
        } else {
            die(json_encode(array('error' => '<li>Error when verifying the captcha</li>')));
        }
    }
}
